package ro.solutions.rr.flightplan.ui.mainNav.main;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import ro.solutions.rr.flightplan.app.MainApp;
import ro.solutions.rr.flightplan.database.dao.EmailServerConfigDao;
import ro.solutions.rr.flightplan.database.dao.PilotsDao;
import ro.solutions.rr.flightplan.database.dao.PlanesDao;
import ro.solutions.rr.flightplan.database.dao.UserAccountDao;
import ro.solutions.rr.flightplan.database.repository.EmailServerConfigRepository;
import ro.solutions.rr.flightplan.database.repository.PilotRepository;
import ro.solutions.rr.flightplan.database.repository.PlanesRepository;
import ro.solutions.rr.flightplan.database.repository.UserAccountRepository;

public class MainActivityViewModel extends AndroidViewModel {

    private String TAG = MainActivityViewModel.class.getSimpleName();

    private UserAccountRepository mUserAccountRepository;
    private EmailServerConfigRepository mEmailServerConfigRepository;
    private PilotRepository mPilotRepository;
    private PlanesRepository mPlanesRepository;

    private UserAccountDao mUserAccountDao;

    private LiveData<Integer> mGlobalDataDelete;
    private MutableLiveData<Boolean> saveUserAccountData;
    private MutableLiveData<Boolean> saveEmailConfigData;
    private MutableLiveData<LatLng> mLocationPickerData;

    public MainActivityViewModel(@NonNull Application application) {
        super(application);

        mUserAccountDao = ((MainApp) application).getDatabaseManager().getUserDao();
        PilotsDao mPilotsDao = ((MainApp) application).getDatabaseManager().getPilotsDao();
        PlanesDao mPlanesDao = ((MainApp) application).getDatabaseManager().getPlanesDao();
        EmailServerConfigDao mEmailServerConfigDao = (((MainApp) application).getDatabaseManager().getEmailServerDao());

        mUserAccountRepository = new UserAccountRepository(mUserAccountDao);
        mPilotRepository = new PilotRepository(mPilotsDao);
        mPlanesRepository = new PlanesRepository(mPlanesDao);
        mEmailServerConfigRepository = new EmailServerConfigRepository(mEmailServerConfigDao);
    }

    public LiveData<Integer> getGlobalDataResetState() {
        if (mGlobalDataDelete == null) {
            mGlobalDataDelete = mUserAccountDao.userRowCount();
        }
        return mGlobalDataDelete;
    }

    public void deleteAllDataLocally() {
        int delReqUser =  mUserAccountRepository.deleteUserData();
        Log.d(TAG , "Delete user details request returned " + delReqUser + " deleted tables");
        int delReqPilots = mPilotRepository.deleteAllPilots();
        Log.d(TAG , "Delete all pilots request returned " + delReqPilots + " deleted tables");
        int delReqPLanes = mPlanesRepository.deleteAllPlanes();
        Log.d(TAG , "Delete all pilots request returned " + delReqPLanes + " deleted tables");
        int delReqUserConfig = mEmailServerConfigRepository.deleteAllEmailServerConfigs();
        Log.d(TAG , "Delete all pilots request returned " + delReqUserConfig + " deleted tables");
    }

    public void saveDataInEmailFrag(Boolean status) {
        Log.d(TAG , "Setting save data value to " + status + " for save email config");
        saveEmailConfigData.setValue(status);
    }
    public void saveDataInUpdateProfileFrag(Boolean status) {
        Log.d(TAG , "Setting save data value to " + status + " for save user data");
        saveUserAccountData.setValue(status);
    }
    public MutableLiveData<Boolean> getSaveUserAccountData() {
        if (saveUserAccountData == null) {
            saveUserAccountData = new MutableLiveData<>();
        } return saveUserAccountData;
    }

    public MutableLiveData<Boolean> getSaveEmailConfigData() {
        if (saveEmailConfigData == null) {
            saveEmailConfigData = new MutableLiveData<>();
        } return saveEmailConfigData;
    }

    public MutableLiveData<LatLng> getArrivalLocationData() {
        if (mLocationPickerData == null) {
            mLocationPickerData = new MutableLiveData<>();
        }
        return mLocationPickerData;
    }

    public void setLocationPickerData(LatLng locationPickerData) {
        mLocationPickerData.setValue(locationPickerData);
    }
}
