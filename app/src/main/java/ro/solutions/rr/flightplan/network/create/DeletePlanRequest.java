package ro.solutions.rr.flightplan.network.create;


import com.google.gson.annotations.SerializedName;

public class DeletePlanRequest {

    @SerializedName("session")
    private String sessionToken;
    @SerializedName("email_config")
    private EmailServerConfigRequest emailServerConfigRequest;
    @SerializedName("plan_id")
    private int planId;

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public EmailServerConfigRequest getEmailServerConfigRequest() {
        return emailServerConfigRequest;
    }

    public void setEmailServerConfigRequest(EmailServerConfigRequest emailServerConfigRequest) {
        this.emailServerConfigRequest = emailServerConfigRequest;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }
}
