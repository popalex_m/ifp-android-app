package ro.solutions.rr.flightplan.ui.mainNav.planes;


import ro.solutions.rr.flightplan.database.models.Plane;

public interface EditPlaneCallback {

    void onPlaneEditCallback(Plane plane);

    void onPlaneDeleteCallback(Plane plane);
}
