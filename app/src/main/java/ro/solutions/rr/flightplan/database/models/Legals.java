package ro.solutions.rr.flightplan.database.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "table_legals")
public class Legals {

    @Expose(serialize = false , deserialize = false)
    @PrimaryKey(autoGenerate = true)
    private int id;

    private Integer planId;

    @SerializedName("civil_aviation_email")
    private String mCivilAviationEmail;
    @SerializedName("civil_aviation_second_email")
    private String mCivilAviationSecondaryEmail;
    @SerializedName("targeted_ads")
    private int mTargeteted_Ats ;

    public int getId() { return id; }

    public void setId(int mId) {
        this.id = mId;
    }

    public String getCivilAviationEmail() {
        return mCivilAviationEmail;
    }

    public void setCivilAviationEmail(String mCivilAviationEmail) { this.mCivilAviationEmail = mCivilAviationEmail; }

    public String getCivilAviationSecondaryEmail() {
        return mCivilAviationSecondaryEmail;
    }

    public void setCivilAviationSecondaryEmail(String mCivilAviationSecondaryEmail) { this.mCivilAviationSecondaryEmail = mCivilAviationSecondaryEmail; }

    public int getTargeteted_Ats() {
        return mTargeteted_Ats;
    }

    public void setTargeteted_Ats(int mTargeteted_Ats) {
        this.mTargeteted_Ats = mTargeteted_Ats;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

}
