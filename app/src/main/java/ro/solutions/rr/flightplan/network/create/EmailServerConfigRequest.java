package ro.solutions.rr.flightplan.network.create;

import com.google.gson.annotations.SerializedName;

public class EmailServerConfigRequest {

    @SerializedName("host")
    private String mHost;

    @SerializedName("port")
    private Integer mPort;

    @SerializedName("secure")
    private boolean mSecure;

    @SerializedName("from")
    private String mFrom;

    @SerializedName("auth")
    private AuthRequest mAuthRequest;

    public String getmHost() {
        return mHost;
    }

    public void setmHost(String mHost) {
        this.mHost = mHost;
    }

    public Integer getmPort() {
        return mPort;
    }

    public void setmPort(Integer mPort) {
        this.mPort = mPort;
    }

    public boolean ismSecure() { return mSecure; }

    public void setmSecure(boolean mSecure) {
        this.mSecure = mSecure;
    }

    public String getmFrom() {
        return mFrom;
    }

    public void setmFrom(String mFrom) {
        this.mFrom = mFrom;
    }

    public AuthRequest getmAuthRequest() {
        return mAuthRequest;
    }

    public void setmAuthRequest(AuthRequest mAuthRequest) {
        this.mAuthRequest = mAuthRequest;
    }
}
