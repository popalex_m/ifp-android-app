package ro.solutions.rr.flightplan.ui.mainNav.createplan;


import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.solutions.rr.flightplan.R;
import ro.solutions.rr.flightplan.ui.mainNav.main.MainActivityViewModel;
import ro.solutions.rr.flightplan.utils.PermissionUtils;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;
import static ro.solutions.rr.flightplan.ui.mainNav.createplan.CreateFlightPlanFragment.TAG_CREATE_DOCUMENT;

public class LocationPickerFragment extends Fragment  implements OnMapReadyCallback,
                                                                 GoogleMap.OnMarkerDragListener{

    private final int REQUEST_MAP_PERMISSIONS = 101;

    private String TAG = LocationPickerFragment.class.getSimpleName();

    @BindView(R.id.location_picker_map_view)
    MapView mMapView;

    private GoogleMap mMap;
    private AppCompatActivity mActivity;

    private LatLng mCurrentSelectedPosition;
    private MarkerOptions mSelectedPositionMarker;
    private String mMarkerTitle = "Selected Location";

    private MainActivityViewModel mMainActivityViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map_fragment, container, false);
        ButterKnife.bind(this, rootView);
        mMapView.onCreate(savedInstanceState);
        try {
            MapsInitializer.initialize(mActivity.getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        initMapView();
        getCurrentLocation();
        mMainActivityViewModel = ViewModelProviders.of(mActivity).get(MainActivityViewModel.class);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.getItem(0).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        int id = item.getItemId();
        switch (id) {
            case R.id.action_confirm:
                saveArrivalLocation(mCurrentSelectedPosition);
                switchBackToCreateDocument();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveArrivalLocation(@NonNull LatLng latLng){
        mMainActivityViewModel.setLocationPickerData(latLng);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity)context;
        mActivity.setTitle("Choose a location");
    }

    private void initMapView(){ mMapView.getMapAsync(this); }

    private void getCurrentLocation() {
        Log.d(TAG , "Getting the current location");
        FusedLocationProviderClient locationServices = getFusedLocationProviderClient(mActivity);
        if (!PermissionUtils.hasPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            Log.d(TAG , "Permissions not granted , asking for fine location permissions");
            PermissionUtils.askForPermission(getContext(), getActivity(), Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_MAP_PERMISSIONS);
        } else {
            locationServices.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if  (location != null) {
                        double currentLat = location.getLatitude();
                        double currentLong = location.getLongitude();
                        Log.d(TAG , "Location request result ->" + currentLat + currentLong);
                        mCurrentSelectedPosition = new LatLng(currentLat, currentLong);
                        mSelectedPositionMarker = new MarkerOptions()
                                                       .position(new LatLng(currentLat, currentLong))
                                                       .title(mMarkerTitle)
                                                       .draggable(true);
                        mMap.addMarker(mSelectedPositionMarker);

                        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(mCurrentSelectedPosition, 20.0f);
                        mMap.animateCamera(yourLocation);
                    } else {
                        Log.d(TAG , "Location is null");
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setOnMarkerDragListener(this);
    }

    @Override
    public void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onMarkerDragStart(Marker marker) { }

    @Override
    public void onMarkerDrag(Marker marker) { }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        Log.d("System out", "onMarkerDragEnd() returning position" + marker.getPosition());
        mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
        mCurrentSelectedPosition = marker.getPosition();
    }

    private void switchBackToCreateDocument() {
        Log.d(TAG , "Switching back to create document frag");
        FragmentManager mFragmentManager = mActivity.getSupportFragmentManager();
        Fragment createFlightPlanFragment =  mFragmentManager.findFragmentByTag(TAG_CREATE_DOCUMENT);
        if (createFlightPlanFragment != null) {
            Log.d(TAG , "Switching back to create document frag");
            mFragmentManager.beginTransaction()
                    .replace(R.id.main_activity_container, createFlightPlanFragment, TAG_CREATE_DOCUMENT)
                    .commit();
        } else {
            Log.d(TAG , "Create document fragment not found");
        }
    }
}
