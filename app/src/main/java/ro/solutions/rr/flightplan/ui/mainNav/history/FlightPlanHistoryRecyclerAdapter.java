package ro.solutions.rr.flightplan.ui.mainNav.history;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.solutions.rr.flightplan.R;
import ro.solutions.rr.flightplan.network.create.PlanHistoryResponse;
import ro.solutions.rr.flightplan.utils.TimeUtils;


public class FlightPlanHistoryRecyclerAdapter extends  RecyclerView.Adapter<FlightPlanHistoryRecyclerAdapter.ViewHolder> {

    private String TAG = FlightPlanHistoryRecyclerAdapter.class.getSimpleName();

    private List<PlanHistoryResponse> mPlanHistoryList ;
    private FlightPlanActionListener mFlightPlanActionCallback;

    FlightPlanHistoryRecyclerAdapter (@NonNull List<PlanHistoryResponse> planHistoryList,
                                      @NonNull FlightPlanActionListener flightPlanActionListener) {
        mPlanHistoryList = planHistoryList;
        mFlightPlanActionCallback = flightPlanActionListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_plans_history, parent, false);
        return new FlightPlanHistoryRecyclerAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PlanHistoryResponse historyResponse = mPlanHistoryList.get(position);
        holder.mTxtViewTakeOffDate.setText(historyResponse.getTakeOffDate());
        holder.mTxtViewFromInfo.setText(historyResponse.getFromInfo());
        holder.mTxtViewToInfo.setText(historyResponse.getToInfo());

        if (historyResponse.getCancelledOn() != null) {
            holder.mTxtViewCancelledStatus.setVisibility(View.VISIBLE);
        } else {
            holder.mTxtViewCancelledStatus.setVisibility(View.GONE);
        }

        if (TimeUtils.isDateBeforeCurrent(historyResponse.getTakeOffDate()) && historyResponse.getCancelledOn() == null) {
            holder.mBtnCancelPlan.setVisibility(View.VISIBLE);
          } else {
            holder.mBtnCancelPlan.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() { return mPlanHistoryList.size(); }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.txtView_flight_plan_take_off_date)
        TextView mTxtViewTakeOffDate;
        @BindView(R.id.txtView_flight_plan_to_info)
        TextView mTxtViewToInfo;
        @BindView(R.id.txtView_flight_plan_from_info)
        TextView mTxtViewFromInfo;
        @BindView(R.id.txtView_flight_plan_canceled_status)
        TextView mTxtViewCancelledStatus;

        @BindView(R.id.btn_re_schedule_plan)
        ImageButton mBtnReschedulePlan;
        @BindView(R.id.btn_re_cancel_plan)
        ImageButton mBtnCancelPlan;

        ViewHolder(@NonNull View view){
            super(view);
            ButterKnife.bind(this, view);
            mBtnReschedulePlan.setOnClickListener(this);
            mBtnCancelPlan.setOnClickListener(this);
        }

        @Override
        public void onClick(@NonNull View view) {
            int position = getAdapterPosition();
            switch (view.getId()) {
                case R.id.btn_re_cancel_plan :
                    mFlightPlanActionCallback.onFlightPlanCancelled(mPlanHistoryList.get(position).getPlanId());
                    break;

                case  R.id.btn_re_schedule_plan :
                    mFlightPlanActionCallback.onFlightPlanRescheduled(mPlanHistoryList.get(position).getPlanId());
                    break;
            }
        }
    }
}


