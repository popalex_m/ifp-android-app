package ro.solutions.rr.flightplan.database.repository;

import android.arch.lifecycle.LiveData;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;
import ro.solutions.rr.flightplan.database.dao.PlanesDao;
import ro.solutions.rr.flightplan.database.models.Plane;

public class PlanesRepository {

    private String TAG = PlanesRepository.class.getSimpleName();

    private PlanesDao mPlanesDao;

    public PlanesRepository (PlanesDao userDao) { mPlanesDao = userDao; }

    public Completable insertPlane(Plane plane) {
        return Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                mPlanesDao.insertSinglePlane(plane);
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    public void insertPlaneSync (Plane plane) {
        mPlanesDao.insertSinglePlane(plane);
    }

    public void deletePlaneSync (Plane plane) {
        mPlanesDao.deleteSinglePlane(plane);
    }

    public void updateSinglePlane (Plane plane) {
        mPlanesDao.updateSinglePlane(plane);
    }

    public int deleteAllPlanes () {return mPlanesDao.deleteAllPlanes(); }

    public LiveData<List<Plane>> getAllPlanes() {
       return  mPlanesDao.getAllPlanes();
    }
}
