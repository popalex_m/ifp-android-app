package ro.solutions.rr.flightplan.ui.mainNav.dialogs;

import ro.solutions.rr.flightplan.database.models.Plane;

public interface AddEditPlaneCallback {

    void onPlaneAdded(Plane plane);

    void onPlaneEdited(Plane plane);

}
