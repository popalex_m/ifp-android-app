package ro.solutions.rr.flightplan.database.models;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

public class PlanHistoryDetails {

    @Embedded
    Plan plan;

    @Relation(parentColumn = "id" , entityColumn = "planId")
    public Trip mTrip;

    @Relation(parentColumn = "id" , entityColumn = "planId")
    public Legals mLegals;

}
