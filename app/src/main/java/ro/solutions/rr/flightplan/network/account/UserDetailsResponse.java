
package ro.solutions.rr.flightplan.network.account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserDetailsResponse {

    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("family_name")
    @Expose
    private String familyName;
    @SerializedName("cc_email")
    @Expose
    private String ccEmail;
    @SerializedName("session")
    @Expose
    private String session;
    @SerializedName("pilots")
    @Expose
    private List<PilotResponse> pilotResponses = null;
    @SerializedName("planes")
    @Expose
    private List<PlaneResponse> planeResponses = null;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getCcEmail() {
        return ccEmail;
    }

    public void setCcEmail(String ccEmail) {
        this.ccEmail = ccEmail;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public List<PilotResponse> getPilotResponses() {
        return pilotResponses;
    }

    public void setPilotResponses(List<PilotResponse> pilotResponses) {
        this.pilotResponses = pilotResponses;
    }

    public List<PlaneResponse> getPlaneResponses() {
        return planeResponses;
    }

    public void setPlaneResponses(List<PlaneResponse> planeResponses) {
        this.planeResponses = planeResponses;
    }

}
