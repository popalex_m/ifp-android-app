package ro.solutions.rr.flightplan.ui.mainNav;

public enum  OperationTypes {
    EDIT_EXISTING_PILOT,
    CREATE_NEW_PILOT,
    UPDATE_EXITING_PILOT,
    EDIT_EXISTING_PLANE,
    CREATE_NEW_PLANE,
    UPDATE_EXITING_PLANE
}
