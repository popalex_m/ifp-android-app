package ro.solutions.rr.flightplan.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import ro.solutions.rr.flightplan.database.models.UserAccount;

@Dao
public interface UserAccountDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSingleUser(UserAccount user);

    @Update
    int updateUser(UserAccount user);

    @Delete
    void deleteUser(UserAccount users);

    @Query("SELECT * FROM user_account LIMIT 1")
    LiveData<UserAccount> getFirstUser();

    @Query("SELECT * FROM user_account LIMIT 1")
    UserAccount getUserAccount();

    @Query("DELETE FROM user_account")
    int deleteUserAccount();

    @Query("SELECT COUNT(*) FROM user_account")
    int getUserRowCount();

    @Query("SELECT COUNT(*) FROM user_account")
    LiveData<Integer> userRowCount();
}
