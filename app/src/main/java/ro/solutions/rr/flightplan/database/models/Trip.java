package ro.solutions.rr.flightplan.database.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "table_trips")
public class Trip {

    @PrimaryKey
    @Expose
    private Integer id;

    private Integer planId;

    @SerializedName("takeoff_date")
    private String takeOffDate;
    @SerializedName("landing_date")
    private String landingDate;
    @SerializedName("est_flight_time")
    private String estFlightTime;
    @SerializedName("stop_count")
    private int stopCount;
    @SerializedName("shipped_ppl_count")
    private int shippedPplCount;
    @SerializedName("owner_consent")
    private String ownerConsent;
    @SerializedName("activity_type")
    private String mActivityType;

    public Trip() { }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTakeOffDate() {
        return takeOffDate;
    }

    public void setTakeOffDate(String takeOffDate) {
        this.takeOffDate = takeOffDate;
    }

    public String getLandingDate() {
        return landingDate;
    }

    public void setLandingDate(String landingDate) {
        this.landingDate = landingDate;
    }

    public String getEstFlightTime() {
        return estFlightTime;
    }

    public void setEstFlightTime(String estFlightTime) {
        this.estFlightTime = estFlightTime;
    }

    public int getStopCount() {
        return stopCount;
    }

    public void setStopCount(int stopCount) {
        this.stopCount = stopCount;
    }

    public int getShippedPplCount() {
        return shippedPplCount;
    }

    public void setShippedPplCount(int shippedPplCount) {
        this.shippedPplCount = shippedPplCount;
    }

    public String getOwnerConsent() {
        return ownerConsent;
    }

    public void setOwnerConsent(String ownerConsent) {
        this.ownerConsent = ownerConsent;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public String getActivityType() { return mActivityType; }

    public void setActivityType(String mActivityType) { this.mActivityType = mActivityType; }

}
