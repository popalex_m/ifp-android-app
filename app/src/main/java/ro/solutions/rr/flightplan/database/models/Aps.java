package ro.solutions.rr.flightplan.database.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "table_aps")
public class Aps {

    @Expose(serialize = false , deserialize = false)
    @PrimaryKey(autoGenerate = true)
    private int mId;

    @SerializedName("send_email_to_carabinieri")
    private boolean mSendEmailToCarabinieri;
    @SerializedName("mail")
    private boolean mMail;
    @SerializedName("carabinieri_name")
    private String mCarabinieriName;

    public Aps() { }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public boolean isSendEmailToCarabinieri() {
        return mSendEmailToCarabinieri;
    }

    public void setSendEmailToCarabinieri(boolean mSendEmailToCarabinieri) {
        this.mSendEmailToCarabinieri = mSendEmailToCarabinieri;
    }

    public boolean isMail() {
        return mMail;
    }

    public void setMail(boolean mMail) {
        this.mMail = mMail;
    }

    public String getCarabinieriName() {
        return mCarabinieriName;
    }

    public void setCarabinieriName(String mCarabinieriName) {
        this.mCarabinieriName = mCarabinieriName;
    }
}
