package ro.solutions.rr.flightplan.database.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@Entity(tableName = "user_account")
public class UserAccount {

    @Expose(serialize = false , deserialize = false)
    @PrimaryKey(autoGenerate = true)
    public int id;

    @SerializedName("first_name")
    public String mFirstName;
    @SerializedName("family_name")
    public String mLastName;
    @SerializedName("password")
    private String mPassword;
    @SerializedName("user_email")
    public String mUserEmail;
    @SerializedName("cc_email")
    private String mCcEmail;
    @SerializedName("session")
    public String mSessionToken;

    public UserAccount() { }

    public UserAccount(int id, String mFirstName, String mLastName, String mPassword, String mUserEmail, String mCcEmail, String mSessionToken) {
        this.id = id;
        this.mFirstName = mFirstName;
        this.mLastName = mLastName;
        this.mPassword = mPassword;
        this.mUserEmail = mUserEmail;
        this.mCcEmail = mCcEmail;
        this.mSessionToken = mSessionToken;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getmFirstName() {
        return mFirstName;
    }

    public void setmFirstName(String mFirstName) {
        this.mFirstName = mFirstName;
    }

    public String getmLastName() {
        return mLastName;
    }

    public void setmLastName(String mLastName) {
        this.mLastName = mLastName;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public String getmUserEmail() {
        return mUserEmail;
    }

    public void setmUserEmail(String mUserEmail) {
        this.mUserEmail = mUserEmail;
    }

    public String getmSessionToken() {
        return mSessionToken;
    }

    public void setmSessionToken(String mSessionToken) {
        this.mSessionToken = mSessionToken;
    }

    public String getCcEmail() { return mCcEmail; }

    public void setCcEmail(String mCcEmail) { this.mCcEmail = mCcEmail; }

}
