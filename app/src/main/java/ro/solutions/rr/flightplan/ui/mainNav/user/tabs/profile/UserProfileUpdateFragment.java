package ro.solutions.rr.flightplan.ui.mainNav.user.tabs.profile;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.solutions.rr.flightplan.R;
import ro.solutions.rr.flightplan.database.models.UserAccount;
import ro.solutions.rr.flightplan.ui.mainNav.main.MainActivityViewModel;
import ro.solutions.rr.flightplan.utils.PasswordUtils;

public class UserProfileUpdateFragment extends Fragment {

    private UserProfileViewModel mUserProfileViewModel;
    private MainActivityViewModel mMainActivityViewModel;

    private AppCompatActivity mActivity;

    @BindView(R.id.editText_new_password)
    EditText mEditTextNewPassword;
    @BindView(R.id.editText_old_password)
    EditText  mEditTextOldPassword;
    @BindView(R.id.editText_confirm_new_password)
    EditText  mEditTextConfirmNewPassword;

    @BindView(R.id.editText_first_name)
    EditText mEditeTextFirstName;
    @BindView(R.id.editText_last_name)
    EditText mEditTextLastName;
    @BindView(R.id.edit_text_email)
    EditText mEditTextEmail;
    @BindView(R.id.editText_copy_email)
    EditText mEditTextCCopyEmail;

    @BindView(R.id.progress_bar_loading_user_account_data)
    ProgressBar mProgressBarLoading;
    @BindView(R.id.main_profile_settings)
    LinearLayout mMainProfileSettingsLayout;
    @BindView(R.id.security_profile_settings)
    LinearLayout mSecurityProfileSettingsLayout;

    private MutableLiveData<Boolean> mSaveUserProfile;
    private MutableLiveData<String> mOperationMessage;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings_user, container, false);
        ButterKnife.bind(this, rootView);
        setHasOptionsMenu(true);

        mUserProfileViewModel = ViewModelProviders.of(this).get(UserProfileViewModel.class);
        mMainActivityViewModel = ViewModelProviders.of(mActivity).get(MainActivityViewModel.class);

        initViewModelObservers();
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity)context;
    }

    private void initViewModelObservers() {
        mUserProfileViewModel.getLoadingStatus().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean shouldShowLoading) {
                if (shouldShowLoading != null) {
                    if (shouldShowLoading) {
                        mProgressBarLoading.setVisibility(View.VISIBLE);
                        mMainProfileSettingsLayout.setVisibility(View.INVISIBLE);
                        mSecurityProfileSettingsLayout.setVisibility(View.INVISIBLE);
                    } else {
                        mProgressBarLoading.setVisibility(View.INVISIBLE);
                        mMainProfileSettingsLayout.setVisibility(View.VISIBLE);
                        mSecurityProfileSettingsLayout.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        mUserProfileViewModel.getUserAccount().observe(this, new Observer<UserAccount>() {
            @Override
            public void onChanged(@Nullable UserAccount userAccount) {
                if (userAccount != null) {
                    initUserDetails(userAccount);
                }
            }
        });
        mOperationMessage = mUserProfileViewModel.getOperationsMessage();
        mOperationMessage.observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String msg) {
                if (msg != null) {
                    if (!msg.isEmpty()) {
                        displayToast(msg);
                    }
                }
            }
        });
        mSaveUserProfile = mMainActivityViewModel.getSaveUserAccountData();
        mSaveUserProfile.observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean saveUserData) {
                if (saveUserData != null && saveUserData) {
                   saveUserProfileInformation();
                   mSaveUserProfile.setValue(false);
                }
            }
        });
    }

    private void initUserDetails(@NonNull UserAccount userAccount) {
        mEditeTextFirstName.setText(userAccount.getmFirstName());
        mEditTextLastName.setText(userAccount.getmLastName());
        mEditTextEmail.setText(userAccount.getmUserEmail());
        mEditTextCCopyEmail.setText(userAccount.getCcEmail());
        try {
            String pass = PasswordUtils.decodePassword(userAccount.getPassword());
            mEditTextOldPassword.setText(pass);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void saveUserProfileInformation() {
        String firstName = mEditeTextFirstName.getText().toString();
        String lastName = mEditTextLastName.getText().toString();
        String cCopyEmail = mEditTextCCopyEmail.getText().toString();

        String oldPass = mEditTextOldPassword.getText().toString();
        String newPass = mEditTextNewPassword.getText().toString();
        String confirmNewPass = mEditTextConfirmNewPassword.getText().toString();
        mUserProfileViewModel.saveProfileUpdates(firstName, lastName, cCopyEmail, oldPass, newPass, confirmNewPass);
    }

    private void displayToast(String msg){
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
