package ro.solutions.rr.flightplan.database.models;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

import java.util.List;

public class TripDetails {

    private int id;

    @Embedded
    Trip trip;

    @Relation(parentColumn = "id" , entityColumn = "planId")
    public List<TripFrom> mTripFrom;

    @Relation(parentColumn = "id" , entityColumn = "planId")
    public List<TripTo> mTripTo;

}
