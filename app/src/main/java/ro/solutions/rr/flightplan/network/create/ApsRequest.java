package ro.solutions.rr.flightplan.network.create;

import com.google.gson.annotations.SerializedName;

public class ApsRequest {

    @SerializedName("send_email_to_carabinieri")
    private boolean mSendEmailToCarabinieri;

    @SerializedName("mail")
    private String mMail;

    @SerializedName("carabinieri_name")
    private String mCarabinieriName;

    public boolean ismSendEmailToCarabinieri() {
        return mSendEmailToCarabinieri;
    }

    public void setmSendEmailToCarabinieri(boolean mSendEmailToCarabinieri) {
        this.mSendEmailToCarabinieri = mSendEmailToCarabinieri;
    }

    public String getmMail() {
        return mMail;
    }

    public void setmMail(String mMail) {
        this.mMail = mMail;
    }

    public String getmCarabinieriName() {
        return mCarabinieriName;
    }

    public void setmCarabinieriName(String mCarabinieriName) {
        this.mCarabinieriName = mCarabinieriName;
    }
}
