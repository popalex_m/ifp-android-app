package ro.solutions.rr.flightplan.utils;


import android.util.Base64;

import java.io.UnsupportedEncodingException;

public class PasswordUtils  {

    public static String encodePassword (String unEncodedPass) throws UnsupportedEncodingException {
        byte[] data = unEncodedPass.getBytes("UTF-8");
        return  Base64.encodeToString(data, Base64.DEFAULT);
    }

    public static String decodePassword (String encodedValue) throws UnsupportedEncodingException {
        byte[] data = (Base64.decode(encodedValue, Base64.DEFAULT));
        return new String(data , "UTF-8");
    }
}
