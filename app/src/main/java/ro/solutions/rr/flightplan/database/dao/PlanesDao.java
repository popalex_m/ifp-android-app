package ro.solutions.rr.flightplan.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import ro.solutions.rr.flightplan.database.models.Plane;

@Dao
public interface PlanesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMultiplePlanes(Plane... plane);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSinglePlane(Plane plane);

    @Query("SELECT COUNT(*) FROM table_planes")
    int planesRowCount();

    @Query("DELETE FROM table_planes")
    int deleteAllPlanes();

    @Delete
    int deleteSinglePlane(Plane plane);

    @Update
    int updateSinglePlane(Plane plane);

    @Query("SELECT * from table_planes")
    LiveData<List<Plane>> getAllPlanes();
}
