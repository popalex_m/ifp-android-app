package ro.solutions.rr.flightplan.ui.mainNav.planes;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.solutions.rr.flightplan.R;
import ro.solutions.rr.flightplan.database.models.Plane;
import ro.solutions.rr.flightplan.ui.mainNav.ViewAnimations;

public class PlanesRecyclerAdapter extends RecyclerView.Adapter<PlanesRecyclerAdapter.ViewHolder> {

    private List<Plane> mCurrentPlaneList;
    private Boolean mEditMode;
    private EditPlaneCallback mCallbackEditPilot;

    PlanesRecyclerAdapter (@NonNull List<Plane> mCurrentPlanesList, @NonNull EditPlaneCallback callback) {
        this.mCurrentPlaneList = mCurrentPlanesList;
        this.mCallbackEditPilot = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_planes, parent, false);
        return new PlanesRecyclerAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
          int pos = holder.getAdapterPosition();
          holder.mPlaneName.setText(mCurrentPlaneList.get(pos).getName());if (mEditMode) {
            ViewAnimations.getViewAnimator().startRevealAnimation(holder.mEditPlanesCardView, holder.mEditButtons);
        } else {
            ViewAnimations.getViewAnimator().startHideRevealAnimation(holder.mEditPlanesCardView, holder.mEditButtons);
        }
    }

    @Override
    public int getItemCount() {
        return mCurrentPlaneList.size() ;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.plane_name)
        TextView mPlaneName;
        @BindView(R.id.btn_edit_pilot)
        ImageButton mEditPlaneData;
        @BindView(R.id.btn_delete_pilot)
        ImageButton mDeletePlane;

        @BindView(R.id.cardview_edit_pilots)
        CardView mEditPlanesCardView;
        @BindView(R.id.edit_buttons_layout)
        LinearLayout mEditButtons;

        ViewHolder(@NonNull View view){
            super(view);
            ButterKnife.bind(this, view);
            mEditPlaneData.setOnClickListener(this);
            mDeletePlane.setOnClickListener(this);
        }

        @Override
        public void onClick(@NonNull View view) {
            int position = getAdapterPosition();
            Plane pilot = mCurrentPlaneList.get(position);
            switch (view.getId()) {
                case R.id.btn_edit_pilot :
                    if (mCallbackEditPilot != null) {
                        mCallbackEditPilot.onPlaneEditCallback(pilot);
                    }
                    break;

                case R.id.btn_delete_pilot :
                    if (mCallbackEditPilot != null) {
                        mCallbackEditPilot.onPlaneDeleteCallback(pilot);
                    }
                    break;
            }
        }
    }

    public void setEditMode(Boolean value) {
        this.mEditMode = value;
    }

    public void addItem(Plane plane) {
        this.mCurrentPlaneList.add(plane);
    }
}
