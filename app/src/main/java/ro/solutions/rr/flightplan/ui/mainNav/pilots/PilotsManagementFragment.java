package ro.solutions.rr.flightplan.ui.mainNav.pilots;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.solutions.rr.flightplan.R;
import ro.solutions.rr.flightplan.database.models.Pilot;
import ro.solutions.rr.flightplan.ui.mainNav.OperationTypes;
import ro.solutions.rr.flightplan.ui.mainNav.dialogs.AddEditPilotDialogFragment;
import ro.solutions.rr.flightplan.ui.mainNav.dialogs.AddEditPilotDialogListener;
import ro.solutions.rr.flightplan.utils.DialogUtils;

import static ro.solutions.rr.flightplan.ui.mainNav.OperationTypes.CREATE_NEW_PILOT;
import static ro.solutions.rr.flightplan.ui.mainNav.OperationTypes.EDIT_EXISTING_PILOT;

public class PilotsManagementFragment extends Fragment implements
                                                         AddEditPilotDialogListener,
                                                         View.OnClickListener,
                                                         EditPilotCallback {

    private String TAG = PilotsManagementFragment.class.getSimpleName();

    private int PILOTS_FRAGMENT_REQ_CODE = 300;
    private String EDIT_MODE_STATE = "EDIT_MODE_STATE";

    @BindView(R.id.recycler_pilots_list)
    RecyclerView mRecyclerPilotsList;
    @BindView(R.id.fabAddPilot)
    FloatingActionButton mFabAddPilots;
    @BindView(R.id.progressBarPilotsFrag)
    ProgressBar mProgressBarPilots;

    private FragmentManager mFragmentManager;
    private PilotsRecyclerAdapter mPilotsAdapter;
    private PilotsManagementViewModel mPilotsManagementViewModel;
    private AppCompatActivity mActivity;
    private Boolean mEditMode = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pilots_management, container, false);
        ButterKnife.bind(this, rootView);

        if (savedInstanceState != null) {
            mEditMode = savedInstanceState.getBoolean(EDIT_MODE_STATE);
        }

        mFragmentManager = getFragmentManager();
        mPilotsManagementViewModel = ViewModelProviders.of(this).get(PilotsManagementViewModel.class);
        initViewModelObservers();
        initListeners();
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity)context;
        mActivity.setTitle("Pilots Management");
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putBoolean(EDIT_MODE_STATE, mEditMode);
        super.onSaveInstanceState(outState);
    }

    private void initListeners(){
        mFabAddPilots.setOnClickListener(this);
    }

    private void initViewModelObservers() {
        mPilotsManagementViewModel.getCurrentPilotsInDb().observe(this, new Observer<List<Pilot>>() {
            @Override
            public void onChanged(@Nullable List<Pilot> pilots) {
                if (pilots != null) {
                    initRecyclerView(pilots);
                }
            }
        });
        mPilotsManagementViewModel.getRequestStatus().observe(this, new Observer<HashMap<Boolean, String>>() {
            @Override
            public void onChanged(@Nullable HashMap<Boolean, String> requestStatus) {
                if (requestStatus != null) {
                    showMessageToast(requestStatus.get(true));
                }
            }
        });
        mPilotsManagementViewModel.getShouldShowLoadingDialog().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean shouldShowLoadingDialog) {
                if (shouldShowLoadingDialog != null) {
                    if (shouldShowLoadingDialog) {
                       showLoading(true);
                    } else {
                        showLoading(false);
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        int id = item.getItemId();
        switch (id) {
            case R.id.action_edit :
                if (mEditMode != null) {
                    if (mEditMode) {
                        mEditMode = false;
                        mPilotsAdapter.setEditMode(false);
                        item.setIcon(ContextCompat.getDrawable(mActivity, R.drawable.ic_edit_white_24dp));
                    } else {
                        mEditMode = true;
                        mPilotsAdapter.setEditMode(true);
                        item.setIcon(ContextCompat.getDrawable(mActivity, R.drawable.ic_edit_accent_24dp));
                    }
                    refreshRecyclerView();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (mEditMode) {
            menu.getItem(0).setIcon(ContextCompat.getDrawable(mActivity, R.drawable.ic_edit_accent_24dp));
        } else {
            menu.getItem(0).setIcon(ContextCompat.getDrawable(mActivity, R.drawable.ic_edit_white_24dp));
        }
        menu.getItem(1).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void openAddPilotFragment(OperationTypes operationType, @Nullable Pilot pilots) {
        AddEditPilotDialogFragment addEditPilotDialogFragment = new AddEditPilotDialogFragment();
        if (operationType == EDIT_EXISTING_PILOT) {
            if (pilots != null) {
                addEditPilotDialogFragment.setPilot(pilots);
                addEditPilotDialogFragment.setOperationType(EDIT_EXISTING_PILOT);
            }
        } else {
            addEditPilotDialogFragment.setOperationType(CREATE_NEW_PILOT);
        }
        addEditPilotDialogFragment.setTargetFragment(PilotsManagementFragment.this, PILOTS_FRAGMENT_REQ_CODE);
        addEditPilotDialogFragment.show(mFragmentManager , "");
    }

    private void initRecyclerView (@NonNull List<Pilot> pilots) {
        mPilotsAdapter = new PilotsRecyclerAdapter(pilots , this);
        mPilotsAdapter.setEditMode(mEditMode);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerPilotsList.setLayoutManager(mLayoutManager);
        mRecyclerPilotsList.setAdapter(mPilotsAdapter);
        attachRecyclerScrollListener();
    }

    private void attachRecyclerScrollListener(){
        mRecyclerPilotsList.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                if (dy > 0)
                    mFabAddPilots.hide();
                else if (dy < 0)
                    mFabAddPilots.show();
            }
        });
    }

    @Override
    public void onPilotAdded(Pilot pilot) {
        mPilotsManagementViewModel.addPilotRemote(pilot);
    }

    @Override
    public void onPilotEdited(Pilot pilot) {
        mPilotsManagementViewModel.updateRemotePilot(pilot);
    }

    private void refreshRecyclerView() {
        mPilotsAdapter.notifyDataSetChanged();
        mRecyclerPilotsList.invalidate();
    }

    private void showMessageToast(@NonNull String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabAddPilot :
                openAddPilotFragment(CREATE_NEW_PILOT,null);
                break;
        }
    }

    @Override
    public void onPilotEditDataClick(Pilot pilot) {
        openAddPilotFragment(EDIT_EXISTING_PILOT, pilot);
    }

    @Override
    public void onPilotDeleteClick(Pilot pilots) {
        DialogUtils.getYesNoAlertDialog(getContext() , "Are you sure you want to delete this pilot ?" , "Confirm or deny")
                .setPositiveButton("Ok", (dialogInterface, i)
                        -> mPilotsManagementViewModel.deletePilotRemote(pilots)).show();
    }

    private void showLoading(boolean status){
        if (status){
            mProgressBarPilots.setVisibility(View.VISIBLE);
            mRecyclerPilotsList.setVisibility(View.INVISIBLE);
            mFabAddPilots.setVisibility(View.INVISIBLE);
        } else {
            mProgressBarPilots.setVisibility(View.INVISIBLE);
            mRecyclerPilotsList.setVisibility(View.VISIBLE);
            mFabAddPilots.setVisibility(View.VISIBLE);
        }
    }
}
