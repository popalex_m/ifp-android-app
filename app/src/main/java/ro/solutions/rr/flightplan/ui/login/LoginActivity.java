package ro.solutions.rr.flightplan.ui.login;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import butterknife.ButterKnife;
import ro.solutions.rr.flightplan.R;
import ro.solutions.rr.flightplan.database.models.UserAccount;
import ro.solutions.rr.flightplan.ui.mainNav.main.MainActivity;

public class LoginActivity extends AppCompatActivity {

    private String TAG = LoginActivity.class.getSimpleName();

    private FragmentManager mFragmentManager;
    private LoginActivityViewModel mLoginActivityViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        int color = getResources().getColor(R.color.colorPrimary);
        if (Build.VERSION.SDK_INT >- 21) {
            Window win = this.getWindow();
            win.getAttributes().systemUiVisibility |=
                    (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            win.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            win.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            win.setStatusBarColor(color);
        }

        mFragmentManager = getSupportFragmentManager();
        mLoginActivityViewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(getApplication()).create(LoginActivityViewModel.class);
        mLoginActivityViewModel.getFirstUserAccount().observe(this, new Observer<UserAccount>() {
            @Override
            public void onChanged(@Nullable UserAccount userAccount) {
                Log.d(TAG, "Observing changes in user account state");
                if (userAccount != null) {
                   switchToMainActivity();
                } else {
                    switchToMainLogin();
                }
            }
        });
    }

    private void switchToMainLogin(){
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_login_fragment_container, new LoginFragment()).commit();
    }

    private void switchToMainActivity() {
        Intent mainActivityIntent = new Intent(this, MainActivity.class);
        startActivity(mainActivityIntent);
    }
}
