package ro.solutions.rr.flightplan.database.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "table_trip_from")
public class TripFrom {

    @PrimaryKey
    @Expose
    private Integer id;

    private int planId;

    @SerializedName("type")
    private int tripFromType;
    @SerializedName("info")
    private String tripFromInfo;

    public TripFrom() { }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getTripFromType() {
        return tripFromType;
    }

    public void setTripFromType(int tripFromType) {
        this.tripFromType = tripFromType;
    }

    public String getTripFromInfo() {
        return tripFromInfo;
    }

    public void setTripFromInfo(String tripFromInfo) {
        this.tripFromInfo = tripFromInfo;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }
}
