package ro.solutions.rr.flightplan.network.account;

import com.google.gson.annotations.SerializedName;

public class UserLoginRequest {

    @SerializedName("email")
    private String mEmail;
    @SerializedName("pass")
    private String mPassword;

    public UserLoginRequest(String mEmail, String mPassword) {
        this.mEmail = mEmail;
        this.mPassword = mPassword;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmPassword() {
        return mPassword;
    }

    public void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }
}
