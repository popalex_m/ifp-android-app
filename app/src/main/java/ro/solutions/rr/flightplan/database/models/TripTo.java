package ro.solutions.rr.flightplan.database.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "table_trip_to")
public class TripTo {

    @PrimaryKey
    @Expose
    private Integer id;

    private int planId;

    @SerializedName("type")
    private int tripToType;
    @SerializedName("info")
    private String tripToInfo;
    @SerializedName("lat")
    private Double tripToLatitude;
    @SerializedName("long")
    private Double tripToLongitude;

    public TripTo() { }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getTripToType() {
        return tripToType;
    }

    public void setTripToType(int tripToType) {
        this.tripToType = tripToType;
    }

    public String getTripToInfo() {
        return tripToInfo;
    }

    public void setTripToInfo(String tripToInfo) {
        this.tripToInfo = tripToInfo;
    }

    public Double getTripToLatitude() {
        return tripToLatitude;
    }

    public void setTripToLatitude(Double tripToLatitude) {
        this.tripToLatitude = tripToLatitude;
    }

    public Double getTripToLongitude() {
        return tripToLongitude;
    }

    public void setTripToLongitude(Double tripToLongitude) {
        this.tripToLongitude = tripToLongitude;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }
}
