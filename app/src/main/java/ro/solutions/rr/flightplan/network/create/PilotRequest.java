package ro.solutions.rr.flightplan.network.create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PilotRequest {

    @SerializedName("name")
    @Expose
    private String pilotName;
    @SerializedName("license_nr")
    @Expose
    private String licenseNr;
    @SerializedName("phone_nr")
    @Expose
    private String phoneNr;

    public String getPilotName() {
        return pilotName;
    }

    public void setPilotName(String pilotName) {
        this.pilotName = pilotName;
    }

    public String getLicenseNr() {
        return licenseNr;
    }

    public void setLicenseNr(String licenseNr) {
        this.licenseNr = licenseNr;
    }

    public String getPhoneNr() {
        return phoneNr;
    }

    public void setPhoneNr(String phoneNr) {
        this.phoneNr = phoneNr;
    }
}
