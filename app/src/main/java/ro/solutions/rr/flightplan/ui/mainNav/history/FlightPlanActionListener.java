package ro.solutions.rr.flightplan.ui.mainNav.history;

public interface FlightPlanActionListener {

    void onFlightPlanCancelled(int planId);

    void onFlightPlanRescheduled(int planId);
}
