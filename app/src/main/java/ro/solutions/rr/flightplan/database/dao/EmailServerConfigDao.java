package ro.solutions.rr.flightplan.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import ro.solutions.rr.flightplan.database.models.EmailServerConfig;


@Dao
public interface EmailServerConfigDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertEmailServerConfig (EmailServerConfig emailServerConfig);

    @Update
    void updateEmailServerConfig (EmailServerConfig emailServerConfig);

    @Query("SELECT * FROM table_email_server LIMIT 1")
    LiveData<EmailServerConfig> getEmailServerDetailsLive();

    @Query("SELECT COUNT(*) FROM table_email_server")
    int pilotsRowCount();

    @Query("SELECT * FROM table_email_server LIMIT 1")
    EmailServerConfig getEmailServerDetails();

    @Query("DELETE FROM table_email_server")
    int deleteAllEmailConfiguration();
}
