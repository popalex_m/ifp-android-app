package ro.solutions.rr.flightplan.utils;

import android.support.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimeUtils {

    private static String TAG = TimeUtils.class.getSimpleName();

    private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

    @NonNull
    public static String formatDate(int day, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        return format.format(calendar.getTime());
    }

    @NonNull
    public static String formatAutonomy(int hours, int minutes) {
        String formatedAutonomy = hours + " H" + minutes + " M";
        return formatedAutonomy;
    }

    public static boolean isDateBeforeCurrent(@NonNull String takeOffDateString) {
        try {
            Date takeOffDateTime = format.parse(takeOffDateString);

            long currentTimeInMs = System.currentTimeMillis();
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(currentTimeInMs);
            String currentTime = format.format(calendar.getTime());
            Date currentDateTime = format.parse(currentTime);

            return takeOffDateTime.after(currentDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }
}
