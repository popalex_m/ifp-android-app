package ro.solutions.rr.flightplan.network.create;

import com.google.gson.annotations.SerializedName;


public class CreatePlanRequest {

    @SerializedName("session")
    private String mSessionTkn;

    @SerializedName("email_config")
    private EmailServerConfigRequest mEmailConfig;

    @SerializedName("plan")
    private PlanRequest mPlanRequest;

    public String getmSessionTkn() {
        return mSessionTkn;
    }

    public void setmSessionTkn(String mSessionTkn) {
        this.mSessionTkn = mSessionTkn;
    }

    public EmailServerConfigRequest getmEmailConfig() {
        return mEmailConfig;
    }

    public void setmEmailConfig(EmailServerConfigRequest mEmailConfig) {
        this.mEmailConfig = mEmailConfig;
    }

    public PlanRequest getmPlanRequest() {
        return mPlanRequest;
    }

    public void setmPlanRequest(PlanRequest mPlanRequest) {
        this.mPlanRequest = mPlanRequest;
    }
}
