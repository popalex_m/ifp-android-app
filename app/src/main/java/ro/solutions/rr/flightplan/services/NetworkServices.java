package ro.solutions.rr.flightplan.services;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.QueryMap;
import ro.solutions.rr.flightplan.network.account.UserDetailsResponse;
import ro.solutions.rr.flightplan.network.account.UserLoginRequest;
import ro.solutions.rr.flightplan.network.account.UserPasswordUpdateRequest;
import ro.solutions.rr.flightplan.network.account.UserProfileUpdateRequest;
import ro.solutions.rr.flightplan.network.account.UserSignUpRequest;
import ro.solutions.rr.flightplan.network.create.CreatePlanReponse;
import ro.solutions.rr.flightplan.network.create.CreatePlanRequest;
import ro.solutions.rr.flightplan.network.create.DeletePlanRequest;
import ro.solutions.rr.flightplan.network.create.PlanHistoryResponse;
import ro.solutions.rr.flightplan.network.pilot.CreateEditPilotRequest;
import ro.solutions.rr.flightplan.network.pilot.CreatePilotResponse;
import ro.solutions.rr.flightplan.network.plane.CreateEditPlaneRequest;
import ro.solutions.rr.flightplan.network.plane.CreateEditPlaneResponse;

public interface NetworkServices {

    @POST("/users/register")
    Observable<Response<ResponseBody>> createUserAccount(@Body UserSignUpRequest userSignUpRequest);

    @PUT("users/update/profile")
    Observable<Response<ResponseBody>> updateUserAccount(@Body UserProfileUpdateRequest userProfileUpdateRequest);

    @PUT("users/update/password")
    Observable<Response<ResponseBody>> updateUserPassword(@Body UserPasswordUpdateRequest userProfileUpdateRequest);

    @POST("/users/login")
    Observable<Response<UserDetailsResponse>> login(@Body UserLoginRequest userLoginRequest);

    @POST("/pilots/")
    Observable<Response<CreatePilotResponse>> addNewPilot(@Body CreateEditPilotRequest pilotDetails);

    @HTTP(method = "DELETE", path = "/pilots/", hasBody = true)
    Observable<Response<ResponseBody>> deletePilot (@Body CreateEditPilotRequest pilotDetails);

    @PUT("/pilots/")
    Observable<Response<ResponseBody>> updatePilot(@Body CreateEditPilotRequest pilotDetails);

    @POST("/planes/")
    Observable<Response<CreateEditPlaneResponse>> addNewPlane(@Body CreateEditPlaneRequest pilotDetails);

    @HTTP(method = "DELETE", path = "/planes/", hasBody = true)
    Observable<Response<ResponseBody>> deletePlane(@Body CreateEditPlaneRequest pilotDetails);

    @POST("/planes/")
    Observable<Response<ResponseBody>> updatePlane(@Body CreateEditPlaneRequest pilotDetails);

    @POST("/plans/")
    Observable<Response<CreatePlanReponse>> createFlightPlan(@Body CreatePlanRequest createFlightPlan);

    @GET("/plans/")
    Observable<Response<List<PlanHistoryResponse>>> getPreviouslyCreatedPlans (@QueryMap Map<String, String> params);

    @HTTP(method = "DELETE", path = "/plans/", hasBody = true)
    Observable<Response<ResponseBody>> cancelFlightPlan(@Body DeletePlanRequest deletePlanRequest);

}
