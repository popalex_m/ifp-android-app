package ro.solutions.rr.flightplan.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import ro.solutions.rr.flightplan.database.models.Pilot;

@Dao
public interface PilotsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSinglePilot (Pilot pilot);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMultiplePilots (List<Pilot> planes);

    @Query("SELECT * FROM table_pilots")
    LiveData<List<Pilot>> getAllPilots();

    @Query("SELECT COUNT(*) FROM table_pilots")
    int pilotsRowCount();

    @Delete
    void deleteSinglePilot(Pilot pilot);

    @Update
    void updateSinglePilot(Pilot pilot);

    @Query("DELETE FROM table_pilots")
    int deleteAllPilots();

}
