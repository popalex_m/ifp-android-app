package ro.solutions.rr.flightplan.ui.login;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.solutions.rr.flightplan.R;
import ro.solutions.rr.flightplan.ui.mainNav.main.MainActivity;

public class LoginFragment extends Fragment implements View.OnClickListener{

    private String TAG = LoginFragment.class.getSimpleName();

    @BindView(R.id.btn_sign_up)
    Button mBtnSignUp;
    @BindView(R.id.btn_login)
    Button mBtnLogin;

    @BindView(R.id.edit_text_email)
    EditText mEditTextEmail;
    @BindView(R.id.edit_text_password)
    EditText mEditTextPassword;

    @BindView(R.id.main_login_Layout)
    RelativeLayout mRelativeLayoutMainLogin;
    @BindView(R.id.logging_in_progress_bar)
    ProgressBar mLoggingInProgressBar;

    private FragmentManager mFragmentManager;
    private LoginFragmentViewModel mLoginFragmentViewModel;

    private String mEmail;
    private String mPassword;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_login, container, false);
        ButterKnife.bind(this, rootView);
        mFragmentManager = getFragmentManager();
        mBtnSignUp.setOnClickListener(this);
        mBtnLogin.setOnClickListener(this);

        mLoginFragmentViewModel = ViewModelProviders.of(this).get(LoginFragmentViewModel.class);
        registerViewModelObservables();
        return rootView;
    }

    private void registerViewModelObservables() {
        mLoginFragmentViewModel.loginRequestInProgress().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean mLoginRequest) {
                if (mLoginRequest != null) {
                    if (mLoginRequest) {
                        showLoadingSpinner(true);
                    } else {
                        showLoadingSpinner(false);
                    }
                }
            }
        });

        mLoginFragmentViewModel.isUserDataRetrievalComplete().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean mDataSyncComplete) {
                if (mDataSyncComplete != null) {
                    if (mDataSyncComplete) {
                        showMessageToast("Sign in successful !");
                        switchToMainActivity();
                    }
                }
            }
        });

        mLoginFragmentViewModel.getLoginRequestMessage().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String loginMessage) {
                if (loginMessage != null) {
                    showMessageToast(loginMessage);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sign_up :
                switchToSignUpFragment();
                break;

            case R.id.btn_login :
                boolean areDetailsFilled = getLoginDetails();
                if (areDetailsFilled) {
                   mLoginFragmentViewModel.startLoginRequest(mEmail , mPassword);
                }
                break;
        }
    }

    private void switchToSignUpFragment() {
        mFragmentManager.beginTransaction()
                .replace(R.id.main_login_fragment_container, new SignUpFragment())
                .commit();
    }

    private boolean getLoginDetails() {
       mEmail =  mEditTextEmail.getText().toString();
       mPassword =  mEditTextPassword.getText().toString();
       if (!mEmail.isEmpty() && !mPassword.isEmpty()) {
           Log.d(TAG , "Details are complete");
           return true;
       } else {
           Log.d(TAG , "Details are not completed");
           return false;
       }
    }

    private void showMessageToast(@NonNull String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    private void showLoadingSpinner (boolean status) {
        if (status) {
            Log.w(TAG , "Showing login loading progress");
            mRelativeLayoutMainLogin.setVisibility(View.INVISIBLE);
            mLoggingInProgressBar.setVisibility(View.VISIBLE);
        } else {
            Log.w(TAG , "Hiding login loading progress");
            mRelativeLayoutMainLogin.setVisibility(View.VISIBLE);
            mLoggingInProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    private void switchToMainActivity() {
        Intent switchToMainActivity =  new Intent(getActivity(), MainActivity.class );
        startActivity(switchToMainActivity);
        if (getActivity() != null) {
            getActivity().finish();
        }
    }
}
