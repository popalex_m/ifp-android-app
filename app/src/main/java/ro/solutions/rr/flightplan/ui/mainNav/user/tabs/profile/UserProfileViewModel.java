package ro.solutions.rr.flightplan.ui.mainNav.user.tabs.profile;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.UnsupportedEncodingException;

import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import okhttp3.ResponseBody;
import retrofit2.Response;
import ro.solutions.rr.flightplan.app.MainApp;
import ro.solutions.rr.flightplan.database.dao.UserAccountDao;
import ro.solutions.rr.flightplan.database.models.UserAccount;
import ro.solutions.rr.flightplan.database.repository.RemoteRepository;
import ro.solutions.rr.flightplan.database.repository.UserAccountRepository;
import ro.solutions.rr.flightplan.network.account.UserPasswordUpdateRequest;
import ro.solutions.rr.flightplan.network.account.UserProfileUpdateRequest;
import ro.solutions.rr.flightplan.services.NetworkServices;
import ro.solutions.rr.flightplan.ui.TextValidationUtils;
import ro.solutions.rr.flightplan.utils.PasswordUtils;

public class UserProfileViewModel extends AndroidViewModel{

    private String TAG = UserProfileViewModel.class.getSimpleName();

    private MutableLiveData<Boolean> mShouldShowLoadingDialog;
    private MutableLiveData<String> mMessage;

    private UserAccountRepository mUserAccountRepository;
    private RemoteRepository mNetworkRepository;

    public UserProfileViewModel(@NonNull Application application) {
        super(application);
        UserAccountDao userAccountDao = ((MainApp) application).getDatabaseManager().getUserDao();
        NetworkServices networkServices = ((MainApp)application).getNetworkServices();
        mUserAccountRepository = new UserAccountRepository(userAccountDao);
        mNetworkRepository = new RemoteRepository(networkServices);
    }

    public MutableLiveData<Boolean> getLoadingStatus() {
        if (mShouldShowLoadingDialog == null) {
            mShouldShowLoadingDialog =  new MutableLiveData<>();
            mShouldShowLoadingDialog.setValue(false);
        } return mShouldShowLoadingDialog;
    }

    public LiveData<UserAccount> getUserAccount() {
        return mUserAccountRepository.getFistUserAccount();
    }

    public MutableLiveData<String> getOperationsMessage() {
        if (mMessage == null) {
            mMessage = new MutableLiveData<>();
            mMessage.setValue("");
        } return mMessage;
    }

    public void saveProfileUpdates(@NonNull String firstName, @NonNull String lastName, @NonNull String ccEmail, @NonNull String oldPass, @NonNull String newPass, @NonNull String confNewPass) {
        if (!oldPass.isEmpty() && !newPass.isEmpty() && !confNewPass.isEmpty()) {
            if (!TextValidationUtils.verifyPasswordMatch(newPass, confNewPass)) {
                mMessage.setValue("New password and confirmation does not match");
            } else if (!firstName.isEmpty() && !lastName.isEmpty() && !ccEmail.isEmpty()) {
                String sessionTkn = mUserAccountRepository.getLoggedInUser().getmSessionToken();

                UserPasswordUpdateRequest userPasswordUpdateRequest =  new UserPasswordUpdateRequest(sessionTkn, newPass, oldPass);
                UserProfileUpdateRequest userProfileUpdateRequest = new UserProfileUpdateRequest(sessionTkn, firstName, lastName, ccEmail);

                startUploadRequest(userPasswordUpdateRequest, userProfileUpdateRequest);
            }
        } else {
            mMessage.setValue("Please complete all of the fields to save the current settings !");
        }
    }

    private void startUploadRequest(@NonNull UserPasswordUpdateRequest passUpdReq, @NonNull UserProfileUpdateRequest profileUpdReq) {
        Log.d(TAG , "Starting upload request for new profile and password information");
        Observable<Response<ResponseBody>> profileUpdReqObs = mNetworkRepository.updateAccountDetailsReq(profileUpdReq);
        Observable<Response<ResponseBody>> passUpdReqObs = mNetworkRepository.updateAccountPasswordReq(passUpdReq);

        mShouldShowLoadingDialog.setValue(true);
        Observable.zip(profileUpdReqObs, passUpdReqObs, new BiFunction<Response<ResponseBody>, Response<ResponseBody>, Boolean>() {
            @Override
            public Boolean apply(Response<ResponseBody> responseBodyResponse, Response<ResponseBody> responseBodyResponse2) throws Exception {
                if (responseBodyResponse.code() == 200 && responseBodyResponse2.code() == 200)
                    return true;
                else {
                    return false;
                }
            }
        }).subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean reqStatus) throws Exception {
                mShouldShowLoadingDialog.setValue(false);
                if (reqStatus) {
                    updateLocalUserAccount(profileUpdReq.getmFirstName(), profileUpdReq.getmLastName(), profileUpdReq.getmCopyEmail() ,passUpdReq.getmNewPassword());
                    mMessage.setValue("Successfully updated the profile and password info");
                    Log.d(TAG , "Returned success on both status codes");
                } else {
                    mMessage.setValue("Failed to update user data");
                    Log.e(TAG , "Failed profile and pass update requests ");
                }
            }
        });
    }

    private void updateLocalUserAccount(String firstName, String lastName, String ccEmail, String newPass) throws UnsupportedEncodingException {
        Log.d(TAG , "Updating user account data locally");
        UserAccount loggedInUser = mUserAccountRepository.getLoggedInUser();
        loggedInUser.setCcEmail(ccEmail);
        String newPassEnd = PasswordUtils.encodePassword(newPass);
        loggedInUser.setPassword(newPassEnd);
        loggedInUser.setmFirstName(firstName);
        loggedInUser.setmLastName(lastName);
        mUserAccountRepository.updateUserAccount(loggedInUser);
    }
}
