package ro.solutions.rr.flightplan.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;

import ro.solutions.rr.flightplan.database.models.Legals;
import ro.solutions.rr.flightplan.database.models.Plan;
import ro.solutions.rr.flightplan.database.models.Trip;
import ro.solutions.rr.flightplan.database.models.TripFrom;
import ro.solutions.rr.flightplan.database.models.TripTo;

@Dao
public interface FlightPlanHistoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertNewFlightPlan(Plan flightPlan);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertNewLegals(Legals legals);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertNewTrip(Trip trip);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertNewTripFrom(TripFrom tripFrom);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertNewTripTo(TripTo tripTo);

}
