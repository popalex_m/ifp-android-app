package ro.solutions.rr.flightplan.network.create;

import com.google.gson.annotations.SerializedName;

public class LegalsRequest {

    @SerializedName("civil_aviation_email")
    private String mCivilAviationEmail;

    @SerializedName("civil_aviation_second_email")
    private String mCivilAviationSecondaryEmail;

    @SerializedName("targeted_ats")
    private int mTargeteted_Ats;

    @SerializedName("aps")
    private ApsRequest mApsRequest;

    public String getmCivilAviationEmail() {
        return mCivilAviationEmail;
    }

    public void setmCivilAviationEmail(String mCivilAviationEmail) {
        this.mCivilAviationEmail = mCivilAviationEmail;
    }

    public String getmCivilAviationSecondaryEmail() {
        return mCivilAviationSecondaryEmail;
    }

    public void setmCivilAviationSecondaryEmail(String mCivilAviationSecondaryEmail) {
        this.mCivilAviationSecondaryEmail = mCivilAviationSecondaryEmail;
    }

    public int getmTargeteted_Ats() {
        return mTargeteted_Ats;
    }

    public void setmTargeteted_Ats(int mTargeteted_Ats) {
        this.mTargeteted_Ats = mTargeteted_Ats;
    }

    public ApsRequest getmApsRequest() {
        return mApsRequest;
    }

    public void setmApsRequest(ApsRequest mApsRequest) {
        this.mApsRequest = mApsRequest;
    }
}
