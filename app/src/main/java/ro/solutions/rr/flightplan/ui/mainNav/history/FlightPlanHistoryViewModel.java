package ro.solutions.rr.flightplan.ui.mainNav.history;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import ro.solutions.rr.flightplan.app.MainApp;
import ro.solutions.rr.flightplan.database.dao.EmailServerConfigDao;
import ro.solutions.rr.flightplan.database.dao.UserAccountDao;
import ro.solutions.rr.flightplan.database.models.EmailServerConfig;
import ro.solutions.rr.flightplan.database.repository.EmailServerConfigRepository;
import ro.solutions.rr.flightplan.database.repository.RemoteRepository;
import ro.solutions.rr.flightplan.database.repository.UserAccountRepository;
import ro.solutions.rr.flightplan.network.create.AuthRequest;
import ro.solutions.rr.flightplan.network.create.DeletePlanRequest;
import ro.solutions.rr.flightplan.network.create.EmailServerConfigRequest;
import ro.solutions.rr.flightplan.network.create.PlanHistoryResponse;
import ro.solutions.rr.flightplan.services.NetworkServices;
import ro.solutions.rr.flightplan.utils.NetworkUtils;

public class FlightPlanHistoryViewModel extends AndroidViewModel {

    private String TAG = FlightPlanHistoryViewModel.class.getSimpleName();

    private RemoteRepository mRemoteRepository;
    private UserAccountRepository mUserAccountRepository;
    private EmailServerConfigRepository mEmailServerConfig;

    private MutableLiveData<String> mPlansHistoryRequestMessage;
    private MutableLiveData<Boolean> mShowLoadingSpinner;
    private MutableLiveData<List<PlanHistoryResponse>> mPlanHistoryDetails;

    private String mAccessToken;

    public FlightPlanHistoryViewModel(@NonNull Application application) {
        super(application);
        NetworkServices networkServices = ((MainApp)getApplication()).getNetworkServices();
        UserAccountDao userAccountDao = ((MainApp)getApplication()).getDatabaseManager().getUserDao();
        EmailServerConfigDao emailServerConfigDao = ((MainApp) getApplication()).getDatabaseManager().getEmailServerDao();

        mRemoteRepository = new RemoteRepository(networkServices);
        mUserAccountRepository = new UserAccountRepository(userAccountDao);
        mEmailServerConfig = new EmailServerConfigRepository(emailServerConfigDao);

        mAccessToken = mUserAccountRepository.getLoggedInUser().getmSessionToken();
    }

    public void initHistoryFromRemote() {
        if (NetworkUtils.isConnected(getApplication())) {
            getFlightPlanHistory();
        } else {
            mShowLoadingSpinner.setValue(false);
            mPlansHistoryRequestMessage.setValue("No internet connection available, please connect to the internet and try again !");
        }
    }

    private void getFlightPlanHistory() {
        mRemoteRepository
                .getFlightPlanHistory(mAccessToken)
                .subscribe(new Observer<Response<List<PlanHistoryResponse>>>() {
                    @Override
                    public void onSubscribe(Disposable d) { }

                    @Override
                    public void onNext(Response<List<PlanHistoryResponse>> response) {
                        int statusCode = response.code();
                        if (statusCode == 200) {
                            Log.d(TAG, "Received status code -> " + statusCode + ", getting flight plan data from response");
                            int flightPlanListSize = response.body().size();
                            if (flightPlanListSize == 0) {
                                mPlansHistoryRequestMessage.setValue("No previous plans are available !");
                            } else {
                                Log.d(TAG, "Received " + flightPlanListSize + " available flight plans from the backend");
                                setPlanHistoryDetails(response.body());

                            }
                        } else {
                            mPlansHistoryRequestMessage.setValue("Successfully retrieved your flight plan history !");
                            Log.e(TAG , "Received error code -> " + statusCode);
                            mPlansHistoryRequestMessage.setValue("Server error, could not retrieve flight plan history");
                        }
                        mShowLoadingSpinner.setValue(false);
                    }

                    @Override
                    public void onError(Throwable e) { e.printStackTrace(); }

                    @Override
                    public void onComplete() { }
                });
    }

    public void deletePlanRequest(int plainId) {
        mShowLoadingSpinner.setValue(true);

        DeletePlanRequest deletePlanRequest = getDeletePlanRequest(plainId);
        mRemoteRepository
                .cancelFlightPlan(deletePlanRequest)
                .subscribe(new Observer<Response<ResponseBody>>() {
            @Override
            public void onSubscribe(Disposable d) { }

            @Override
            public void onNext(Response<ResponseBody> deletePlanResponseResponse) {
                mPlansHistoryRequestMessage.setValue("Successfully cancelled this flight plan");
                mShowLoadingSpinner.setValue(false);
                initHistoryFromRemote();
            }

            @Override
            public void onError(Throwable e) {
                mShowLoadingSpinner.setValue(false);
                mPlansHistoryRequestMessage.setValue("Error while trying to cancel the plan");
                e.printStackTrace(); }

            @Override
            public void onComplete() { }
        });
    }

    private DeletePlanRequest getDeletePlanRequest (int planId) {
        DeletePlanRequest deletePlanRequest = new DeletePlanRequest();
        deletePlanRequest.setSessionToken(mAccessToken);

        EmailServerConfig emailServerConfig = mEmailServerConfig.getEmailServerConfig();

        EmailServerConfigRequest emailServerConfigRequest = new EmailServerConfigRequest();
        emailServerConfigRequest.setmHost(emailServerConfig.getHost());
        emailServerConfigRequest.setmPort(emailServerConfig.getPort());
        emailServerConfigRequest.setmSecure(emailServerConfig.isSecure());

        AuthRequest authRequest = new AuthRequest();
        if (emailServerConfig.isSecure()) {
            authRequest.setPass(emailServerConfig.getEmailServerAuthPassword());
            authRequest.setUser(emailServerConfig.getEmailServerAuthUsername());
            emailServerConfigRequest.setmFrom(emailServerConfig.getFrom());
        } else {
            authRequest.setUser("");
            authRequest.setPass("");
            emailServerConfigRequest.setmFrom("");
        }
        emailServerConfigRequest.setmAuthRequest(authRequest);

        deletePlanRequest.setEmailServerConfigRequest(emailServerConfigRequest);
        deletePlanRequest.setPlanId(planId);
        return deletePlanRequest;
    }

    public MutableLiveData<String> getPlansHistoryRequestMessage() {
        if (mPlansHistoryRequestMessage == null) {
            mPlansHistoryRequestMessage = new MutableLiveData<>();
        } return mPlansHistoryRequestMessage;
    }

    public MutableLiveData<Boolean> getShowLoadingSpinner() {
        if (mShowLoadingSpinner == null) {
            mShowLoadingSpinner = new MutableLiveData<>();
        } return mShowLoadingSpinner;
    }

    public MutableLiveData<List<PlanHistoryResponse>> getPlanHistoryDetails() {
        if (mPlanHistoryDetails == null) {
            mPlanHistoryDetails = new MutableLiveData<>();
        } return mPlanHistoryDetails;
    }

    private void setPlanHistoryDetails(List<PlanHistoryResponse> mPlanHistoryDetails) {
        this.mPlanHistoryDetails.setValue(mPlanHistoryDetails);
    }
}
