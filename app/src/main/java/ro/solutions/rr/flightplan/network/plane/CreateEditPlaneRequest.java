package ro.solutions.rr.flightplan.network.plane;

import com.google.gson.annotations.SerializedName;

import ro.solutions.rr.flightplan.database.models.Plane;

public class CreateEditPlaneRequest {

    @SerializedName("session")
    String mSession;
    @SerializedName("plane")
    Plane mPlane;

    public String getmSession() { return mSession; }

    public void setmSession(String mSession) { this.mSession = mSession; }

    public Plane getmPlane() { return mPlane; }

    public void setmPlane(Plane mPlane) { this.mPlane = mPlane; }
}
