package ro.solutions.rr.flightplan.database.repository;

import android.arch.lifecycle.LiveData;
import android.util.Log;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;
import ro.solutions.rr.flightplan.database.dao.PilotsDao;
import ro.solutions.rr.flightplan.database.models.Pilot;

public class PilotRepository {

    private String TAG = PlanesRepository.class.getSimpleName();

    private PilotsDao mPilotsDao;

    public PilotRepository (PilotsDao pilotsDao) { mPilotsDao = pilotsDao; }

    public Completable insertSinglePilot (Pilot pilot) {
        return Completable.fromAction(new Action() {
            @Override
            public void run() {
                Log.d(TAG , "Inserting one pilot to DAO");
                try {
                    mPilotsDao.insertSinglePilot(pilot);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());

    }

    public void insertPilotsSync(List<Pilot> pilot) {
        mPilotsDao.insertMultiplePilots(pilot);
    }

    public void insertSinglePilotSync(Pilot pilot) {
        Log.d(TAG , "Adding  with id " + pilot.getId() + " to the database");
        mPilotsDao.insertSinglePilot(pilot);
    }

    public void deleteSinglePilotSync(Pilot pilot) {
        mPilotsDao.deleteSinglePilot(pilot);
    }

    public int deleteAllPilots() {
        return mPilotsDao.deleteAllPilots();
    }

    public void updateSinglePilotSync(Pilot pilot) {
        mPilotsDao.updateSinglePilot(pilot);
    }

    public LiveData<List<Pilot>> getAllPilots() {
        return mPilotsDao.getAllPilots();
    }
}
