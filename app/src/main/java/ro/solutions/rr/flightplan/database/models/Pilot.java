package ro.solutions.rr.flightplan.database.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "table_pilots")
public class Pilot {

    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    private Integer id;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("family_name")
    @Expose
    private String familyName;
    @SerializedName("license_nr")
    @Expose
    private String licenseNr;
    @SerializedName("phone_nr")
    @Expose
    private String phoneNr;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) { this.familyName = familyName; }

    public String getLicenseNr() {
        return licenseNr;
    }

    public void setLicenseNr(String licenseNr) {
        this.licenseNr = licenseNr;
    }

    public String getPhoneNr() {
        return phoneNr;
    }

    public void setPhoneNr(String phoneNr) {
        this.phoneNr = phoneNr;
    }
}
