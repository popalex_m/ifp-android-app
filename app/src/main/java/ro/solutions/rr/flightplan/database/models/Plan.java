package ro.solutions.rr.flightplan.database.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity (tableName = "table_plans")
public class Plan {

    @PrimaryKey
    public int id;

    public Plan() { }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

