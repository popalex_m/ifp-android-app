package ro.solutions.rr.flightplan.database.dao;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;

import ro.solutions.rr.flightplan.database.models.Legals;


@Dao
public interface LegalsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertNewLegals(Legals legals);


}
