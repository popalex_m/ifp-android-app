package ro.solutions.rr.flightplan.network.plane;

import com.google.gson.annotations.SerializedName;

public class CreateEditPlaneResponse {

    @SerializedName("id")
    Integer mNewPlaneId;

    public Integer getmNewPlaneId() { return mNewPlaneId; }

    public void setmNewPlaneId(Integer mNewPlaneId) { this.mNewPlaneId = mNewPlaneId; }
}
