package ro.solutions.rr.flightplan.network.account;

import com.google.gson.annotations.SerializedName;

public class UserProfileUpdateRequest {

    @SerializedName("session")
    private String mSessionToken;
    @SerializedName("first_name")
    private String mFirstName;
    @SerializedName("family_name")
    private String mLastName;
    @SerializedName("copy_email")
    private String mCopyEmail;

    public UserProfileUpdateRequest(String mSessionToken, String mFirstName, String mLastName, String mCopyEmail) {
        this.mSessionToken = mSessionToken;
        this.mFirstName = mFirstName;
        this.mLastName = mLastName;
        this.mCopyEmail = mCopyEmail;
    }

    public UserProfileUpdateRequest() {}

    public String getmFirstName() {
        return mFirstName;
    }

    public void setmFirstName(String mFirstName) {
        this.mFirstName = mFirstName;
    }

    public String getmLastName() {
        return mLastName;
    }

    public void setmLastName(String mLastName) {
        this.mLastName = mLastName;
    }

    public String getmCopyEmail() { return mCopyEmail; }

    public void setmCopyEmail(String mCopyEmail) { this.mCopyEmail = mCopyEmail; }

    public String getmSessionToken() { return mSessionToken; }

    public void setmSessionToken(String mSessionToken) { this.mSessionToken = mSessionToken; }
}
