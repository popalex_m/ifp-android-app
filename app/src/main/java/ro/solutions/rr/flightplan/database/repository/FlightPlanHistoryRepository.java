package ro.solutions.rr.flightplan.database.repository;

import ro.solutions.rr.flightplan.database.dao.FlightPlanHistoryDao;
import ro.solutions.rr.flightplan.database.models.Legals;
import ro.solutions.rr.flightplan.database.models.Plan;
import ro.solutions.rr.flightplan.database.models.Trip;
import ro.solutions.rr.flightplan.database.models.TripFrom;
import ro.solutions.rr.flightplan.database.models.TripTo;

public class FlightPlanHistoryRepository {

    private FlightPlanHistoryDao mFlightPlanHistoryDao;

    public FlightPlanHistoryRepository(FlightPlanHistoryDao mFlightPlanHistoryDao) {
        this.mFlightPlanHistoryDao = mFlightPlanHistoryDao;
    }

    public void insertNewFLightPlan(Plan plan, Trip trip, TripFrom tripFrom, TripTo tripTo, Legals legals) {
         mFlightPlanHistoryDao.insertNewFlightPlan(plan);
         mFlightPlanHistoryDao.insertNewLegals(legals);
         mFlightPlanHistoryDao.insertNewTrip(trip);
         mFlightPlanHistoryDao.insertNewTripFrom(tripFrom);
         mFlightPlanHistoryDao.insertNewTripTo(tripTo);
    }
}
