package ro.solutions.rr.flightplan.ui.login;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import ro.solutions.rr.flightplan.app.MainApp;
import ro.solutions.rr.flightplan.database.dao.PilotsDao;
import ro.solutions.rr.flightplan.database.dao.PlanesDao;
import ro.solutions.rr.flightplan.database.dao.UserAccountDao;
import ro.solutions.rr.flightplan.database.models.Pilot;
import ro.solutions.rr.flightplan.database.models.Plane;
import ro.solutions.rr.flightplan.database.models.UserAccount;
import ro.solutions.rr.flightplan.database.repository.PilotRepository;
import ro.solutions.rr.flightplan.database.repository.PlanesRepository;
import ro.solutions.rr.flightplan.database.repository.UserAccountRepository;
import ro.solutions.rr.flightplan.network.account.PilotResponse;
import ro.solutions.rr.flightplan.network.account.PlaneResponse;
import ro.solutions.rr.flightplan.network.account.UserDetailsResponse;
import ro.solutions.rr.flightplan.network.account.UserLoginRequest;
import ro.solutions.rr.flightplan.services.NetworkServices;
import ro.solutions.rr.flightplan.utils.NetworkUtils;
import ro.solutions.rr.flightplan.utils.PasswordUtils;

public class LoginFragmentViewModel extends AndroidViewModel {

    private String TAG = LoginFragmentViewModel.class.getSimpleName();

    private UserAccountRepository mUserAccountRepository;
    private PilotRepository mPilotRepository;
    private PlanesRepository mPlanesRepository;
    private NetworkServices mNetworkServices;

    private MutableLiveData<Boolean> mIsLoggingIn;
    private MutableLiveData<Boolean> mUserDataSyncComplete;
    private MutableLiveData<String> mLoginRequestMessage;

    private UserLoginRequest mUserLoginRequest;
    private UserAccount mUserAccount;

    public LoginFragmentViewModel(@NonNull Application application) {
        super(application);
        mNetworkServices = ((MainApp)application).getNetworkServices();

        UserAccountDao mUserAccountDao = ((MainApp) application).getDatabaseManager().getUserDao();
        PilotsDao mPilotsDao = ((MainApp) application).getDatabaseManager().getPilotsDao();
        PlanesDao mPlanesDao = ((MainApp) application).getDatabaseManager().getPlanesDao();

        mUserAccountRepository = new UserAccountRepository(mUserAccountDao);
        mPilotRepository = new PilotRepository(mPilotsDao);
        mPlanesRepository = new PlanesRepository(mPlanesDao);
    }

    public MutableLiveData<Boolean> loginRequestInProgress(){
        if (mIsLoggingIn == null) {
            mIsLoggingIn = new MutableLiveData<>();
            mIsLoggingIn.setValue(false);
        }
        return mIsLoggingIn;
    }

    public MutableLiveData<Boolean> isUserDataRetrievalComplete() {
        if (mUserDataSyncComplete == null) {
            mUserDataSyncComplete = new MutableLiveData<>();
            mUserDataSyncComplete.setValue(false);
        }
        return mUserDataSyncComplete;
    }

    public MutableLiveData<String> getLoginRequestMessage() {
        if (mLoginRequestMessage == null) {
            mLoginRequestMessage = new MutableLiveData<>();
        }
        return mLoginRequestMessage;
    }

    public void startLoginRequest(String userName, String  password) {
        if (NetworkUtils.isConnected(getApplication())) {
            mUserLoginRequest = new UserLoginRequest(userName , password);
            mNetworkServices.login(mUserLoginRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<UserDetailsResponse>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            mIsLoggingIn.postValue(true);
                        }

                        @Override
                        public void onNext(Response<UserDetailsResponse> userDetailsResponseResponse) {
                            int statusCode = userDetailsResponseResponse.code();
                            UserDetailsResponse userDetails = userDetailsResponseResponse.body();
                            if (userDetailsResponseResponse.code() == 200) {
                                Log.d(TAG , "Received login request with status code " + statusCode);
                                if (userDetails != null) {
                                    try {
                                        insertUserData(userDetails,password, userName);
                                        mIsLoggingIn.setValue(false);
                                        mLoginRequestMessage.setValue("Logging in was successful");
                                    } catch (UnsupportedEncodingException e) {
                                        mIsLoggingIn.setValue(false);
                                        mLoginRequestMessage.setValue("Login failed, bad username or password");
                                        e.printStackTrace();
                                    }
                                }
                            } else {
                                Log.e(TAG , "Received login request with status code " + statusCode);
                                mIsLoggingIn.setValue(false);
                                mLoginRequestMessage.setValue("Login failed, bad username or password");
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            mIsLoggingIn.setValue(false);
                            if (e instanceof HttpException ||
                                    e instanceof SocketTimeoutException ||
                                            e instanceof UnknownHostException ||
                                                 e instanceof IOException) {
                                mLoginRequestMessage.setValue("Server cannot be reached");
                            }
                        }
                        @Override
                        public void onComplete() { }
                    });
        } else {
            Log.d(TAG , "No internet connection available");
            mLoginRequestMessage.setValue("Please connect to the internet then try to log in");
        }
    }

    private void insertUserData(@NonNull UserDetailsResponse userDetailsResponse, @NonNull String pass, @NonNull String userName) throws UnsupportedEncodingException {
            mUserAccount = new UserAccount();
            String encodedPass = PasswordUtils.encodePassword(pass);
            mUserAccount.setmLastName(userDetailsResponse.getFamilyName());
            mUserAccount.setmFirstName(userDetailsResponse.getFirstName());
            mUserAccount.setCcEmail(userDetailsResponse.getCcEmail());
            mUserAccount.setPassword(encodedPass);
            mUserAccount.setmSessionToken(userDetailsResponse.getSession());
            mUserAccount.setmUserEmail(userName);

            mUserAccountRepository.insertUserAccountSync(mUserAccount);

            if (userDetailsResponse.getPilotResponses() != null) {
                for (PilotResponse pilotResponse : userDetailsResponse.getPilotResponses()) {
                    Pilot pilot = new Pilot();
                    pilot.setId(pilotResponse.getId());
                    pilot.setFirstName(pilotResponse.getFirstName());
                    pilot.setFamilyName(pilotResponse.getFamilyName());
                    pilot.setLicenseNr(pilotResponse.getLicenseNr());
                    pilot.setPhoneNr(pilotResponse.getPhoneNr());
                    mPilotRepository.insertSinglePilotSync(pilot);
                }
            } else {
               Log.w(TAG , "Backend did not return any pilot associated with this account");
            }
            if (userDetailsResponse.getPlaneResponses() != null) {
                for (PlaneResponse planes : userDetailsResponse.getPlaneResponses()) {
                    Plane plane = new Plane();
                    plane.setId(planes.getId());
                    plane.setName(planes.getName());
                    mPlanesRepository.insertPlaneSync(plane);
                }
            } else {
                Log.w(TAG , "Backend did not return any plane associated with this account");
            }
    }
}
