package ro.solutions.rr.flightplan.network.pilot;

import com.google.gson.annotations.SerializedName;

public class CreatePilotResponse {

    @SerializedName("id")
    Integer mNewPilotId;

    public Integer getmNewPilotId() {
        return mNewPilotId;
    }

    public void setmNewPilotId(Integer mNewPilotId) {
        this.mNewPilotId = mNewPilotId;
    }
}
