package ro.solutions.rr.flightplan.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;

import ro.solutions.rr.flightplan.R;

public class DialogUtils {

    public static AlertDialog.Builder getYesNoAlertDialog(Context ctx, String message, String title) {
        return new AlertDialog.Builder(ctx)
                .setTitle(title)
                .setMessage(message)
                .setNegativeButton(ctx.getString(R.string.label_btn_cancel), (dialog, which) -> dialog.dismiss());
    }

    public static void showOkAlertDialog(Context ctx, String title, String message) {
        new AlertDialog.Builder(ctx)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                .show();
    }

    public static ProgressDialog getLoadingDialog(Context ctx, String msg){
        ProgressDialog loading = new ProgressDialog(ctx);
        loading.setCancelable(false);
        loading.setMessage(msg);
        loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        return loading;
    }
}
