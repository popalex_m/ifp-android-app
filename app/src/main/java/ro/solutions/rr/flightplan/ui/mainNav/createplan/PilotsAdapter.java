package ro.solutions.rr.flightplan.ui.mainNav.createplan;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ro.solutions.rr.flightplan.R;
import ro.solutions.rr.flightplan.database.models.Pilot;

public class PilotsAdapter extends ArrayAdapter<Pilot> {

    private List<Pilot> mPilots;

    PilotsAdapter(Context context, List<Pilot> pilots) {
        super(context, android.R.layout.simple_list_item_1, pilots);
        mPilots = pilots;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String pilot = getItem(position).getFamilyName() + " " +  getItem(position).getFirstName();
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
        }
        TextView planeName = convertView.findViewById(android.R.id.text1);
        planeName.setText(pilot);
        planeName.setTextColor(getContext().getResources().getColor(R.color.cardview_dark_background));
        return convertView;
    }

    @NonNull
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        String pilot = getItem(position).getFamilyName() + " " +  getItem(position).getFirstName();
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
        }
        TextView planeName = convertView.findViewById(android.R.id.text1);
        planeName.setText(pilot);
        planeName.setTextColor(getContext().getResources().getColor(R.color.cardview_dark_background));
        return convertView;
    }

    @Override
    public Pilot getItem(int pos) {
        return mPilots.get(pos);
    }

}
