package ro.solutions.rr.flightplan.network.create;

import com.google.gson.annotations.SerializedName;

public class TripToRequest {

    @SerializedName("type")
    private int tripToType;
    @SerializedName("info")
    private String tripToInfo;
    @SerializedName("lat")
    private Double tripToLatitude;
    @SerializedName("long")
    private Double tripToLongitude;

    public int getTripToType() {
        return tripToType;
    }

    public void setTripToType(int tripToType) {
        this.tripToType = tripToType;
    }

    public String getTripToInfo() {
        return tripToInfo;
    }

    public void setTripToInfo(String tripToInfo) {
        this.tripToInfo = tripToInfo;
    }

    public Double getTripToLatitude() {
        return tripToLatitude;
    }

    public void setTripToLatitude(Double tripToLatitude) {
        this.tripToLatitude = tripToLatitude;
    }

    public Double getTripToLongitude() {
        return tripToLongitude;
    }

    public void setTripToLongitude(Double tripToLongitude) {
        this.tripToLongitude = tripToLongitude;
    }
}
