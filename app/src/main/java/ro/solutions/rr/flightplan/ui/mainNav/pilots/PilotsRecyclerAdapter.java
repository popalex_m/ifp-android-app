package ro.solutions.rr.flightplan.ui.mainNav.pilots;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.solutions.rr.flightplan.R;
import ro.solutions.rr.flightplan.database.models.Pilot;
import ro.solutions.rr.flightplan.ui.mainNav.ViewAnimations;

public class PilotsRecyclerAdapter extends RecyclerView.Adapter<PilotsRecyclerAdapter.ViewHolder>{

    private List<Pilot> mCurrentPilotList;
    private Boolean mEditMode;
    private EditPilotCallback mCallbackEditPilot;

    PilotsRecyclerAdapter(@NonNull List<Pilot> mCurrentPilotList, @NonNull EditPilotCallback callback) {
        this.mCurrentPilotList = mCurrentPilotList;
        this.mCallbackEditPilot = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_pilots, parent, false);
        return new PilotsRecyclerAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        int pos = holder.getAdapterPosition();
        Pilot pilot = mCurrentPilotList.get(pos);
        holder.mPilotFirstName.setText(String.format("%s %s", pilot.getFirstName(), pilot.getFamilyName()));
        if (mEditMode) {
            ViewAnimations.getViewAnimator().startRevealAnimation(holder.mEditPilotsCardview, holder.mEditButtons);
        } else {
            ViewAnimations.getViewAnimator().startHideRevealAnimation(holder.mEditPilotsCardview, holder.mEditButtons);
        }
    }

    @Override
    public int getItemCount() {
        return mCurrentPilotList.size() ;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.pilot_name)
        TextView mPilotFirstName;
        @BindView(R.id.edit_buttons_layout)
        LinearLayout mEditButtons;

        @BindView(R.id.btn_edit_pilot)
        ImageButton mEditPilotData;
        @BindView(R.id.btn_delete_pilot)
        ImageButton mDeletePilot;
        @BindView(R.id.cardview_edit_pilots)
        CardView mEditPilotsCardview;

        ViewHolder(@NonNull View view){
            super(view);
            ButterKnife.bind(this, view);
            mEditPilotData.setOnClickListener(this);
            mDeletePilot.setOnClickListener(this);
        }

        @Override
        public void onClick(@NonNull View view) {
            int position = getAdapterPosition();
            Pilot pilot = mCurrentPilotList.get(position);
            switch (view.getId()) {
                case R.id.btn_edit_pilot :
                    if (mCallbackEditPilot != null) {
                        mCallbackEditPilot.onPilotEditDataClick(pilot);
                    }
                    break;

                case R.id.btn_delete_pilot :
                    if (mCallbackEditPilot != null) {
                        mCallbackEditPilot.onPilotDeleteClick(pilot);
                    }
                    break;
            }
        }
    }

    public void setEditMode(Boolean value) {
        this.mEditMode = value;
    }

    public void addItem(Pilot pilot) {
        this.mCurrentPilotList.add(pilot);
    }
}
