package ro.solutions.rr.flightplan.database.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "table_email_server")
public class EmailServerConfig {

    @PrimaryKey(autoGenerate = true)
    private int mId;
    private String mHost;
    private Integer mPort;
    private boolean mSecure;
    private String mFrom;
    private String mEmailServerAuthUsername;
    private String mEmailServerAuthPassword;

    public EmailServerConfig() { }

    public EmailServerConfig(int mId, String mHost, Integer mPort, boolean mSecure, String mFrom, String mEmailServerAuthUsername, String mEmailServerAuthPassword) {
        this.mId = mId;
        this.mHost = mHost;
        this.mPort = mPort;
        this.mSecure = mSecure;
        this.mFrom = mFrom;
        this.mEmailServerAuthUsername = mEmailServerAuthUsername;
        this.mEmailServerAuthPassword = mEmailServerAuthPassword;
    }

    public void setSecure(boolean mSecure) {
        this.mSecure = mSecure;
    }

    public String getFrom() {
        return mFrom;
    }

    public void setFrom(String mFrom) {
        this.mFrom = mFrom;
    }

    public String getEmailServerAuthUsername() {
        return mEmailServerAuthUsername;
    }

    public void setEmailServerAuthUsername(String mEmailServerAuthUsername) { this.mEmailServerAuthUsername = mEmailServerAuthUsername; }

    public String getEmailServerAuthPassword() {
        return mEmailServerAuthPassword;
    }

    public void setEmailServerAuthPassword(String mEmailServerAuthPassword) { this.mEmailServerAuthPassword = mEmailServerAuthPassword; }

    public Integer getId() {
        return mId;
    }

    public void setId(Integer mId) {
        this.mId = mId;
    }

    public String getHost() {
        return mHost;
    }

    public void setHost(String mHost) {
        this.mHost = mHost;
    }

    public Integer getPort() {
        return mPort;
    }

    public void setPort(Integer mPort) {
        this.mPort = mPort;
    }

    public void setmSecure(boolean mSecure) { this.mSecure = mSecure; }

    public boolean isSecure() { return mSecure; }
}
