package ro.solutions.rr.flightplan.ui.mainNav.user.tabs.email;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import ro.solutions.rr.flightplan.app.MainApp;
import ro.solutions.rr.flightplan.database.dao.EmailServerConfigDao;
import ro.solutions.rr.flightplan.database.models.EmailServerConfig;
import ro.solutions.rr.flightplan.database.repository.EmailServerConfigRepository;
import ro.solutions.rr.flightplan.ui.TextValidationUtils;

public class EmailConfigViewModel extends AndroidViewModel {

    private MutableLiveData<Boolean> mShouldShowLoadingDialog;
    private MutableLiveData<String> mMessage;
    private EmailServerConfigRepository mEmailServerConfigRepository;

    public EmailConfigViewModel(@NonNull Application application) {
        super(application);
        EmailServerConfigDao mEmailServerDao = ((MainApp) application).getDatabaseManager().getEmailServerDao();
        mEmailServerConfigRepository = new EmailServerConfigRepository(mEmailServerDao);
    }

    public MutableLiveData<Boolean> getLoadingStatus() {
        if (mShouldShowLoadingDialog == null) {
            mShouldShowLoadingDialog =  new MutableLiveData<>();
            mShouldShowLoadingDialog.setValue(false);
        } return mShouldShowLoadingDialog;
    }

    public MutableLiveData<String> getOperationsMessage(){
        if (mMessage == null) {
            mMessage = new MutableLiveData<>();
            mMessage.setValue("");
        } return mMessage;
    }

    public LiveData<EmailServerConfig> getEmailServerConfig() {
        return mEmailServerConfigRepository.getEmailServerConfigLive();
    }


    public void saveEmailServerConfig(@NonNull String emailConfigUserName,
                                      @NonNull String emailConfigPassword,
                                      @NonNull String emailConfigConfirmPassword,
                                      @NonNull String emailConfigFromEmail,
                                      @NonNull String emailConfigHost,
                                      @NonNull Integer emailConfigPort,
                                      boolean  secureEmailStatus) {

        if (!emailConfigUserName.isEmpty() &&
                !emailConfigPassword.isEmpty() &&
                !emailConfigConfirmPassword.isEmpty() &&
                !emailConfigFromEmail.isEmpty()) {
            if (!TextValidationUtils.verifyEmailAddress(emailConfigFromEmail)){
                mMessage.setValue("You have not entered a valid email");
            } if (!TextValidationUtils.verifyPasswordMatch(emailConfigConfirmPassword, emailConfigPassword)) {
                mMessage.setValue("The entered passwords that do not match");
            } else {
                EmailServerConfig emailServerConfig = new EmailServerConfig();
                emailServerConfig.setPort(emailConfigPort);
                emailServerConfig.setHost(emailConfigHost);
                emailServerConfig.setEmailServerAuthPassword(emailConfigPassword);
                emailServerConfig.setEmailServerAuthUsername(emailConfigUserName);
                emailServerConfig.setmSecure(secureEmailStatus);
                emailServerConfig.setFrom(emailConfigFromEmail);

                saveEmailServerCredentials(emailServerConfig);
                mMessage.setValue("Settings saved successfully !");
            }
        } else{
            mMessage.setValue("Please complete all of the fields to save the current settings");
        }
    }

    private void saveEmailServerCredentials(EmailServerConfig emailServerConfig) {
        mEmailServerConfigRepository.updateEmailServerConfig(emailServerConfig);
    }
}

