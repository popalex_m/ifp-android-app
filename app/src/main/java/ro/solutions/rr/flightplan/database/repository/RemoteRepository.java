package ro.solutions.rr.flightplan.database.repository;

import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
import ro.solutions.rr.flightplan.network.account.UserDetailsResponse;
import ro.solutions.rr.flightplan.network.account.UserLoginRequest;
import ro.solutions.rr.flightplan.network.account.UserPasswordUpdateRequest;
import ro.solutions.rr.flightplan.network.account.UserProfileUpdateRequest;
import ro.solutions.rr.flightplan.network.create.CreatePlanReponse;
import ro.solutions.rr.flightplan.network.create.CreatePlanRequest;
import ro.solutions.rr.flightplan.network.create.DeletePlanRequest;
import ro.solutions.rr.flightplan.network.create.PlanHistoryResponse;
import ro.solutions.rr.flightplan.network.pilot.CreateEditPilotRequest;
import ro.solutions.rr.flightplan.network.pilot.CreatePilotResponse;
import ro.solutions.rr.flightplan.network.plane.CreateEditPlaneRequest;
import ro.solutions.rr.flightplan.network.plane.CreateEditPlaneResponse;
import ro.solutions.rr.flightplan.services.NetworkServices;

public class RemoteRepository {

    private NetworkServices mNetworkApi;

    public RemoteRepository(NetworkServices mNetworkApi) {
        this.mNetworkApi = mNetworkApi;
    }

    public Observable<Response<CreatePilotResponse>> uploadNewPilot (@NonNull CreateEditPilotRequest pilotRequest) {
        return mNetworkApi.addNewPilot(pilotRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Response<CreateEditPlaneResponse>> uploadNewPlane (@NonNull CreateEditPlaneRequest planeRequest) {
        return mNetworkApi.addNewPlane(planeRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Response<ResponseBody>> deletePilot (@NonNull CreateEditPilotRequest createEditPilotRequest) {
        return mNetworkApi.deletePilot(createEditPilotRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Response<ResponseBody>> deletePlane (@NonNull CreateEditPlaneRequest createEditPilotRequest) {
        return mNetworkApi.deletePlane(createEditPilotRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Response<ResponseBody>> updatePilot (@NonNull CreateEditPilotRequest createEditPilotRequest) {
        return mNetworkApi.updatePilot(createEditPilotRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Response<ResponseBody>> updatePlane (@NonNull CreateEditPlaneRequest createEditPilotRequest) {
        return mNetworkApi.updatePlane(createEditPilotRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Response<UserDetailsResponse>> login (@NonNull UserLoginRequest userLoginRequest) {
        return mNetworkApi.login(userLoginRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Response<ResponseBody>> updateAccountDetailsReq (@NonNull UserProfileUpdateRequest userProfileUpdateRequest) {
        return mNetworkApi.updateUserAccount(userProfileUpdateRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Response<ResponseBody>> updateAccountPasswordReq (@NonNull UserPasswordUpdateRequest userPasswordUpdateRequest) {
        return mNetworkApi.updateUserPassword(userPasswordUpdateRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Response<CreatePlanReponse>> updateCreateFlightPlan (@NonNull CreatePlanRequest createPlan){
        return mNetworkApi.createFlightPlan(createPlan)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Response<List<PlanHistoryResponse>>> getFlightPlanHistory(@NonNull String sessionTkn){
        Map<String, String> params = new HashMap<>();
        params.put("session", sessionTkn);
        params.put("only_for_session", "true");
        params.put("only_cancelable", "false");
        return mNetworkApi.getPreviouslyCreatedPlans(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Response<ResponseBody>> cancelFlightPlan(@NonNull DeletePlanRequest deletePlanRequest){
        return mNetworkApi.cancelFlightPlan(deletePlanRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
