package ro.solutions.rr.flightplan.ui.mainNav.dialogs;

import ro.solutions.rr.flightplan.database.models.Pilot;


public interface AddEditPilotDialogListener {
    void onPilotAdded (Pilot pilot);

    void onPilotEdited (Pilot pilot);
}
