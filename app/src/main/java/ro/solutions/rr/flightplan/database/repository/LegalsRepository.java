package ro.solutions.rr.flightplan.database.repository;

import android.support.annotation.NonNull;

import ro.solutions.rr.flightplan.database.dao.LegalsDao;
import ro.solutions.rr.flightplan.database.models.Legals;

public class LegalsRepository {

    private LegalsDao mLegalsDao;

    public LegalsRepository(LegalsDao legalsDao) {
        mLegalsDao = legalsDao;
    }

    public void insertLegals(@NonNull Legals legals) {
        mLegalsDao.insertNewLegals(legals);
    }
}
