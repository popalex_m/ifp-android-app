package ro.solutions.rr.flightplan.ui.mainNav.history;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.solutions.rr.flightplan.R;
import ro.solutions.rr.flightplan.network.create.PlanHistoryResponse;

public class FlightPlanHistoryFragment extends Fragment
                                            implements FlightPlanActionListener {

    private String TAG = FlightPlanHistoryFragment.class.getSimpleName();

    private FlightPlanHistoryViewModel mFlightPlanHistoryViewModel;

    @BindView(R.id.progressBarHistoryFragment)
    ProgressBar mLoadingProgressBar;
    @BindView(R.id.recycler_history_list)
    RecyclerView mRecyclerViewPlanHistory;

    private FlightPlanHistoryRecyclerAdapter mFlightPlanHistoryAdapter;
    private AppCompatActivity mActivity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_flight_history_layout, container, false);
        ButterKnife.bind(this, rootView);

        mFlightPlanHistoryViewModel = ViewModelProviders.of(this).get(FlightPlanHistoryViewModel.class);
        initViewModelObservers();
        initFlightPlanHistoryData();
        return rootView;
    }

    private void initFlightPlanHistoryData() {
        mFlightPlanHistoryViewModel.initHistoryFromRemote();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity)context;
        mActivity.setTitle("Flight History");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void initViewModelObservers() {
        mFlightPlanHistoryViewModel.getShowLoadingSpinner().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean shouldShowLoadingSpinner) {
                if (shouldShowLoadingSpinner != null) {
                    showLoadingSpinner(shouldShowLoadingSpinner);
                }
            }
        });
        mFlightPlanHistoryViewModel.getPlansHistoryRequestMessage().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String networkRequestMessage) {
                if (networkRequestMessage != null && !networkRequestMessage.isEmpty()) {
                        showToastMessage(networkRequestMessage);
                }
            }
        });
        mFlightPlanHistoryViewModel.getPlanHistoryDetails().observe(this, new Observer<List<PlanHistoryResponse>>() {
            @Override
            public void onChanged(@Nullable List<PlanHistoryResponse> planHistoryResponses) {
                if (planHistoryResponses != null) {
                    initRecyclerView(planHistoryResponses);
                }
            }
        });
    }

    private void initRecyclerView (@NonNull List<PlanHistoryResponse> planHistoryDetailsList) {
        mFlightPlanHistoryAdapter = new FlightPlanHistoryRecyclerAdapter(planHistoryDetailsList , this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerViewPlanHistory.setLayoutManager(mLayoutManager);
        mRecyclerViewPlanHistory.setAdapter(mFlightPlanHistoryAdapter);
    }

    private void showLoadingSpinner(boolean status) {
        if (status) {
            mRecyclerViewPlanHistory.setVisibility(View.INVISIBLE);
            mLoadingProgressBar.setVisibility(View.VISIBLE);
        } else {
            mRecyclerViewPlanHistory.setVisibility(View.VISIBLE);
            mLoadingProgressBar.setVisibility(View.GONE);
        }
    }

    private void showToastMessage (String msg) {
        Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFlightPlanCancelled(int planId) {
        Log.d(TAG , "Receiving cancel request for plan id -> " + planId);
        mFlightPlanHistoryViewModel.deletePlanRequest(planId);
    }

    @Override
    public void onFlightPlanRescheduled(int planId) {
        Log.d(TAG , "Receiving rechedule request for plan id -> " + planId);

    }
}
