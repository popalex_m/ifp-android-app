package ro.solutions.rr.flightplan.ui.mainNav.planes;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.solutions.rr.flightplan.R;
import ro.solutions.rr.flightplan.database.models.Plane;
import ro.solutions.rr.flightplan.ui.mainNav.OperationTypes;
import ro.solutions.rr.flightplan.ui.mainNav.dialogs.AddEditPlaneCallback;
import ro.solutions.rr.flightplan.ui.mainNav.dialogs.AddEditPlaneDialogFragment;

import static ro.solutions.rr.flightplan.ui.mainNav.OperationTypes.EDIT_EXISTING_PLANE;

public class PlanesManagementFragment extends Fragment implements EditPlaneCallback,
                                                                  AddEditPlaneCallback,
                                                                  View.OnClickListener{

    private String TAG = PlanesManagementFragment.class.getSimpleName();

    private int PLANES_FRAGMENT_REQ_CODE = 400;
    private String EDIT_MODE_STATE = "EDIT_MODE_STATE";

    @BindView(R.id.recycler_planes_list)
    RecyclerView mPlanesListRecycler;
    @BindView(R.id.fabAddPlane)
    FloatingActionButton mFabAddPlane;
    @BindView(R.id.progressBarPlanesFrag)
    ProgressBar mProgressBarLoading;

    private FragmentManager mFragmentManager;
    private PlanesRecyclerAdapter mPlanesAdapter;
    private PlanesManagementViewModel mPlanesManagementViewModel;
    private AppCompatActivity mActivity;
    private Boolean mEditMode = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_planes_management, container, false);
        ButterKnife.bind(this, rootView);
        if (savedInstanceState != null) { mEditMode = savedInstanceState.getBoolean(EDIT_MODE_STATE); }

        mFragmentManager = getFragmentManager();
        mPlanesManagementViewModel = ViewModelProviders.of(this).get(PlanesManagementViewModel.class);
        initViewModelObservers();
        initListeners();
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity)context;
        mActivity.setTitle("Planes Management");
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putBoolean(EDIT_MODE_STATE, mEditMode);
        super.onSaveInstanceState(outState);
    }

    private void initViewModelObservers() {
       mPlanesManagementViewModel.getAllPlanesInDb().observe(this, new Observer<List<Plane>>() {
           @Override
           public void onChanged(@Nullable List<Plane> planes) {
               if (planes != null) {
                   initRecyclerView(planes);
               }
           }
       });
       mPlanesManagementViewModel.getShouldShowLoadingDialog().observe(this, new Observer<Boolean>() {
           @Override
           public void onChanged(@Nullable Boolean shouldShowLoadingDlg) {
               if (shouldShowLoadingDlg != null) {
                   if (shouldShowLoadingDlg){
                      showLoading(true);
                   } else {
                      showLoading(false);
                   }
               }
           }
       });
       mPlanesManagementViewModel.getMessage().observe(this, new Observer<String>() {
           @Override
           public void onChanged(@Nullable String message) {
               if (message != null) {
                   if (!message.isEmpty()){
                       showMessageToast(message);
                   }
               }
           }
       });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (mEditMode) {
            menu.getItem(0).setIcon(ContextCompat.getDrawable(mActivity, R.drawable.ic_edit_accent_24dp));
        } else {
            menu.getItem(0).setIcon(ContextCompat.getDrawable(mActivity, R.drawable.ic_edit_white_24dp));
        }
        menu.getItem(1).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        int id = item.getItemId();
        switch (id) {
            case R.id.action_edit:
                if (mEditMode != null) {
                    if (mEditMode) {
                        mEditMode = false;
                        mPlanesAdapter.setEditMode(false);
                        item.setIcon(ContextCompat.getDrawable(mActivity, R.drawable.ic_edit_white_24dp));
                    } else {
                        mEditMode = true;
                        mPlanesAdapter.setEditMode(true);
                        item.setIcon(ContextCompat.getDrawable(mActivity, R.drawable.ic_edit_accent_24dp));
                    }
                    refreshRecyclerView();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void refreshRecyclerView() {
        mPlanesAdapter.notifyDataSetChanged();
        mPlanesListRecycler.invalidate();
    }

    private void initListeners() {
        mFabAddPlane.setOnClickListener(this);
    }

    private void initRecyclerView (@NonNull List<Plane> planes) {
        mPlanesAdapter = new PlanesRecyclerAdapter(planes , this);
        mPlanesAdapter.setEditMode(mEditMode);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mPlanesListRecycler.setLayoutManager(mLayoutManager);
        mPlanesListRecycler.setAdapter(mPlanesAdapter);
        attachRecyclerScrollListener();
    }

    private void showLoading(boolean status){
        if (status){
            mProgressBarLoading.setVisibility(View.VISIBLE);
            mPlanesListRecycler.setVisibility(View.INVISIBLE);
            mFabAddPlane.setVisibility(View.INVISIBLE);
        } else {
            mProgressBarLoading.setVisibility(View.INVISIBLE);
            mPlanesListRecycler.setVisibility(View.VISIBLE);
            mFabAddPlane.setVisibility(View.VISIBLE);
        }
    }

    private void attachRecyclerScrollListener(){
        mPlanesListRecycler.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                if (dy > 0)
                    mFabAddPlane.hide();
                else if (dy < 0)
                    mFabAddPlane.show();
            }
        });
    }

    private void showMessageToast(@NonNull String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
         switch (view.getId()){
             case R.id.fabAddPlane :
                 openAddEditPlaneDialog(OperationTypes.CREATE_NEW_PLANE , null);
                 break;
         }
    }

    private void openAddEditPlaneDialog(OperationTypes operationType, @Nullable Plane plane) {
        AddEditPlaneDialogFragment addEditPlaneDialogFragment = new AddEditPlaneDialogFragment();
        if (operationType == EDIT_EXISTING_PLANE) {
            if (plane != null) {
                Log.d(TAG, "Setting editMode for plane -> " + plane.getName());
                addEditPlaneDialogFragment.setPlane(plane);
                addEditPlaneDialogFragment.setOperationType(operationType);
            }
        } else {
            addEditPlaneDialogFragment.setOperationType(operationType);
        }
        addEditPlaneDialogFragment.setTargetFragment(PlanesManagementFragment.this, PLANES_FRAGMENT_REQ_CODE);
        addEditPlaneDialogFragment.show(mFragmentManager , "");
    }

    @Override
    public void onPlaneAdded(Plane plane) {
        Log.e(TAG , "sending an add new plane  request to the backend");
        mPlanesManagementViewModel.addNewPlane(plane);
    }

    @Override
    public void onPlaneEdited(Plane plane) {
        mPlanesManagementViewModel.updatePlane(plane);
    }

    @Override
    public void onPlaneEditCallback(Plane plane) {
        openAddEditPlaneDialog(OperationTypes.EDIT_EXISTING_PLANE , plane);
    }

    @Override
    public void onPlaneDeleteCallback(Plane plane) {
         mPlanesManagementViewModel.deletePlane(plane);
    }
}
