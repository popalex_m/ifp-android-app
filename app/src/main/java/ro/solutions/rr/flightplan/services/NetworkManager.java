package ro.solutions.rr.flightplan.services;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import ro.solutions.rr.flightplan.BuildConfig;

public class NetworkManager {

    private static Retrofit mRetrofit = null;

    public synchronized static Retrofit getClient(String baseUrl) {
        if (mRetrofit == null) {
            createInstance(baseUrl);
        }
        return mRetrofit;
    }

    private static void createInstance(String mBaseUrl){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(BuildConfig.NETWORK_CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(BuildConfig.NETWORK_WRITE_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(BuildConfig.NETWORK_CONNECT_TIMEOUT, TimeUnit.SECONDS);
        OkHttpClient okHttpClient = okHttpBuilder.build();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(mBaseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

}
