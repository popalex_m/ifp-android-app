package ro.solutions.rr.flightplan.network.create;

import com.google.gson.annotations.SerializedName;

public class TripFromRequest {

    @SerializedName("type")
    private int tripFromType;

    @SerializedName("info")
    private String tripFromInfo;

    public int getTripFromType() {
        return tripFromType;
    }

    public void setTripFromType(int tripFromType) {
        this.tripFromType = tripFromType;
    }

    public String getTripFromInfo() {
        return tripFromInfo;
    }

    public void setTripFromInfo(String tripFromInfo) {
        this.tripFromInfo = tripFromInfo;
    }
}
