package ro.solutions.rr.flightplan.ui;

import android.support.annotation.NonNull;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextValidationUtils {

    private static boolean isValidEmail(@NonNull String email) {
        String PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private static boolean areStringsIdentical(@NonNull String firstText, @NonNull String secondText) {
         if (!firstText.isEmpty() && !secondText.isEmpty()){
            return Objects.equals(firstText , secondText);
         } else {
             return false;
         }
    }

    public static boolean verifyEmailAddress(@NonNull String email) {
        return TextValidationUtils.isValidEmail(email);
    }

    public static boolean verifyPasswordMatch(@NonNull String pass1, @NonNull String pass2) {
        return TextValidationUtils.areStringsIdentical(pass1, pass2);
    }


}
