package ro.solutions.rr.flightplan.ui;

public class TakeOffLandingTypes {

    public static Integer AIRFIELD = 0;
    public static Integer HELIPAD = 1;
    public static Integer HIDROPAD = 2;
    public static Integer NO_TAKEOFF_TYPE_SELECTED = 3;
}
