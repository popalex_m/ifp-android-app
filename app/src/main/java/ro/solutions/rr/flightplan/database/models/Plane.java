package ro.solutions.rr.flightplan.database.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "table_planes")
public class Plane {

    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    @Ignore
    private String autonomy;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAutonomy() { return autonomy; }

    public void setAutonomy(String autonomy) { this.autonomy = autonomy; }
}
