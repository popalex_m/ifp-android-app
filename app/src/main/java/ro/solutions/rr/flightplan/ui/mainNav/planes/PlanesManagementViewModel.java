package ro.solutions.rr.flightplan.ui.mainNav.planes;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import ro.solutions.rr.flightplan.app.MainApp;
import ro.solutions.rr.flightplan.database.dao.PlanesDao;
import ro.solutions.rr.flightplan.database.dao.UserAccountDao;
import ro.solutions.rr.flightplan.database.models.Plane;
import ro.solutions.rr.flightplan.database.repository.PlanesRepository;
import ro.solutions.rr.flightplan.database.repository.RemoteRepository;
import ro.solutions.rr.flightplan.database.repository.UserAccountRepository;
import ro.solutions.rr.flightplan.network.plane.CreateEditPlaneRequest;
import ro.solutions.rr.flightplan.network.plane.CreateEditPlaneResponse;
import ro.solutions.rr.flightplan.services.NetworkServices;
import ro.solutions.rr.flightplan.utils.NetworkUtils;

public class PlanesManagementViewModel extends AndroidViewModel {

    private String TAG = PlanesManagementViewModel.class.getSimpleName();

    private PlanesRepository mPlanesRepository;
    private RemoteRepository mNetworkRepository;
    private UserAccountRepository mUserAccountRepository;

    private MutableLiveData<Boolean> mShouldShowLoadingDialog;
    private MutableLiveData<String> mMessage;

    public PlanesManagementViewModel(@NonNull Application application) {
        super(application);

        UserAccountDao mUserAccountDao = ((MainApp) application).getDatabaseManager().getUserDao();
        PlanesDao mPlanesDao = ((MainApp) application).getDatabaseManager().getPlanesDao();
        NetworkServices mNetworkServices = ((MainApp)application).getNetworkServices();

        mUserAccountRepository = new UserAccountRepository(mUserAccountDao);
        mPlanesRepository = new PlanesRepository(mPlanesDao);
        mNetworkRepository = new RemoteRepository(mNetworkServices);
    }

    public LiveData<List<Plane>> getAllPlanesInDb() {
       return mPlanesRepository.getAllPlanes();
    }

    public MutableLiveData<Boolean> getShouldShowLoadingDialog() {
        if (mShouldShowLoadingDialog == null) {
            mShouldShowLoadingDialog = new MutableLiveData<>();
            mShouldShowLoadingDialog.setValue(false);
        } return mShouldShowLoadingDialog;
    }

    public MutableLiveData<String> getMessage() {
        if (mMessage == null) {
            mMessage= new  MutableLiveData<>();
            mMessage.setValue("");
        } return mMessage;
    }


    public void updatePlane(@NonNull Plane plane){
        if (NetworkUtils.isConnected(getApplication())){
            updatePlaneRemote(plane);
        } else {
            mMessage.setValue("No internet connection available");
        }
    }

    public void deletePlane(@NonNull Plane plane) {
        if (NetworkUtils.isConnected(getApplication())){
            deletePlaneRemote(plane);
        } else {
            mMessage.setValue("No internet connection available");
        }
    }

    public void addNewPlane(@NonNull Plane plane) {
        if (NetworkUtils.isConnected(getApplication())){
            addNewPlaneRemote(plane);
        } else {
            mMessage.setValue("No internet connection available");
        }
    }

    private void addNewPlaneRemote(@NonNull Plane planeReq) {
        CreateEditPlaneRequest planeRequest = getPlaneReq(planeReq);
        mShouldShowLoadingDialog.setValue(true);
        mNetworkRepository.uploadNewPlane(planeRequest)
                .subscribe(new Observer<Response<CreateEditPlaneResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) { }

                    @Override
                    public void onNext(Response<CreateEditPlaneResponse> response) {
                        int statusCode = response.code();
                        if (statusCode == 200) {
                            Log.d(TAG , "Create req returned successful status code -> " + statusCode);
                            if (response.body() != null) {
                                Plane plane = new Plane();
                                plane.setName(planeReq.getName());
                                plane.setId(response.body().getmNewPlaneId());
                                insertPlaneLocally(plane);
                            } else {
                                Log.e(TAG , "Response body is empty");
                            }
                        } else {
                            Log.e(TAG , "Create req returned error status code -> " + statusCode);
                        }
                        mShouldShowLoadingDialog.setValue(false);
                    }

                    @Override
                    public void onError(Throwable e) { e.printStackTrace(); }

                    @Override
                    public void onComplete() { }
                });
    }

    private void deletePlaneRemote(@NonNull Plane planeReq) {
        CreateEditPlaneRequest planeRequest = getPlaneReq(planeReq);
        mShouldShowLoadingDialog.setValue(true);
        mNetworkRepository.deletePlane(planeRequest)
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) { }

                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        int statusCode = response.code();
                        if (statusCode == 200) {
                            Log.d(TAG , "Delete request returned successful status code -> " + statusCode);
                            deletePlaneLocally(planeReq);
                        } else {
                            mMessage.setValue("Error processing request");
                            Log.e(TAG , "Delete request returned error status code -> " + statusCode);
                        }
                        mShouldShowLoadingDialog.setValue(false);
                    }

                    @Override
                    public void onError(Throwable e) { e.printStackTrace(); }

                    @Override
                    public void onComplete() { }
                });
    }


    private void updatePlaneRemote(@NonNull Plane planeReq) {
        CreateEditPlaneRequest planeRequest = getPlaneReq(planeReq);
        mShouldShowLoadingDialog.setValue(true);
        mNetworkRepository.updatePlane(planeRequest)
                .subscribe(new Observer<Response<ResponseBody>>() {
            @Override
            public void onSubscribe(Disposable d) { }

            @Override
            public void onNext(Response<ResponseBody> response) {
                int statusCode = response.code();
                if (statusCode == 200){
                    Log.d(TAG , "Delete request returned successful status code -> " + statusCode);
                    updatePlaneLocally(planeReq);
                } else {
                    mMessage.setValue("Error processing request");
                    Log.e(TAG , "Delete request returned error status code -> " + statusCode);
                }
                mShouldShowLoadingDialog.setValue(false);
            }

            @Override
            public void onError(Throwable e) { e.printStackTrace(); }

            @Override
            public void onComplete() { }
        });
    }

    private void insertPlaneLocally(@NonNull Plane plane){
        mPlanesRepository.insertPlaneSync(plane);
        mMessage.setValue(" Successfully added " + plane.getName());
    }

    private void deletePlaneLocally(@NonNull Plane plane) {
        mPlanesRepository.deletePlaneSync(plane);
        mMessage.setValue(" Successfully deleted " + plane.getName());
    }

    private void updatePlaneLocally(@NonNull Plane plane) {
        mPlanesRepository.updateSinglePlane(plane);
        mMessage.setValue(" Successfully edited " + plane.getName());
    }

    private CreateEditPlaneRequest getPlaneReq(Plane plane) {
        String token = mUserAccountRepository.getLoggedInUser().getmSessionToken();
        CreateEditPlaneRequest planeRequest = new CreateEditPlaneRequest();
        planeRequest.setmPlane(plane);
        planeRequest.setmSession(token);
        return planeRequest;
    }
}
