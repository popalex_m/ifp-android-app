package ro.solutions.rr.flightplan.ui.mainNav.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.solutions.rr.flightplan.R;
import ro.solutions.rr.flightplan.database.models.Plane;
import ro.solutions.rr.flightplan.ui.mainNav.OperationTypes;

public class AddEditPlaneDialogFragment extends DialogFragment implements DialogInterface.OnClickListener{


    private String TAG = AddEditPilotDialogFragment.class.getSimpleName();

    @BindView(R.id.editText_plane_name)
    EditText mEditTextPlaneName;

    private Plane mToEditPlane;
    private OperationTypes mOperationType;
    private AddEditPlaneCallback mAddPlaneDialogCallback;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View dialogView = inflater.inflate(R.layout.dialog_fragment_create_plane, null);
        ButterKnife.bind(this, dialogView);
        builder.setView(dialogView);
        builder.setPositiveButton("OK",this);
        builder.setNegativeButton("Cancel",this);
        addTextChangeListeners();
        initData();
        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case Dialog.BUTTON_POSITIVE :
                mAddPlaneDialogCallback = (AddEditPlaneCallback) getTargetFragment();
                switch (mOperationType) {
                    case CREATE_NEW_PLANE:
                        if (mAddPlaneDialogCallback != null) {
                            Log.d(TAG , "sending add plane() request callback from dialog fragment");
                            mAddPlaneDialogCallback.onPlaneAdded(mToEditPlane);
                        }
                        break;
                    case EDIT_EXISTING_PLANE:
                        if (mAddPlaneDialogCallback != null) {
                            Log.d(TAG , "sending edit plane() request callback from dialog fragment");
                            mAddPlaneDialogCallback.onPlaneEdited(mToEditPlane);
                        }
                        break;
                }
                break;

            case Dialog.BUTTON_NEGATIVE :
                dismiss();
                break;
        }
    }

    private void addTextChangeListeners () {
        mEditTextPlaneName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            public void afterTextChanged(Editable s) {
                mToEditPlane.setName(s.toString());
            }
        });
    }

    private void initData() {
        if (mToEditPlane == null) {
            mToEditPlane = new Plane();
        } else {
            mEditTextPlaneName.setText(mToEditPlane.getName());
        }
    }

    public void setPlane(@NonNull Plane plane) {
        mToEditPlane = plane;
    }

    public void setOperationType(OperationTypes operationType) {
        mOperationType = operationType;
    }
}
