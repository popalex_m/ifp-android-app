package ro.solutions.rr.flightplan.ui.login;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
import ro.solutions.rr.flightplan.app.MainApp;
import ro.solutions.rr.flightplan.database.dao.UserAccountDao;
import ro.solutions.rr.flightplan.network.account.UserSignUpRequest;
import ro.solutions.rr.flightplan.services.NetworkServices;
import ro.solutions.rr.flightplan.ui.TextValidationUtils;
import ro.solutions.rr.flightplan.utils.NetworkUtils;

public class SignUpFragmentViewModel extends AndroidViewModel {

    private String TAG = SignUpFragmentViewModel.class.getSimpleName();

    private  NetworkServices mNetworkServices;
    private  UserAccountDao mUserAccountDao;
    private  MutableLiveData<Boolean> mSignUpReqStatus;
    private  MutableLiveData<String> mSignUpReqMessage;

    public SignUpFragmentViewModel(@NonNull Application application) {
        super(application);
        mUserAccountDao = ((MainApp) application).getDatabaseManager().getUserDao();
        mNetworkServices = ((MainApp) application).getNetworkServices();
    }

    public MutableLiveData<Boolean> getSignUpRequestStatus () {
       if (mSignUpReqStatus == null) {
           mSignUpReqStatus = new MutableLiveData<>();
           mSignUpReqStatus.setValue(false);
       } return mSignUpReqStatus;
    }

    public MutableLiveData<String> getSignUpRequestMessage () {
        if (mSignUpReqMessage == null) {
            mSignUpReqMessage = new MutableLiveData<>();
            mSignUpReqMessage.setValue(null);
        } return mSignUpReqMessage;
    }

    private void uploadAccountDetails(@NonNull UserSignUpRequest signUpRequest) {
        if (NetworkUtils.isConnected(getApplication())) {
            submitAccountDetailsToRemote(signUpRequest);
        } else {
            mSignUpReqMessage.setValue("There is no internet connection available");
        }
    }

    public void validateForms(@NonNull String fName,
                              @NonNull String lastName,
                              @NonNull String email,
                              @NonNull String pass,
                              @NonNull String confirmPass,
                              boolean confirmStatus) {
        boolean userDetailsCompletionStatus = verifyUserDetails(fName, lastName , email, pass, confirmPass);
        boolean verifyEmailAddress = TextValidationUtils.verifyEmailAddress(email);
        boolean verifyPassMatch = TextValidationUtils.verifyPasswordMatch(pass, confirmPass);
        if (userDetailsCompletionStatus) {
            if (verifyEmailAddress) {
                if (verifyPassMatch) {
                    if (confirmStatus) {
                        saveUserDetailsToDao(fName, lastName, email, pass);
                    } else {
                        mSignUpReqMessage.setValue("Please toggle the confirm switch to proceed!");
                    }
                } else {
                    mSignUpReqMessage.setValue("Passwords do not match !");
                }
            } else {
                mSignUpReqMessage.setValue("Please user a valid email address!");
            }
        } else {
            mSignUpReqMessage.setValue("Please complete all the details to be able to sign up for an user account!");
        }
    }

    private void submitAccountDetailsToRemote(@NonNull UserSignUpRequest signUpRequest) {
        mNetworkServices.createUserAccount(signUpRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
            @Override
            public void onSubscribe(Disposable d) { }

            @Override
            public void onNext(Response<ResponseBody> response) {
                        int statusCode =  response.code();
                        if (statusCode == 200) {
                            mSignUpReqStatus.setValue(true);
                            mSignUpReqMessage.setValue("Sign up has completed successfully");
                            Log.d(TAG , "Received 200 status code, everything is ok");
                        } else {
                            mSignUpReqStatus.setValue(false);
                            try {
                                JSONObject errorMsg = null;
                                if (response.errorBody() != null) {
                                    errorMsg = new JSONObject(response.errorBody().string());
                                }
                                mSignUpReqMessage.setValue("Sign up request failed : " + errorMsg);
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
            }

            @Override
            public void onError(Throwable e) { e.printStackTrace();}

            @Override
            public void onComplete() { }
        });
    }
    private boolean verifyUserDetails (@NonNull String firstName,
                                       @NonNull String lastName,
                                       @NonNull String email,
                                       @NonNull String pass,
                                       @NonNull String confirmPass) {
        return !firstName.isEmpty()
                && !lastName.isEmpty()
                && !pass.isEmpty()
                && !email.isEmpty()
                && !confirmPass.isEmpty();
    }

    private void saveUserDetailsToDao(@NonNull String name, @NonNull String lastName, @NonNull String email, @NonNull String pass) {
        UserSignUpRequest userSignUpRequest = new UserSignUpRequest();
        userSignUpRequest.setmFirstName(name);
        userSignUpRequest.setmLastName(lastName);
        userSignUpRequest.setmEmail(email);
        userSignUpRequest.setmPassword(pass);
        uploadAccountDetails(userSignUpRequest);
    }
}
