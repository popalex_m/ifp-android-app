package ro.solutions.rr.flightplan.network.create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlaneRequest {

    @SerializedName("name")
    @Expose
    private String planeName;

    @SerializedName("autonomy")
    @Expose
    private String autonomy;

    public String getPlaneName() {
        return planeName;
    }

    public void setPlaneName(String planeName) {
        this.planeName = planeName;
    }

    public String getAutonomy() {
        return autonomy;
    }

    public void setAutonomy(String autonomy) {
        this.autonomy = autonomy;
    }
}
