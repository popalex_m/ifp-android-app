package ro.solutions.rr.flightplan.ui.login;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import ro.solutions.rr.flightplan.app.MainApp;
import ro.solutions.rr.flightplan.database.models.UserAccount;

public class LoginActivityViewModel extends AndroidViewModel {

    private LiveData<UserAccount> mUserAccountObservable;

    public LoginActivityViewModel(@NonNull Application application) {
        super(application);
        mUserAccountObservable = ((MainApp) application).getDatabaseManager().getUserDao().getFirstUser();
    }

    public LiveData<UserAccount> getFirstUserAccount(){
       return mUserAccountObservable;
    }
}
