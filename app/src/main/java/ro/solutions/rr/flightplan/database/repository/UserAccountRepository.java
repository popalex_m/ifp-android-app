package ro.solutions.rr.flightplan.database.repository;

import android.arch.lifecycle.LiveData;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;
import ro.solutions.rr.flightplan.database.dao.UserAccountDao;
import ro.solutions.rr.flightplan.database.models.UserAccount;

public class UserAccountRepository {

    private String TAG = UserAccountRepository.class.getSimpleName();

    private UserAccountDao mUserDao;

    public UserAccountRepository(UserAccountDao userDao) { mUserDao = userDao; }

    public Completable insertUserAccountAsync (UserAccount userAccount) {
       return Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                mUserDao.insertSingleUser(userAccount);
            }
        }).observeOn(AndroidSchedulers.mainThread())
               .subscribeOn(Schedulers.io());

    }

    public void insertUserAccountSync (UserAccount userAccount) {
        mUserDao.insertSingleUser(userAccount);
    }

    public UserAccount getLoggedInUser() {
        return mUserDao.getUserAccount();
    }

    public LiveData<UserAccount> getFistUserAccount() {
        return mUserDao.getFirstUser();
    }

    public int deleteUserData () {
        return mUserDao.deleteUserAccount();
    }

    public int updateUserAccount (UserAccount userAccount) { return mUserDao.updateUser(userAccount); }
}

