package ro.solutions.rr.flightplan.services;

import android.app.Application;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;

import ro.solutions.rr.flightplan.database.dao.EmailServerConfigDao;
import ro.solutions.rr.flightplan.database.dao.FlightPlanHistoryDao;
import ro.solutions.rr.flightplan.database.dao.PilotsDao;
import ro.solutions.rr.flightplan.database.dao.PlanesDao;
import ro.solutions.rr.flightplan.database.dao.UserAccountDao;
import ro.solutions.rr.flightplan.database.models.Aps;
import ro.solutions.rr.flightplan.database.models.EmailServerConfig;
import ro.solutions.rr.flightplan.database.models.Legals;
import ro.solutions.rr.flightplan.database.models.Pilot;
import ro.solutions.rr.flightplan.database.models.Plan;
import ro.solutions.rr.flightplan.database.models.Plane;
import ro.solutions.rr.flightplan.database.models.Trip;
import ro.solutions.rr.flightplan.database.models.TripFrom;
import ro.solutions.rr.flightplan.database.models.TripTo;
import ro.solutions.rr.flightplan.database.models.UserAccount;

@Database(entities = {UserAccount.class, Plane.class, Pilot.class, EmailServerConfig.class, Aps.class , Legals.class, Trip.class , TripFrom.class, TripTo.class, Plan.class}, version = 1)
public abstract class DatabaseManager extends RoomDatabase {

    private static final String DB_NAME = "database.db";

    private static DatabaseManager sInstance;

    public static synchronized DatabaseManager getDatabaseInstance(Application application) {
        if (sInstance == null) {
            sInstance = createDatabase(application);
        }
        return sInstance;
    }

    private static DatabaseManager createDatabase(Application mApplication) {
        return Room.databaseBuilder(mApplication, DatabaseManager.class, DB_NAME).allowMainThreadQueries().build();
    }

    public abstract UserAccountDao getUserDao();

    public abstract PlanesDao getPlanesDao();

    public abstract PilotsDao getPilotsDao();

    public abstract EmailServerConfigDao getEmailServerDao();

    public abstract FlightPlanHistoryDao getFlightPlanHistoryDao();

}
