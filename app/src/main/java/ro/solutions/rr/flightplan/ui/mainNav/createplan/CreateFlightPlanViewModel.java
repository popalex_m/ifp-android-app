package ro.solutions.rr.flightplan.ui.mainNav.createplan;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import retrofit2.Response;
import ro.solutions.rr.flightplan.app.MainApp;
import ro.solutions.rr.flightplan.database.dao.EmailServerConfigDao;
import ro.solutions.rr.flightplan.database.dao.FlightPlanHistoryDao;
import ro.solutions.rr.flightplan.database.dao.PilotsDao;
import ro.solutions.rr.flightplan.database.dao.PlanesDao;
import ro.solutions.rr.flightplan.database.models.EmailServerConfig;
import ro.solutions.rr.flightplan.database.models.Legals;
import ro.solutions.rr.flightplan.database.models.Pilot;
import ro.solutions.rr.flightplan.database.models.Plan;
import ro.solutions.rr.flightplan.database.models.Plane;
import ro.solutions.rr.flightplan.database.models.Trip;
import ro.solutions.rr.flightplan.database.models.TripFrom;
import ro.solutions.rr.flightplan.database.models.TripTo;
import ro.solutions.rr.flightplan.database.repository.EmailServerConfigRepository;
import ro.solutions.rr.flightplan.database.repository.FlightPlanHistoryRepository;
import ro.solutions.rr.flightplan.database.repository.PilotRepository;
import ro.solutions.rr.flightplan.database.repository.PlanesRepository;
import ro.solutions.rr.flightplan.database.repository.RemoteRepository;
import ro.solutions.rr.flightplan.network.create.ApsRequest;
import ro.solutions.rr.flightplan.network.create.AuthRequest;
import ro.solutions.rr.flightplan.network.create.CreatePlanReponse;
import ro.solutions.rr.flightplan.network.create.CreatePlanRequest;
import ro.solutions.rr.flightplan.network.create.EmailServerConfigRequest;
import ro.solutions.rr.flightplan.network.create.LegalsRequest;
import ro.solutions.rr.flightplan.network.create.PilotRequest;
import ro.solutions.rr.flightplan.network.create.PlanRequest;
import ro.solutions.rr.flightplan.network.create.PlaneRequest;
import ro.solutions.rr.flightplan.network.create.TripFromRequest;
import ro.solutions.rr.flightplan.network.create.TripRequest;
import ro.solutions.rr.flightplan.network.create.TripToRequest;
import ro.solutions.rr.flightplan.services.NetworkServices;

import static ro.solutions.rr.flightplan.ui.TakeOffLandingTypes.AIRFIELD;
import static ro.solutions.rr.flightplan.ui.TakeOffLandingTypes.HELIPAD;
import static ro.solutions.rr.flightplan.ui.TakeOffLandingTypes.HIDROPAD;
import static ro.solutions.rr.flightplan.ui.TakeOffLandingTypes.NO_TAKEOFF_TYPE_SELECTED;

public class CreateFlightPlanViewModel extends AndroidViewModel{

    private String TAG = CreateFlightPlanViewModel.class.getSimpleName();

    private PlanesRepository mPlanesRepository;
    private PilotRepository mPilotRepository;
    private RemoteRepository mRemoteRepository;
    private EmailServerConfigRepository mEmailServerConfig;
    private FlightPlanHistoryRepository mFlightPlanHistoryRepository;

    private String mCivilAviationEmail;
    private String mCivilAviationSecondEmail;
    private Integer mTargetedAts;

    private Double mArrivalNorthLocation;
    private Double mArrivalEastLocation;
    private String mArrivalLocationAddress;

    private MutableLiveData<Integer> mLandingType;
    private MutableLiveData<Integer> mTakeOffType;

    private Plane mSelectedPlane;
    private Pilot mSelectedPilot;

    private String mTakeOffLocationInfo;

    // -- Take Off / Landing date
    private String mTakeOffDate;
    private String mLandingDate;

    //-- Autonomy estimation values --//
    private MutableLiveData<Integer> mAutonomyHours;
    private MutableLiveData<Integer> mAutonomyMinutes;
    private String mEstimatedFlightTime = "2";

    //-- Movements / Time estimations --//
    private MutableLiveData<Integer> mActivityTimeInDays;
    private MutableLiveData<Integer> mNumberOfMovements;

    private String mActivityType;
    private String mOwnerConsent;

    private String mEmailCarabinieri;

    private String mToken;

    private MutableLiveData<String> mBackendRequestMessage;
    private MutableLiveData<Boolean> mIsUploadingDataStatus;

    public CreateFlightPlanViewModel(@NonNull Application application) {
        super(application);
        PlanesDao planesDao = ((MainApp) getApplication()).getDatabaseManager().getPlanesDao();
        PilotsDao pilotsDao = ((MainApp) getApplication()).getDatabaseManager().getPilotsDao();
        EmailServerConfigDao emailServerConfigDao = ((MainApp) getApplication()).getDatabaseManager().getEmailServerDao();
        FlightPlanHistoryDao flightPlanHistoryDao = ((MainApp) getApplication()).getDatabaseManager().getFlightPlanHistoryDao();

        NetworkServices networkServices = ((MainApp) getApplication()).getNetworkServices();

        mPlanesRepository = new PlanesRepository(planesDao);
        mPilotRepository =  new PilotRepository(pilotsDao);
        mRemoteRepository = new RemoteRepository(networkServices);
        mEmailServerConfig = new EmailServerConfigRepository(emailServerConfigDao);
        mFlightPlanHistoryRepository = new FlightPlanHistoryRepository(flightPlanHistoryDao);

        mToken = ((MainApp) application).getDatabaseManager().getUserDao().getUserAccount().mSessionToken;

    }

    public MutableLiveData<String> getmBackendRequestMessage() {
        if (mBackendRequestMessage == null) {
            mBackendRequestMessage = new MutableLiveData<>();
        } return mBackendRequestMessage;
    }

    public LiveData<List<Plane>> getCurrentAvailablePlanes() {
        return mPlanesRepository.getAllPlanes();
    }

    public LiveData<List<Pilot>> getPilotRepository() {
        return mPilotRepository.getAllPilots();
    }

    public MutableLiveData<Integer> getTakeOffType() {
        if (mTakeOffType == null) {
            mTakeOffType = new MutableLiveData<>();
        } return mTakeOffType;
    }

    public MutableLiveData<Integer> getArrivalType() {
        if (mLandingType == null) {
            mLandingType = new MutableLiveData<>();
        }
        return mLandingType;
    }

    public MutableLiveData<Integer> getStopCount() {
        if (mNumberOfMovements == null) {
            mNumberOfMovements = new MutableLiveData<>();
            mNumberOfMovements.setValue(0);
        }
        return mNumberOfMovements;
    }

    public MutableLiveData<Integer> getActivityTimeInDays() {
        if (mActivityTimeInDays == null) {
            mActivityTimeInDays = new MutableLiveData<>();
            mActivityTimeInDays.setValue(0);
        }
        return mActivityTimeInDays;
    }

    public MutableLiveData<Integer> getAutonomyHours() {
        if (mAutonomyHours == null) {
            mAutonomyHours = new MutableLiveData<>();
            mAutonomyHours.setValue(0);
        }
        return mAutonomyHours;
    }

    public MutableLiveData<Integer> getAutonomyMinutes() {
        if (mAutonomyMinutes == null) {
            mAutonomyMinutes = new MutableLiveData<>();
            mAutonomyMinutes.setValue(0);
        }
        return mAutonomyMinutes;
    }

    public void saveCivilAviationEmail (@NonNull String civilAviationEmail) {
        Log.d(TAG , "Saving civil aviation email " + civilAviationEmail);
        mCivilAviationEmail = civilAviationEmail;
    }
    public void saveCivilAviationSecondEmail (@NonNull String civilAviationSecondEmail) {
        Log.d(TAG , "Saving civil aviation secondary email " + civilAviationSecondEmail);
        mCivilAviationSecondEmail = civilAviationSecondEmail;
    }

    public void saveSelectedPilot (@NonNull Pilot pilot) {
        Log.d(TAG , "Saving selected pilot with id -> " + pilot.getId());
        mSelectedPilot = pilot;
    }

    public void saveSelectedPlane (@NonNull Plane plane){
        Log.d(TAG , "Saving selected planes with id -> " + plane.getId());
        mSelectedPlane = plane;
    }

    public void setTakeOffTypeAirport () {
        mTakeOffType.setValue(AIRFIELD);
    }

    public void setTakeOffTypeHelipad () {
        mTakeOffType.setValue(HELIPAD);
    }

    public void setTakeOffTypeHidropad () {
        mTakeOffType.setValue(HIDROPAD);
    }

    public void setTakeOffTypeNone () {
        mTakeOffType.setValue(NO_TAKEOFF_TYPE_SELECTED);
    }

    public void setArrivalTypeAirfield () {
        mLandingType.setValue(AIRFIELD);
    }

    public void setArrivalTypeHelipad () {
        mLandingType.setValue(HELIPAD);
    }

    public void setArrivalTypeHidropad () {
        mLandingType.setValue(HIDROPAD);
    }

    public void saveTakeOffLocationInfo (String takeOffLocationInfo) {
        Log.d(TAG , "Saving take off location info as " + takeOffLocationInfo);
        mTakeOffLocationInfo = takeOffLocationInfo;
    }

    public void setmTargetedAts(Integer mTargetedAts) {
        this.mTargetedAts = mTargetedAts;
    }

    public void setArrivalNorthLocation(Double mArrivalNorthLocation) {
        this.mArrivalNorthLocation = mArrivalNorthLocation;
    }

    public void setArrivalEastLocation(Double mArrivalEastLocation) {
        this.mArrivalEastLocation = mArrivalEastLocation;
    }

    public void setArrivalLocationAdress(String mArrivalLocationAdress) {
        this.mArrivalLocationAddress = mArrivalLocationAdress;
    }

    public void setmTakeOffDate(String mTakeOffDate) {
        Log.d(TAG , "Setting take off date to : " + mTakeOffDate);
        this.mTakeOffDate = mTakeOffDate;
    }

    public void setmLandingDate(String mLandingDate) {
        Log.d(TAG , "Setting take off date to : " + mLandingDate);
        this.mLandingDate = mLandingDate;
    }

    public void setAutonomyHours(int autonomyHours) {
        if (mAutonomyHours != null) {
            mAutonomyHours.setValue(autonomyHours);
        }
    }

    public void setAutonomyMinutes(int autonomyMinutes) {
        if (mAutonomyMinutes != null) {
            mAutonomyMinutes.setValue(autonomyMinutes);
        }
    }

    public void increaseNumberOfMovements() {
            if (mNumberOfMovements.getValue() != null) {
                int currentVal = mNumberOfMovements.getValue();
                mNumberOfMovements.setValue(currentVal + 1);
            }
    }

    public void decreaseNumberOfMovements() {
        if (mNumberOfMovements.getValue() != null) {
            int currentVal = mNumberOfMovements.getValue();
            if (currentVal >= 1) {
                mNumberOfMovements.setValue(currentVal - 1);
            }
        }
    }

    public void increaseActivityDays() {
        if (mActivityTimeInDays.getValue() != null) {
            int currentVal = mActivityTimeInDays.getValue();
            mActivityTimeInDays.setValue(currentVal + 1);
        }
    }

    public void decreaseActivityDays() {
        if (mActivityTimeInDays.getValue() != null) {
            int currentVal = mActivityTimeInDays.getValue();
            if (currentVal >= 1) {
                mActivityTimeInDays.setValue(currentVal - 1);
            }
        }
    }

    public void setOwnerConsent(String mOwnerConsent) {
        Log.d(TAG , "Adding owner consent -> " + mOwnerConsent);
        this.mOwnerConsent = mOwnerConsent;
    }

    public void setActivityType(String mActivityType) {
        this.mActivityType = mActivityType;
    }
    public void setmEmailCarabinieri(String mEmailCarabinieri) {
        this.mEmailCarabinieri = mEmailCarabinieri;
    }


    public void prepareFlightPlanUpload(){
        if(validateAllNecesaryFields()){
            PlanRequest planRequest =  createFlightPlanData();

            CreatePlanRequest createPlanRequest = new CreatePlanRequest();
            createPlanRequest.setmPlanRequest(planRequest);
            createPlanRequest.setmSessionTkn(mToken);

            EmailServerConfig emailServerConfig = mEmailServerConfig.getEmailServerConfig();

            EmailServerConfigRequest  emailServerConfigRequest = new EmailServerConfigRequest();

            emailServerConfigRequest.setmHost(emailServerConfig.getHost());
            emailServerConfigRequest.setmPort(emailServerConfig.getPort());
            emailServerConfigRequest.setmSecure(emailServerConfig.isSecure());

            AuthRequest authRequest = new AuthRequest();
            if (emailServerConfig.isSecure()) {
                authRequest.setPass(emailServerConfig.getEmailServerAuthPassword());
                authRequest.setUser(emailServerConfig.getEmailServerAuthUsername());
                emailServerConfigRequest.setmFrom(emailServerConfig.getFrom());
            } else {
                authRequest.setUser("");
                authRequest.setPass("");
                emailServerConfigRequest.setmFrom("");
            }
            emailServerConfigRequest.setmAuthRequest(authRequest);

            createPlanRequest.setmEmailConfig(emailServerConfigRequest);

            uploadFlightPlan(createPlanRequest);
        }
    }

    private PlanRequest createFlightPlanData() {
        PlanRequest currentFlightPlanRequest = new PlanRequest();

        PilotRequest pilotRequest = new PilotRequest();
        pilotRequest.setPilotName(mSelectedPilot.getFamilyName() + " " + mSelectedPilot.getFirstName());
        pilotRequest.setLicenseNr(mSelectedPilot.getLicenseNr());
        pilotRequest.setPhoneNr(mSelectedPilot.getPhoneNr());

        currentFlightPlanRequest.setPilot(pilotRequest);
        if (mAutonomyHours.getValue() != null && mAutonomyMinutes.getValue() != null ) {
            int autHours = mAutonomyHours.getValue();
            int autMinutes = mAutonomyMinutes.getValue();
            String formattedAutonony = autHours + " H " + autMinutes + " M";
            mSelectedPlane.setAutonomy(formattedAutonony);
        } else {
            Log.d(TAG , "Autonomy data is missing");
        }

        PlaneRequest planeRequest = new PlaneRequest();
        planeRequest.setAutonomy(mSelectedPlane.getAutonomy());
        planeRequest.setPlaneName(mSelectedPlane.getName());
        currentFlightPlanRequest.setPlane(planeRequest);

        TripRequest tripData = prepareTripData();
        LegalsRequest legalsRequest = prepareLegalsData();

        ApsRequest apsRequest = prepareApsData();
        legalsRequest.setmApsRequest(apsRequest);

        currentFlightPlanRequest.setLegalsRequest(legalsRequest);
        currentFlightPlanRequest.setTripRequest(tripData);

        return currentFlightPlanRequest;
    }

    private ApsRequest prepareApsData(){
        ApsRequest apsRequest = new ApsRequest();
        apsRequest.setmSendEmailToCarabinieri(true);
        apsRequest.setmMail(mEmailCarabinieri);
        apsRequest.setmCarabinieriName("test");
        return apsRequest;
    }

    private boolean validateAllNecesaryFields(){
        return validateAdditionalTripData()
                && validateTripFromData()
                && validateTripToData()
                && validateLegalsData()
                && validateSelectedPilotAndPlane();
    }

    private boolean validateSelectedPilotAndPlane(){
        if (mSelectedPlane != null && mSelectedPilot != null){
            return true;
        } else {
            Log.d(TAG, "Pilot or plane is not assigned");
            return false;
        }
    }

    /** Returns boolean depending if data from legals is ok or not
     */
    private boolean validateLegalsData() {
        if (mCivilAviationEmail != null && !mCivilAviationEmail.isEmpty()) {
             if (mCivilAviationSecondEmail != null && !mCivilAviationSecondEmail.isEmpty()) {
                 if (mTargetedAts == 0 || mTargetedAts == 1 || mTargetedAts == 2) {
                     Log.d(TAG , "All LegalsRequest data is valid");
                     return true;
                   } else {
                     mBackendRequestMessage.setValue("Please aselect the targeted authority");
                     Log.w(TAG , "mTargetData is invalid");
                     return false;
                  }
             } else {
                 Log.w(TAG , "mCivilAviationSecondEmail is invalid");
                 return false;
             }
          } else {
              Log.w(TAG , "mCivilAviationEmail is invalid");
              return true;
        }
    }

    @NonNull
    private LegalsRequest prepareLegalsData() {
        LegalsRequest legalsRequest = new LegalsRequest();
        legalsRequest.setmCivilAviationEmail(mCivilAviationEmail);
        legalsRequest.setmCivilAviationSecondaryEmail(mCivilAviationSecondEmail);
        legalsRequest.setmTargeteted_Ats(mTargetedAts);
        return legalsRequest;
    }

    /** Validate trip data
     */
    private boolean validateAdditionalTripData() {
             if (mNumberOfMovements.getValue() != null) {
                 if (mActivityTimeInDays.getValue() != null) {
                     if (mLandingDate != null && !mLandingDate.isEmpty()) {
                         if (mTakeOffDate != null && !mTakeOffDate.isEmpty()) {
                             if( mOwnerConsent != null && !mOwnerConsent.isEmpty()) {
                                 Log.d(TAG , "All additional trip data is valid");
                                 return true;
                             } else {
                                 mBackendRequestMessage.setValue("Please complete the owner consent");
                                 Log.w(TAG , "mOwnerConsent is invalid , please complete all the forms");
                                 return false;
                             }
                         } else {
                             mBackendRequestMessage.setValue("Please add the take off date");
                             Log.w(TAG , "mTakeOffDate is invalid , please complete all the forms");
                             return false;
                         }
                     } else {
                         mBackendRequestMessage.setValue("Please add the landing date");
                         Log.w(TAG , "mLandingDate is invalid , please complete all the forms");
                         return false;
                     }
                 } else {
                     mBackendRequestMessage.setValue("Please add the activity time");
                     Log.w(TAG , "mActivityTimeInDays is invalid , please complete all the forms");
                     return false;
                 }
             } else {
                 mBackendRequestMessage.setValue("Please add the number of movements");
                 Log.w(TAG , "mNumberOfMovements is invalid , please complete all the forms");
                 return false;
         }
    }

    @NonNull
    private TripRequest prepareTripData() {
        TripRequest tripRequest = new TripRequest();

        tripRequest.setEstFlightTime(mEstimatedFlightTime);
        tripRequest.setShippedPplCount(mNumberOfMovements.getValue());
        tripRequest.setStopCount(mActivityTimeInDays.getValue());
        tripRequest.setLandingDate(mLandingDate);
        tripRequest.setTakeOffDate(mTakeOffDate);
        tripRequest.setOwnerConsent(mOwnerConsent);
        tripRequest.setmActivityType(mActivityType);

        tripRequest.setTripFromRequest(prepareTripFromData());
        tripRequest.setTripToRequest(prepareTripToData());
        return tripRequest;
    }

    /** Validates tripTo data
     */
    private boolean validateTripToData (){
        if (mArrivalEastLocation != null ) {
            if (mArrivalNorthLocation != null) {
                if (mArrivalLocationAddress != null && !mArrivalLocationAddress.isEmpty()) {
                    if (mTakeOffType != null && mTakeOffType.getValue() != null) {
                        Log.d(TAG , "All tripTo data is valid");
                        return true;
                    } else {
                        Log.w(TAG, "mTakeOffType is invalid");
                        return false;
                    }
                } else {
                    Log.w(TAG, "mArrivalLocationAddress is invalid");
                    return false;
                }
            } else {
                Log.w(TAG, "mArrivalLocationNorthLocation is invalid");
                return false;
            }
        } else {
                Log.w(TAG, "mArrivalLocationEastLocation is invalid");
                return false;
        }
    }
    /**
      Prepares tripTo data
     */
    @NonNull
    private TripToRequest prepareTripToData() {
        TripToRequest tripToRequest = new TripToRequest();
        tripToRequest.setTripToLatitude(mArrivalEastLocation);
        tripToRequest.setTripToLongitude(mArrivalNorthLocation);
        tripToRequest.setTripToInfo(mArrivalLocationAddress);
        tripToRequest.setTripToType(mTakeOffType.getValue());
        return tripToRequest;
    }

    /** Validates tripFrom data
     */
    private boolean validateTripFromData (){
        if( mTakeOffLocationInfo != null && !mTakeOffLocationInfo.isEmpty()) {
            if (!mArrivalLocationAddress.isEmpty()) {
                if (mTakeOffType.getValue() != null) {
                    Log.d(TAG , "All tripFrom data is valid");
                    return true;
                } else {
                    Log.w(TAG, "mTakeOffType value is invalid ");
                    return false;
                }
            } else {
                Log.w(TAG, "mArrivalLocation value is invalid ");
                return false;
            }
        } else {
            Log.w(TAG, "mTakeOffLocationInfo value is invalid ");
            return false;
        }
    }

    /**Prepares tripFrom data
     */
    @NonNull
    private TripFromRequest prepareTripFromData(){
        TripFromRequest tripFromRequest = new TripFromRequest();
        tripFromRequest.setTripFromInfo(mTakeOffLocationInfo);
        tripFromRequest.setTripFromType(mTakeOffType.getValue());
        return tripFromRequest;
    }

    private void uploadFlightPlan(@NonNull CreatePlanRequest createPlanRequest){
        mIsUploadingDataStatus.setValue(true);
        mRemoteRepository.updateCreateFlightPlan(createPlanRequest)
                .subscribe(new Observer<Response<CreatePlanReponse>>() {
            @Override
            public void onSubscribe(Disposable d) { }

            @Override
            public void onNext(Response<CreatePlanReponse> response) {
                   if (response.code() == 200) {
                       mBackendRequestMessage.setValue("Flight plan was successfully created and sent");
                       int planId = response.body().getId();
                       Log.d(TAG , "Received status code " + response.code() + " and assigned plan id -> " + planId);
                       saveCurrentFlightPlan(planId, createPlanRequest.getmPlanRequest());

                   } else if (response.code() == 500) {
                       Log.d(TAG , "Received status code " + response.code());
                       mBackendRequestMessage.setValue("Could not send the email");
                   }
                   mIsUploadingDataStatus.setValue(false);
            }

            @Override
            public void onError(Throwable e) {
                mIsUploadingDataStatus.setValue(false);
                e.printStackTrace();
            }

            @Override
            public void onComplete() { }
        });
    }
    public MutableLiveData<Boolean> getIsUploadingDataStatus() {
        if (mIsUploadingDataStatus == null) {
            mIsUploadingDataStatus = new MutableLiveData<>();
            mIsUploadingDataStatus.setValue(false);
        } return mIsUploadingDataStatus;
    }

    private void saveCurrentFlightPlan(int planId , @NonNull PlanRequest planRequest) {

        Plan plan = new Plan();
        plan.setId(planId);

        TripRequest tripRequest = planRequest.getTripRequest();

        /*  */
        Trip trip = new Trip();
        trip.setPlanId(planId);
        trip.setTakeOffDate(tripRequest.getTakeOffDate());
        trip.setLandingDate(tripRequest.getLandingDate());
        trip.setActivityType(tripRequest.getmActivityType());
        trip.setEstFlightTime(tripRequest.getEstFlightTime());
        trip.setOwnerConsent(tripRequest.getOwnerConsent());
        trip.setStopCount(tripRequest.getStopCount());

        TripFromRequest tripFromRequest = tripRequest.getTripFromRequest();

        TripFrom tripFrom = new TripFrom();
        tripFrom.setTripFromInfo(tripFromRequest.getTripFromInfo());
        tripFrom.setTripFromType(tripFromRequest.getTripFromType());
        tripFrom.setPlanId(planId);

        TripToRequest tripToRequest = tripRequest.getTripToRequest();

        TripTo tripTo = new TripTo();
        tripTo.setPlanId(planId);
        tripTo.setTripToType(tripToRequest.getTripToType());
        tripTo.setTripToLatitude(tripToRequest.getTripToLatitude());
        tripTo.setTripToLongitude(tripToRequest.getTripToLongitude());
        tripTo.setTripToInfo(tripToRequest.getTripToInfo());

        LegalsRequest legalsRequest = planRequest.getLegalsRequest();

        Legals legals = new Legals();
        legals.setCivilAviationSecondaryEmail(legalsRequest.getmCivilAviationSecondaryEmail());
        legals.setCivilAviationEmail(legalsRequest.getmCivilAviationEmail());
        legals.setPlanId(planId);
        legals.setTargeteted_Ats(legalsRequest.getmTargeteted_Ats());

        mFlightPlanHistoryRepository.insertNewFLightPlan(plan, trip, tripFrom, tripTo, legals);
    }

}
