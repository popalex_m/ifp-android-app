package ro.solutions.rr.flightplan.network.create;

public class CreatePlanReponse {

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
