package ro.solutions.rr.flightplan.ui.mainNav.user.main;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.solutions.rr.flightplan.R;
import ro.solutions.rr.flightplan.ui.mainNav.main.MainActivityViewModel;
import ro.solutions.rr.flightplan.ui.mainNav.user.tabs.email.EmailConfigFragment;
import ro.solutions.rr.flightplan.ui.mainNav.user.tabs.profile.UserProfileUpdateFragment;

public class UserManagementFragment extends Fragment implements TabLayout.OnTabSelectedListener,
                                                                ViewPager.OnPageChangeListener,
                                                                View.OnClickListener{

    private String TAG = UserManagementFragment.class.getSimpleName();

    private String USER_PROFILE_TAG = UserProfileUpdateFragment.class.getSimpleName();
    private String EMAIL_CONFIG_TAG = EmailConfigFragment.class.getSimpleName();

    @BindView(R.id.user_account_tab_layout)
    TabLayout mAccountPagesTabs;
    @BindView(R.id.user_account_viewpager)
    ViewPager mUserAccountViewPager;
    @BindView(R.id.fabSaveSettings)
    Button mSaveSettings;

    private AppCompatActivity mActivity;
    int mCurrentSelectedPage;

    private MainActivityViewModel mMainActivityViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_user_account, container, false);
        ButterKnife.bind(this, rootView);
        setHasOptionsMenu(true);
        initViewPager();
        mMainActivityViewModel = ViewModelProviders.of(mActivity).get(MainActivityViewModel.class);
        mSaveSettings.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity)context;
        mActivity.setTitle("User Account");
    }

    private void initViewPager() {
        Log.d(TAG , "Initializing view pager");
        PagerAdapter mViewPagerAdapter = new PagerAdapter(getFragmentManager());
        mViewPagerAdapter.addFragment(new UserProfileUpdateFragment() , "User Account Settings");
        mViewPagerAdapter.addFragment(new EmailConfigFragment() , "Email Configuration Settings ");

        mUserAccountViewPager.addOnPageChangeListener(this);
        mUserAccountViewPager.setAdapter(mViewPagerAdapter);
        mAccountPagesTabs.setupWithViewPager(mUserAccountViewPager);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem item = menu.findItem(R.id.action_edit);
        item.setVisible(false);
        menu.getItem(1).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mUserAccountViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) { }

    @Override
    public void onTabReselected(TabLayout.Tab tab) { }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

    @Override
    public void onPageSelected(int position) {
        mCurrentSelectedPage = position;
        switch (position){
            case 0 :
                mSaveSettings.setText("Save User Settings");
                break;
            case 1:
                mSaveSettings.setText("Save Email Settings");
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) { }

    @Override
    public void onClick(View view) {
        switch (mCurrentSelectedPage){
            case 0:
                mMainActivityViewModel.saveDataInUpdateProfileFrag(true);
                break;
            case 1:
                mMainActivityViewModel.saveDataInEmailFrag(true);
                break;
        }
    }

    public class PagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        PagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
