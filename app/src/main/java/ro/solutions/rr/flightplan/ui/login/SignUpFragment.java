package ro.solutions.rr.flightplan.ui.login;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.solutions.rr.flightplan.R;

public class SignUpFragment extends Fragment implements View.OnClickListener,
                                                        CompoundButton.OnCheckedChangeListener {

    private String TAG = SignUpFragment.class.getSimpleName();

    @BindView(R.id.edit_text_email)
    EditText mEditTextEmail;
    @BindView(R.id.edit_text_password)
    EditText mEditTextPassword;
    @BindView(R.id.edit_text_confirm_password)
    EditText mEditTextConfirmPassword;
    @BindView(R.id.edit_text_last_name)
    EditText mEditTextLastName;
    @BindView(R.id.edit_text_first_name)
    EditText mEditTextFirstName;

    @BindView(R.id.btn_sign_up)
    Button mBtnSignUp;
    @BindView(R.id.imageViewBackBtn)
    ImageView mBackImgView;
    @BindView(R.id.switch_confirm_toggle)
    Switch mConfirmToggle;

    private Boolean mConfirmSwitchStatus;
    private AppCompatActivity mActivity;
    private SignUpFragmentViewModel mSignUpFragmentViewModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sign_up, container, false);
        ButterKnife.bind(this, rootView);
        initListeners();
        mConfirmSwitchStatus = false;
        mSignUpFragmentViewModel = ViewModelProviders.of(this).get(SignUpFragmentViewModel.class);
        initViewModelObservers();
        return rootView;
    }

    private void initViewModelObservers() {
        mSignUpFragmentViewModel.getSignUpRequestStatus().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean signUpStatus) {
                if (signUpStatus != null) {
                    if (signUpStatus) {
                        switchBackToSignIn();
                    }
                }
            }
        });
        mSignUpFragmentViewModel.getSignUpRequestMessage().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String signUpMessage) {
                if (signUpMessage != null) {
                    if (!signUpMessage.isEmpty()) {
                        showMessageToast(signUpMessage);
                    }
                }
            }
        });
    }

    private void initListeners() {
        mBtnSignUp.setOnClickListener(this);
        mBackImgView.setOnClickListener(this);
        mConfirmToggle.setOnCheckedChangeListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity) context;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sign_up :
                mSignUpFragmentViewModel.validateForms( mEditTextFirstName.getText().toString(),
                                                        mEditTextLastName.getText().toString(),
                                                        mEditTextEmail.getText().toString(),
                                                        mEditTextPassword.getText().toString(),
                                                        mEditTextConfirmPassword.getText().toString(),
                                                        mConfirmSwitchStatus);
                break;

            case R.id.imageViewBackBtn :
                Log.d(TAG , "backBtnImgView() has been requested");
                switchBackToSignIn();
                break;
        }
    }

    public void setFragment(Fragment fragment) {
        FragmentManager mFragmentManager = mActivity.getSupportFragmentManager();
        mFragmentManager.beginTransaction()
                .replace(R.id.main_login_fragment_container, fragment)
                .commit();
    }

    private void switchBackToSignIn() {
         LoginFragment fragment = new LoginFragment();
         setFragment(fragment);
    }

    private void showMessageToast(@NonNull String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean btnToggleStatus) {
        mConfirmSwitchStatus = btnToggleStatus;
    }
}
