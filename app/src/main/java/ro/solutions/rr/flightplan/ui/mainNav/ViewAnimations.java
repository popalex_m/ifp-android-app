package ro.solutions.rr.flightplan.ui.mainNav;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.LinearLayout;

public class ViewAnimations {

    private static ViewAnimations sInstance;

    public static synchronized ViewAnimations getViewAnimator() {
        if (sInstance == null) {
            sInstance = new ViewAnimations();
        }
        return sInstance;
    }

    public void startRevealAnimation(@NonNull CardView cardLayout, @NonNull LinearLayout mEditBtnsLayout ) {
        cardLayout.post(new Runnable() {
            @Override
            public void run() {
                if (cardLayout.isAttachedToWindow()) {
                    // get the center for the clipping circle
                    int cx = cardLayout.getMeasuredWidth() / 2;
                    int cy = cardLayout.getMeasuredHeight() / 2;
                    int startRadius = 0;
                    // get the final radius for the clipping circle
                    int endRadius = Math.max(cardLayout.getWidth(), cardLayout.getHeight()) / 2;
                    Animator anim = ViewAnimationUtils.createCircularReveal(cardLayout, cx, cy, startRadius, endRadius);
                    cardLayout.setVisibility(View.VISIBLE);
                    mEditBtnsLayout.setVisibility(View.VISIBLE);
                    anim.start();
                }
            }
        });
    }

    public void startHideRevealAnimation(@NonNull CardView layout, @NonNull LinearLayout mEditBtnsLayout) {
        layout.post(new Runnable() {
            @Override
            public void run() {
                if (layout.isAttachedToWindow()) {
                // get the center for the clipping circle
                int cx = layout.getMeasuredWidth() / 2;
                int cy = layout.getMeasuredHeight() / 2;
                // get the initial radius for the clipping circle
                int initialRadius = layout.getWidth() / 2;
                // create the animation (the final radius is zero)
                    Animator anim = ViewAnimationUtils.createCircularReveal(layout, cx, cy, initialRadius, 0);
                    // make the view invisible when the animation is done
                    anim.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                             layout.setVisibility(View.VISIBLE);
                             mEditBtnsLayout.setVisibility(View.INVISIBLE);
                        }
                    });
                    // start the animation
                    anim.start();
                }
            }
        });
    }
}
