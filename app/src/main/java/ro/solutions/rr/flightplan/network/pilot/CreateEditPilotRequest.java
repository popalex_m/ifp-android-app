package ro.solutions.rr.flightplan.network.pilot;

import com.google.gson.annotations.SerializedName;

import ro.solutions.rr.flightplan.database.models.Pilot;

public class CreateEditPilotRequest {

    @SerializedName("session")
    String mSession;

    @SerializedName("pilot")
    Pilot mPilot;

    public String getmSession() {
        return mSession;
    }

    public void setmSession(String mSession) {
        this.mSession = mSession;
    }

    public Pilot getmPilot() {
        return mPilot;
    }

    public void setmPilot(Pilot mPilot) {
        this.mPilot = mPilot;
    }
}
