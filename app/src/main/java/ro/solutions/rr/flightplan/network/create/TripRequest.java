package ro.solutions.rr.flightplan.network.create;

import com.google.gson.annotations.SerializedName;


public class TripRequest {

    @SerializedName("from")
    private TripFromRequest tripFromRequest;

    @SerializedName("to")
    private TripToRequest tripToRequest;

    @SerializedName("takeoff_date")
    private String takeOffDate;

    @SerializedName("landing_date")
    private String landingDate;

    @SerializedName("est_flight_time")
    private String estFlightTime;

    @SerializedName("stop_count")
    private int stopCount;

    @SerializedName("shipped_ppl_count")
    private int shippedPplCount;

    @SerializedName("owner_consent")
    private String ownerConsent;

    @SerializedName("activity_type")
    private String mActivityType;

    public TripFromRequest getTripFromRequest() {
        return tripFromRequest;
    }

    public void setTripFromRequest(TripFromRequest tripFromRequest) {
        this.tripFromRequest = tripFromRequest;
    }

    public TripToRequest getTripToRequest() {
        return tripToRequest;
    }

    public void setTripToRequest(TripToRequest tripToRequest) {
        this.tripToRequest = tripToRequest;
    }

    public String getTakeOffDate() {
        return takeOffDate;
    }

    public void setTakeOffDate(String takeOffDate) {
        this.takeOffDate = takeOffDate;
    }

    public String getLandingDate() {
        return landingDate;
    }

    public void setLandingDate(String landingDate) {
        this.landingDate = landingDate;
    }

    public String getEstFlightTime() {
        return estFlightTime;
    }

    public void setEstFlightTime(String estFlightTime) {
        this.estFlightTime = estFlightTime;
    }

    public int getStopCount() {
        return stopCount;
    }

    public void setStopCount(int stopCount) {
        this.stopCount = stopCount;
    }

    public int getShippedPplCount() {
        return shippedPplCount;
    }

    public void setShippedPplCount(int shippedPplCount) {
        this.shippedPplCount = shippedPplCount;
    }

    public String getOwnerConsent() {
        return ownerConsent;
    }

    public void setOwnerConsent(String ownerConsent) {
        this.ownerConsent = ownerConsent;
    }

    public String getmActivityType() { return mActivityType; }

    public void setmActivityType(String mActivityType) { this.mActivityType = mActivityType; }
}
