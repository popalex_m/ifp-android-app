package ro.solutions.rr.flightplan.ui.mainNav.user.tabs.email;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.solutions.rr.flightplan.R;
import ro.solutions.rr.flightplan.database.models.EmailServerConfig;
import ro.solutions.rr.flightplan.ui.mainNav.main.MainActivityViewModel;

public class EmailConfigFragment extends Fragment {

    private String TAG = EmailConfigFragment.class.getSimpleName();

    @BindView(R.id.progress_bar_loading_user_account_data)
    ProgressBar mProgressBarLoading;

    @BindView(R.id.editText_emailConfig_username)
    EditText mEdtTextEmailConfigUsername;
    @BindView(R.id.editText_emailConfig_password)
    EditText mEdtTextEmailConfigPassword;
    @BindView(R.id.editText_emailConfig_confirm_password)
    EditText mEdtTextEmailConfigConfirmPass;
    @BindView(R.id.editText_emailConfig_from_email)
    EditText mEdtTextEmailConfigFromEmail;
    @BindView(R.id.editText_host)
    EditText mEdtTextEmailConfigHost;
    @BindView(R.id.editText_port)
    EditText mEdtTextEmailConfigPort;

    @BindView(R.id.toggle_secure_email)
    Switch mSecureToggle;

    private AppCompatActivity mActivity;
    private EmailConfigViewModel mEmailConfigViewModel;
    private MainActivityViewModel mMainActivityViewModel;
    private boolean mSecureEmailStatus;

    private MutableLiveData<Boolean> mSaveEmailConfig;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_setting_email, container, false);
        ButterKnife.bind(this, rootView);
        setHasOptionsMenu(true);

        mEmailConfigViewModel = ViewModelProviders.of(this).get(EmailConfigViewModel.class);
        mMainActivityViewModel = ViewModelProviders.of(mActivity).get(MainActivityViewModel.class);

        initViewModelObservers();
        initListeners();
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity)context;
    }

    private void initViewModelObservers() {
        mEmailConfigViewModel.getLoadingStatus().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean shouldShowLoading) {
                if (shouldShowLoading != null) {
                    if (shouldShowLoading) {
                        mProgressBarLoading.setVisibility(View.VISIBLE);
                    } else {
                        mProgressBarLoading.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });
        mEmailConfigViewModel.getEmailServerConfig().observe(this, new Observer<EmailServerConfig>() {
            @Override
            public void onChanged(@Nullable EmailServerConfig emailServerConfig) {
                if (emailServerConfig != null) {
                    initEmailServerConfig(emailServerConfig);
                }
            }
        });
        mEmailConfigViewModel.getOperationsMessage().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String msg) {
                if (msg!=null) {
                    if (!msg.isEmpty()) {
                        displayToast(msg);
                    }
                }
            }
        });
        mSaveEmailConfig =  mMainActivityViewModel.getSaveEmailConfigData();
        mSaveEmailConfig.observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean saveUserData) {
                    if (saveUserData != null && saveUserData) {
                        sendEmailConfig();
                        mSaveEmailConfig.setValue(false);
                    }
            }
        });
    }
    private void initListeners() {
        mSecureToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean toglStatus) {
                mSecureEmailStatus = toglStatus;
            }
        });
    }

    private void sendEmailConfig() {
            Log.d(TAG,  "Sending data from the email fragment to the email viewModel()") ;
            String emailConfigUserName = mEdtTextEmailConfigUsername.getText().toString();
            String emailConfigPassword = mEdtTextEmailConfigPassword.getText().toString();
            String emailConfigConfirmPassword = mEdtTextEmailConfigConfirmPass.getText().toString();
            String emailConfigFromEmail = mEdtTextEmailConfigFromEmail.getText().toString();
            String emailConfigHost = mEdtTextEmailConfigHost.getText().toString();
            Integer emailConfigPort = Integer.valueOf(mEdtTextEmailConfigPort.getText().toString());
            mEmailConfigViewModel.saveEmailServerConfig ( emailConfigUserName,
                                                          emailConfigPassword,
                                                          emailConfigConfirmPassword,
                                                          emailConfigFromEmail,
                                                          emailConfigHost,
                                                          emailConfigPort,
                                                          mSecureEmailStatus);
    }

    private void initEmailServerConfig(@NonNull EmailServerConfig serverConfig) {
        mEdtTextEmailConfigPassword.setText(serverConfig.getEmailServerAuthPassword());
        mEdtTextEmailConfigUsername.setText(serverConfig.getEmailServerAuthUsername());
        mEdtTextEmailConfigFromEmail.setText(serverConfig.getFrom());
        mEdtTextEmailConfigHost.setText(serverConfig.getHost());
        mEdtTextEmailConfigPort.setText(String.valueOf(serverConfig.getPort()));
        if (serverConfig.isSecure()) {
            mSecureToggle.setChecked(true);
        } else {
            mSecureToggle.setChecked(false);
        }
    }

    private void displayToast(String msg){
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
