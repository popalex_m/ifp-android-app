package ro.solutions.rr.flightplan.network.create;

import com.google.gson.annotations.SerializedName;

public class PlanHistoryResponse {

    @SerializedName("id")
    private int planId;
    @SerializedName("from_info")
    private String fromInfo;
    @SerializedName("to_info")
    private String toInfo;
    @SerializedName("takeoff_date")
    private String takeOffDate;
    @SerializedName("cancelled_on")
    private String cancelledOn;

    public int getPlanId() { return planId; }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public String getFromInfo() {
        return fromInfo;
    }

    public void setFromInfo(String fromInfo) {
        this.fromInfo = fromInfo;
    }

    public String getToInfo() {
        return toInfo;
    }

    public void setToInfo(String toInfo) {
        this.toInfo = toInfo;
    }

    public String getTakeOffDate() {
        return takeOffDate;
    }

    public void setTakeOffDate(String takeOffDate) {
        this.takeOffDate = takeOffDate;
    }

    public String getCancelledOn() {
        return cancelledOn;
    }

    public void setCancelledOn(String cancelledOn) {
        this.cancelledOn = cancelledOn;
    }

}
