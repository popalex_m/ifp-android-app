package ro.solutions.rr.flightplan.ui.mainNav.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.solutions.rr.flightplan.R;
import ro.solutions.rr.flightplan.database.models.Pilot;
import ro.solutions.rr.flightplan.ui.mainNav.OperationTypes;

public class AddEditPilotDialogFragment extends DialogFragment
                                                   implements
                                                            DialogInterface.OnClickListener{

    private String TAG = AddEditPilotDialogFragment.class.getSimpleName();

    @BindView(R.id.editText_pilot_first_name)
    EditText mEditTextFirstName;
    @BindView(R.id.editText_pilot_last_name)
    EditText mEditTextLastName;
    @BindView(R.id.editText_license_number)
    EditText mEditTextLicenseNumber;
    @BindView(R.id.editText_phone_number)
    EditText mEditTextPhoneNumber;

    private Pilot mToInsertPilot;
    private OperationTypes mOperationType;
    private AddEditPilotDialogListener mAddPilotDialogCallback;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View dialogView = inflater.inflate(R.layout.dialog_fragment_create_pilot, null);
        ButterKnife.bind(this, dialogView);
        builder.setView(dialogView);
        builder.setPositiveButton("OK",this);
        builder.setNegativeButton("Cancel",this);
        addTextChangeListeners();
        initData();
        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case Dialog.BUTTON_POSITIVE :
                mAddPilotDialogCallback = (AddEditPilotDialogListener) getTargetFragment();
                switch (mOperationType) {
                    case CREATE_NEW_PILOT:
                        if (mAddPilotDialogCallback != null) {
                            mAddPilotDialogCallback.onPilotAdded(mToInsertPilot);
                        }
                        break;
                    case EDIT_EXISTING_PILOT:
                        if (mAddPilotDialogCallback != null) {
                            mAddPilotDialogCallback.onPilotEdited(mToInsertPilot);
                        }
                        break;
                }
                break;

            case Dialog.BUTTON_NEGATIVE :
                dismiss();
                break;
        }
    }

    private void addTextChangeListeners () {
        mEditTextFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            public void afterTextChanged(Editable s) {
               mToInsertPilot.setFirstName(s.toString());
            }
        });

        mEditTextLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            public void afterTextChanged(Editable s) {
                mToInsertPilot.setFamilyName(s.toString());
            }
        });

        mEditTextLicenseNumber.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                mToInsertPilot.setLicenseNr(s.toString());
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
        });

        mEditTextPhoneNumber.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                mToInsertPilot.setPhoneNr(s.toString());
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
        });
    }

    private void initData() {
        if (mToInsertPilot == null) {
            mToInsertPilot = new Pilot();
        } else {
            mEditTextFirstName.setText(mToInsertPilot.getFirstName());
            mEditTextLastName.setText(mToInsertPilot.getFamilyName());
            mEditTextLicenseNumber.setText(mToInsertPilot.getLicenseNr());
            mEditTextPhoneNumber.setText(mToInsertPilot.getPhoneNr());
        }
    }

    public void setPilot(@NonNull Pilot pilot) {
        mToInsertPilot = pilot;
    }

    public void setOperationType(OperationTypes operationType) {
        mOperationType = operationType;
    }
}
