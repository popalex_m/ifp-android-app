package ro.solutions.rr.flightplan.ui.mainNav.pilots;

import ro.solutions.rr.flightplan.database.models.Pilot;

public interface EditPilotCallback {

    void onPilotEditDataClick(Pilot pilot);

    void onPilotDeleteClick(Pilot pilot);

}
