package ro.solutions.rr.flightplan.network.create;

import com.google.gson.annotations.SerializedName;

public class PlanRequest {

    @SerializedName("legals")
    private LegalsRequest legalsRequest;

    @SerializedName("trip")
    private TripRequest tripRequest;

    @SerializedName("pilot")
    private PilotRequest pilot;

    @SerializedName("plane")
    private PlaneRequest plane;

    public LegalsRequest getLegalsRequest() {
        return legalsRequest;
    }

    public void setLegalsRequest(LegalsRequest legalsRequest) {
        this.legalsRequest = legalsRequest;
    }

    public TripRequest getTripRequest() {
        return tripRequest;
    }

    public void setTripRequest(TripRequest tripRequest) {
        this.tripRequest = tripRequest;
    }

    public PilotRequest getPilot() {
        return pilot;
    }

    public void setPilot(PilotRequest pilot) {
        this.pilot = pilot;
    }

    public PlaneRequest getPlane() {
        return plane;
    }

    public void setPlane(PlaneRequest plane) {
        this.plane = plane;
    }
}
