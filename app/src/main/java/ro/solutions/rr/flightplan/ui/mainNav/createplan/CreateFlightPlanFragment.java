package ro.solutions.rr.flightplan.ui.mainNav.createplan;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.solutions.rr.flightplan.R;
import ro.solutions.rr.flightplan.database.models.Pilot;
import ro.solutions.rr.flightplan.database.models.Plane;
import ro.solutions.rr.flightplan.ui.mainNav.main.MainActivityViewModel;
import ro.solutions.rr.flightplan.utils.DialogUtils;
import ro.solutions.rr.flightplan.utils.LocationUtils;
import ro.solutions.rr.flightplan.utils.TimeUtils;

import static ro.solutions.rr.flightplan.ui.mainNav.SpinnerHardcodedTypes.MANUAL_INPUT_OPTION;

public class CreateFlightPlanFragment extends Fragment implements View.OnClickListener{

    private String TAG = CreateFlightPlanFragment.class.getSimpleName();

    public static String TAG_MAP_PICKER_FRAGMENT = MapFragment.class.getSimpleName();
    public static String TAG_CREATE_DOCUMENT = CreateFlightPlanFragment.class.getSimpleName();

    @BindView(R.id.spinner_selector_email_input)
    Spinner mSpinnerEmailInput;
    @BindView(R.id.layout_text_input_manual_email_input)
    TextInputLayout mLayoutTextInputManualEmail;
    @BindView(R.id.editTxt_manual_email_input)
    EditText mEdtTextATMManualEmailInput;
    @BindView(R.id.editTxt_aeronatuica_militare_enav_email_input)
    EditText mEdtTextAeronauticaMilitareENAVEmail;


    @BindView(R.id.switch_aeronautica_militare_select)
    Switch mSwitchAeronauticaMilitare;
    @BindView(R.id.switch_enav_select)
    Switch mSwitchEnavSelect;
    @BindView(R.id.textInputLayout_atsenav_email_input)
    TextInputLayout mLayoutAeronatuicaMilitareENAVLayout;

     // ---- Arrival from layout items --- //
    @BindView(R.id.arrival_location_selection_layout)
    LinearLayout mArrivalEnterLocationLayout;

    @BindView(R.id.spinner_selector_airplane)
    Spinner mSpinnerAirplaneSelector;
    @BindView(R.id.switch_arrival_airfield)
    Switch mSwitchArrivalAirfield;
    @BindView(R.id.switch_arrival_helipad)
    Switch mSwitchArrivalHelipad;
    @BindView(R.id.switch_arrival_hidro_surface)
    Switch mSwitchArrivalHydroSurface;

    @BindView(R.id.edtText_North_Coordinates)
    EditText mEdtTextArrivalNorthCoord;
    @BindView(R.id.edtText_East_Coordinates)
    EditText mEdtTextArrivalEastCoord;
    @BindView(R.id.edtText_arrival_adress)
    EditText mEditTextArrivalAddress;

    // --- Departure from from layout switches --///
    @BindView(R.id.editText_departure_take_off)
    TextInputLayout mDepartureEnterLocationLayout;
    @BindView(R.id.switch_departure_from_airfield)
    Switch mSwitchDepartureAirfield;
    @BindView(R.id.switch_departure_from_helipad)
    Switch mSwitchDepartureHelipad;
    @BindView(R.id.switch_departure_from_hidro_surface)
    Switch mSwitchDepartureHydroSurface;
    @BindView(R.id.edtText_departure_location_info)
    EditText mEdtTextDepartureLocationInfo;
    @BindView(R.id.layoutSwitchToMapPickerDeparture)
    LinearLayout mSwitchToMapPickerLayout;

    // -- Take Off / Landing date edit text -- //
    @BindView(R.id.editText_landing_date_time)
    EditText mEditTextLandingDate;
    @BindView(R.id.editText_takeoff_date_time)
    EditText mEditTextTakeOffDate;

    @BindView(R.id.spinnerSelectAssignedPilot)
    Spinner mSelectAssignedPilot;

    // --- Autonomy / activity duration sliders --///
    @BindView(R.id.seekBarAutonomyHours)
    SeekBar mSeekBarAutonomyHours;
    @BindView(R.id.seekBarAutonomyMinutes)
    SeekBar mSeekBarAutonomyMinutes;
    @BindView(R.id.textViewAutonomyHours)
    TextView mTextViewAutomonyHours;
    @BindView(R.id.textViewAutonomyMinutes)
    TextView mTextViewAutomonyMinutes;

    // -- Activity duration in days / Number of estimated movements --//
    @BindView(R.id.imageButtonMovementAddNum)
    ImageButton mImageButtonMovementIncrease;
    @BindView(R.id.imageButtonMovementDecrease)
    ImageButton mImageButtonMovementDecrease;
    @BindView(R.id.textViewMovementsProvided)
    TextView mTextViewMovementsProvided;

    // -- Activity duration in days / Number of estimated movements --//
    @BindView(R.id.imageButtonTimeAdd)
    ImageButton mImageButtonTimeIncrease;
    @BindView(R.id.imageButtonTimeDecrease)
    ImageButton mImageButtonTimeDecrease;
    @BindView(R.id.textViewDaysTime)
    TextView mTextViewDaysTime;

    // -- Owner consent / Activity Type-- //
    @BindView(R.id.edtTextOwnerConsent)
    EditText mEdtTextOwnerConsent;
    @BindView(R.id.spinnerActivityType)
    Spinner mSpinnerActivityType;

    //-- Public Authority Information --//
    @BindView(R.id.spinnerSelectInformationAuthority)
    Spinner mSpinnerSelectInformation;
    @BindView(R.id.carabinieri_information_layout)
    LinearLayout mLayoutCarabinieriInformation;

    private boolean mAeronauticaMilitareStatus;
    private boolean mENAVSelectStatus;

    private boolean mArrivalAirfieldStatus;
    private boolean mArrivalHidroSurfaceStatus;
    private boolean mArrivalArrivalHelipadStatus;

    private boolean mDepartureAirfieldStatus;
    private boolean mDeparturelHidroSurfaceStatus;
    private boolean mDepartureHelipadStatus;

    private AppCompatActivity mActivity;
    private CreateFlightPlanViewModel mCreateFlightPlanViewModel;
    private MainActivityViewModel mMainActivityViewModel;

    private ProgressDialog mProgressDialogUploadingData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_create_flight_plan, container, false);
        ButterKnife.bind(this, rootView);

        mCreateFlightPlanViewModel = ViewModelProviders.of(this).get(CreateFlightPlanViewModel.class);
        mMainActivityViewModel = ViewModelProviders.of(mActivity).get(MainActivityViewModel.class);

        initViewModelObservers();

        initAuthoritiresSpinnerListener();
        initENAVAeronauticaMilitareSwitchListeners();
        initArrivalSwitchListeners();
        initDepartureSwitchListeners();
        initTakeOffLandingDatePickerListeners();
        initAutonomyListeners();
        initActivityMovementsListeners();
        initActivityTypeSpinner();
        initAuthorityInformationSpinner();

        attachManualATMEmailInputListener();
        attachAeronauticaMilitareENAVEmailInputListener();
        attachDepartureLocationEdtTextListener();
        attachMapPickerDepartureListener();
        attachOwnerConsentListener();

        refreshAeronauitcaMilitareENAVEmailLayoutStatus();
        refreshArrivalSwitches();
        refreshDepartureSwitches();

        mProgressDialogUploadingData = DialogUtils.getLoadingDialog(mActivity , "Uploading flight plan");
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity)context;
        mActivity.setTitle("Create Document");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.getItem(0).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_confirm:
                mCreateFlightPlanViewModel.prepareFlightPlanUpload();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initViewModelObservers() {
        mCreateFlightPlanViewModel.getCurrentAvailablePlanes().observe(this, new Observer<List<Plane>>() {
            @Override
            public void onChanged(@Nullable List<Plane> planes) {
                      if (planes != null) {
                          initSelectPlanesSpinner(planes);
                      }
              }
        });
        mCreateFlightPlanViewModel.getPilotRepository().observe(this, new Observer<List<Pilot>>() {
            @Override
            public void onChanged(@Nullable List<Pilot> pilots) {
                if (pilots != null) {
                     initSelectPilotsSpinner(pilots);
                }
            }
        });
        mCreateFlightPlanViewModel.getTakeOffType().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer takeOffType) {
                if (takeOffType != null) {
                    switch (takeOffType) {
                        case 0:
                            mDepartureAirfieldStatus = true;
                            mSwitchDepartureHelipad.setChecked(false);
                            mSwitchDepartureHydroSurface.setChecked(false);
                            break;
                        case 1:
                            mDepartureHelipadStatus = true;
                            mSwitchDepartureAirfield.setChecked(false);
                            mSwitchDepartureHydroSurface.setChecked(false);
                            break;

                        case 2:
                            mDeparturelHidroSurfaceStatus = true;
                            mSwitchDepartureAirfield.setChecked(false);
                            mSwitchDepartureHelipad.setChecked(false);
                            break;
                    }
                }
            }
        });
        mCreateFlightPlanViewModel.getArrivalType().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer landingType) {
                if (landingType != null) {
                    switch (landingType) {
                        case 0:
                            mSwitchArrivalHelipad.setChecked(false);
                            mSwitchArrivalHydroSurface.setChecked(false);
                            break;
                        case 1:
                            mSwitchArrivalAirfield.setChecked(false);
                            mSwitchArrivalHydroSurface.setChecked(false);
                            break;

                        case 2:
                            mSwitchArrivalAirfield.setChecked(false);
                            mSwitchArrivalHelipad.setChecked(false);
                            break;
                    }
                }
            }
        });
        mMainActivityViewModel.getArrivalLocationData().observe(this, new Observer<LatLng>() {
            @Override
            public void onChanged(@Nullable LatLng selectedLatLng) {
                if (selectedLatLng != null) {
                   loadLocationPickerCoordinates(selectedLatLng);
                }
            }
        });
        mCreateFlightPlanViewModel.getAutonomyHours().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer hours) {
                if (hours != null){
                      mTextViewAutomonyHours.setText(String.format("%s H", String.valueOf(hours)));
                }
            }
        });
        mCreateFlightPlanViewModel.getAutonomyMinutes().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer minutes) {
                if (minutes != null){
                    mTextViewAutomonyMinutes.setText(String.format("%s M", String.valueOf(minutes)));
                }
            }
        });
        mCreateFlightPlanViewModel.getStopCount().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer movements) {
                if (movements != null) {
                    mTextViewMovementsProvided.setText(String.valueOf(movements));
                }
            }
        });

        mCreateFlightPlanViewModel.getActivityTimeInDays().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer activityTimeInDays) {
                if (activityTimeInDays != null) {
                    mTextViewDaysTime.setText(String.valueOf(activityTimeInDays));
                }
            }
        });
        mCreateFlightPlanViewModel.getmBackendRequestMessage().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String message) {
                if (message != null && !message.isEmpty()) {
                    showMessage(message);
                }
            }
        });

        mCreateFlightPlanViewModel.getIsUploadingDataStatus().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean isUploadingStatus) {
                if (isUploadingStatus != null && isUploadingStatus) {
                    showUploadingDialog();
                } else {
                    dismissUploadingDialog();
                }
            }
        });
    }

    private void loadLocationPickerCoordinates(@NonNull LatLng selectedLatLng) {
        Location location = new Location("");
        location.setLatitude(selectedLatLng.latitude);
        location.setLongitude(selectedLatLng.longitude);

        mCreateFlightPlanViewModel.setArrivalNorthLocation(selectedLatLng.latitude);
        mCreateFlightPlanViewModel.setArrivalEastLocation(selectedLatLng.longitude);

        String strLatitude = LocationUtils.formatLatitudeAsDMS(location, 1);
        String strLongitude = LocationUtils.formatLongitudeAsDMS(location, 1);

        String address = getAddress(selectedLatLng.latitude, selectedLatLng.longitude);
        mCreateFlightPlanViewModel.setArrivalLocationAdress(address);

        mEdtTextArrivalNorthCoord.setText(strLatitude);
        mEdtTextArrivalEastCoord.setText(strLongitude);
        mEditTextArrivalAddress.setText(address);
    }

    public String getAddress(double latitude, double longitude) {
        String foundAddress = "";
        try {
            Geocoder geocoder = new Geocoder(mActivity, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                foundAddress = city + " , " + address;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return foundAddress;
    }

    private void attachManualATMEmailInputListener() {
        mEdtTextATMManualEmailInput.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable text) {
                mCreateFlightPlanViewModel.saveCivilAviationSecondEmail(String.valueOf(text));
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) { }
        });

    }

    private void attachAeronauticaMilitareENAVEmailInputListener() {
        mEdtTextATMManualEmailInput.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable text) {
                mCreateFlightPlanViewModel.saveCivilAviationEmail(String.valueOf(text));
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) { }
        });
    }

    private void attachDepartureLocationEdtTextListener() {
        mEdtTextDepartureLocationInfo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                    mCreateFlightPlanViewModel.saveTakeOffLocationInfo(String.valueOf(editable));
            }
        });
    }

    private void attachOwnerConsentListener(){
        mEdtTextOwnerConsent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                    mCreateFlightPlanViewModel.setOwnerConsent(editable.toString());
            }
        });
    }

    private void attachMapPickerDepartureListener(){
        mSwitchToMapPickerLayout.setOnClickListener(this);
    }

    private void initSelectPlanesSpinner(@NonNull List<Plane> planes) {
        PlanesAdapter planesAdapter = new PlanesAdapter (mActivity.getApplicationContext(), planes);
        mSpinnerAirplaneSelector.setAdapter(planesAdapter);
        mSpinnerAirplaneSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                mCreateFlightPlanViewModel.saveSelectedPlane(planesAdapter.getItem(position));
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });
    }

    private void initSelectPilotsSpinner(@NonNull List<Pilot> pilots) {
        PilotsAdapter pilotsAdapter = new PilotsAdapter (mActivity.getApplicationContext(), pilots);
        mSelectAssignedPilot.setAdapter(pilotsAdapter);
        mSelectAssignedPilot.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                 mCreateFlightPlanViewModel.saveSelectedPilot(pilotsAdapter.getItem(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });
    }

    private void initAuthoritiresSpinnerListener(){
        ArrayAdapter<CharSequence> authoritiesAdapter = ArrayAdapter.createFromResource(mActivity,
               R.array.input_emails_authorities, android.R.layout.simple_list_item_1);
        mSpinnerEmailInput.setAdapter(authoritiesAdapter);
        mSpinnerEmailInput.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                String authoritiesSelection = mSpinnerEmailInput.getSelectedItem().toString();
                if (authoritiesSelection.contains(MANUAL_INPUT_OPTION)) {
                    mLayoutTextInputManualEmail.setVisibility(View.VISIBLE);
                } else {
                    mCreateFlightPlanViewModel.saveCivilAviationSecondEmail(authoritiesSelection);
                    mLayoutTextInputManualEmail.setVisibility(View.GONE);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });
    }

    /** Listeners for the switches on Aeronauitca Militare / ENAV switches
     */
    private void initENAVAeronauticaMilitareSwitchListeners(){
        mSwitchAeronauticaMilitare.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               mAeronauticaMilitareStatus = isChecked;
               if (mENAVSelectStatus) {
                   mSwitchEnavSelect.setChecked(false);
                   mENAVSelectStatus = false;
               }
                mCreateFlightPlanViewModel.setmTargetedAts(1);
                refreshAeronauitcaMilitareENAVEmailLayoutStatus();
            }
        });
        mSwitchEnavSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mENAVSelectStatus = isChecked;
                if (mAeronauticaMilitareStatus) {
                    mSwitchAeronauticaMilitare.setChecked(false);
                    mAeronauticaMilitareStatus = false;
                }
                mCreateFlightPlanViewModel.setmTargetedAts(2);
                refreshAeronauitcaMilitareENAVEmailLayoutStatus();
            }
        });
    }

    private void initArrivalSwitchListeners() {
        mSwitchArrivalAirfield.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCreateFlightPlanViewModel.setArrivalTypeAirfield();
                }
                mArrivalAirfieldStatus = isChecked;
                refreshArrivalSwitches();
            }
        });
        mSwitchArrivalHelipad.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCreateFlightPlanViewModel.setArrivalTypeHelipad();
                }
                mArrivalArrivalHelipadStatus = isChecked;
                refreshArrivalSwitches();
            }
        });
        mSwitchArrivalHydroSurface.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mArrivalHidroSurfaceStatus = isChecked;
                if (isChecked) {
                    mCreateFlightPlanViewModel.setArrivalTypeHidropad();
                }
                refreshArrivalSwitches();
            }
        });
    }

    private void initDepartureSwitchListeners() {
        mSwitchDepartureAirfield.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCreateFlightPlanViewModel.setTakeOffTypeAirport();
                }
                refreshDepartureSwitches();
            }
        });
        mSwitchDepartureHelipad.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCreateFlightPlanViewModel.setTakeOffTypeHelipad();
                }
                refreshDepartureSwitches();
            }
        });
        mSwitchDepartureHydroSurface.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCreateFlightPlanViewModel.setTakeOffTypeHidropad();
                }
                refreshDepartureSwitches();
            }
        });
    }

    private void refreshAeronauitcaMilitareENAVEmailLayoutStatus(){
        if (mENAVSelectStatus || mAeronauticaMilitareStatus) {
            mLayoutAeronatuicaMilitareENAVLayout.setVisibility(View.VISIBLE);
        } else {
            mLayoutAeronatuicaMilitareENAVLayout.setVisibility(View.GONE);
        }
    }

    private void refreshArrivalSwitches(){
        if (mArrivalAirfieldStatus|| mArrivalHidroSurfaceStatus || mArrivalArrivalHelipadStatus) {
            mArrivalEnterLocationLayout.setVisibility(View.VISIBLE);
        } else {
            mArrivalEnterLocationLayout.setVisibility(View.GONE);

        }
    }

    private void refreshDepartureSwitches(){
        if (mDepartureAirfieldStatus|| mDeparturelHidroSurfaceStatus || mDepartureHelipadStatus) {
            mDepartureEnterLocationLayout.setVisibility(View.VISIBLE);
        } else {
            mDepartureEnterLocationLayout.setVisibility(View.GONE);
        }
    }

    private void initTakeOffLandingDatePickerListeners(){
        mEditTextTakeOffDate.setOnClickListener(this);
        mEditTextLandingDate.setOnClickListener(this);
    }

    /** Init Autonomy Seekbar Listeners
     */
    private void initAutonomyListeners() {
        mSeekBarAutonomyHours.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                mCreateFlightPlanViewModel.setAutonomyHours(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });
        mSeekBarAutonomyMinutes.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                       mCreateFlightPlanViewModel.setAutonomyMinutes(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });
    }

    private void initActivityTypeSpinner() {
        mSpinnerActivityType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                mCreateFlightPlanViewModel.setActivityType(mSpinnerActivityType.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void initAuthorityInformationSpinner() {
        mSpinnerSelectInformation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selection =  mSpinnerSelectInformation.getSelectedItem().toString();
                if (selection.equals("SI")) {
                    setManualCarabinieriInputVisibility(true);
                } else {
                    setManualCarabinieriInputVisibility(false);
                    mCreateFlightPlanViewModel.setmEmailCarabinieri(selection);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });
    }

    private void setManualCarabinieriInputVisibility(boolean visible) {
        if (visible) {
            mLayoutCarabinieriInformation.setVisibility(View.VISIBLE);
        } else {
            mLayoutCarabinieriInformation.setVisibility(View.GONE);
        }
    }

    /**
      Attach Activity time / Number of movements buttons
    */
    private void initActivityMovementsListeners(){
        mImageButtonMovementIncrease.setOnClickListener(this);
        mImageButtonMovementDecrease.setOnClickListener(this);
        mImageButtonTimeDecrease.setOnClickListener(this);
        mImageButtonTimeIncrease.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.layoutSwitchToMapPickerDeparture :
                switchToLocationPickerFragment();
                break;

            case R.id.editText_takeoff_date_time :
                openDatePicker(R.id.editText_takeoff_date_time);
                break;

            case R.id.editText_landing_date_time :
                openDatePicker(R.id.editText_landing_date_time);
                break;

            case R.id.imageButtonMovementAddNum :
                mCreateFlightPlanViewModel.increaseNumberOfMovements();
                break;

            case R.id.imageButtonMovementDecrease :
                mCreateFlightPlanViewModel.decreaseNumberOfMovements();
                break;

            case R.id.imageButtonTimeAdd :
                mCreateFlightPlanViewModel.increaseActivityDays();
                break;

            case R.id.imageButtonTimeDecrease :
                mCreateFlightPlanViewModel.decreaseActivityDays();
                break;
        }
    }

    private void openDatePicker(int id) {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day

        new DatePickerDialog(mActivity, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                String selectedDate;
                switch (id){
                    case R.id.editText_takeoff_date_time :
                        selectedDate = TimeUtils.formatDate(day, month, year);
                        mCreateFlightPlanViewModel.setmTakeOffDate(selectedDate);
                        mEditTextTakeOffDate.setText(selectedDate);
                        break;

                    case R.id.editText_landing_date_time :
                        selectedDate = TimeUtils.formatDate(day, month, year);
                        mEditTextLandingDate.setText(selectedDate);
                        mCreateFlightPlanViewModel.setmLandingDate(selectedDate);
                        break;
                }
            }
        }, mYear, mMonth, mDay).show();
    }

    private void switchToLocationPickerFragment() {
        Log.d(TAG , "Switching to map picker frag");
        LocationPickerFragment locationPickerFragment = new LocationPickerFragment();
        setFragment(locationPickerFragment, TAG_MAP_PICKER_FRAGMENT);
    }

    public void setFragment(Fragment fragment, String fragmentTAG) {
         FragmentManager mFragmentManager = mActivity.getSupportFragmentManager();
         mFragmentManager.beginTransaction()
                .replace(R.id.main_activity_container, fragment , fragmentTAG)
                .addToBackStack(fragmentTAG)
                .commit();
    }

    private void showMessage(@NonNull String msg) {
        Toast.makeText(mActivity , msg, Toast.LENGTH_SHORT).show();
    }

    private void showUploadingDialog(){
        mProgressDialogUploadingData.show();
    }

    private void dismissUploadingDialog(){
        if (mProgressDialogUploadingData != null) {
            mProgressDialogUploadingData.dismiss();
        }
    }

}
