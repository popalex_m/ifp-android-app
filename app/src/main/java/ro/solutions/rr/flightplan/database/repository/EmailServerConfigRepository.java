package ro.solutions.rr.flightplan.database.repository;

import android.arch.lifecycle.LiveData;
import android.util.Log;

import ro.solutions.rr.flightplan.database.dao.EmailServerConfigDao;
import ro.solutions.rr.flightplan.database.models.EmailServerConfig;

public class EmailServerConfigRepository {

    private String TAG = UserAccountRepository.class.getSimpleName();

    private EmailServerConfigDao mEmailServerConfigDao;

    public EmailServerConfigRepository(EmailServerConfigDao emailServerConfigDao) { mEmailServerConfigDao = emailServerConfigDao; }

    public LiveData<EmailServerConfig> getEmailServerConfigLive() { return mEmailServerConfigDao.getEmailServerDetailsLive(); }

    public EmailServerConfig getEmailServerConfig() { return mEmailServerConfigDao.getEmailServerDetails(); }

    public void saveEmailServerConfig(EmailServerConfig emailServerConfig) {
        mEmailServerConfigDao.insertEmailServerConfig(emailServerConfig);
    }

    public void updateEmailServerConfig(EmailServerConfig newEmailServerConfig) {
        EmailServerConfig previousConfig = mEmailServerConfigDao.getEmailServerDetails();
        if (previousConfig != null) {
            Log.d(TAG , "Updating previous email config with id " + previousConfig.getId());
            previousConfig.setId(previousConfig.getId());
            previousConfig.setFrom(newEmailServerConfig.getFrom());
            previousConfig.setEmailServerAuthUsername(newEmailServerConfig.getEmailServerAuthUsername());
            previousConfig.setEmailServerAuthPassword(newEmailServerConfig.getEmailServerAuthPassword());
            previousConfig.setHost(newEmailServerConfig.getHost());
            previousConfig.setPort(newEmailServerConfig.getPort());
            previousConfig.setmSecure(newEmailServerConfig.isSecure());

            mEmailServerConfigDao.updateEmailServerConfig(previousConfig);
        } else {
            Log.d(TAG , "Inserting new email config");
            mEmailServerConfigDao.insertEmailServerConfig(newEmailServerConfig);
        }
    }

    public int deleteAllEmailServerConfigs() {
        return mEmailServerConfigDao.deleteAllEmailConfiguration();
    }
}
