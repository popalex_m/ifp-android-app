package ro.solutions.rr.flightplan.ui.mainNav.pilots;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import ro.solutions.rr.flightplan.app.MainApp;
import ro.solutions.rr.flightplan.database.dao.PilotsDao;
import ro.solutions.rr.flightplan.database.dao.UserAccountDao;
import ro.solutions.rr.flightplan.database.models.Pilot;
import ro.solutions.rr.flightplan.database.models.UserAccount;
import ro.solutions.rr.flightplan.database.repository.PilotRepository;
import ro.solutions.rr.flightplan.database.repository.RemoteRepository;
import ro.solutions.rr.flightplan.database.repository.UserAccountRepository;
import ro.solutions.rr.flightplan.network.pilot.CreateEditPilotRequest;
import ro.solutions.rr.flightplan.network.pilot.CreatePilotResponse;
import ro.solutions.rr.flightplan.services.NetworkServices;
import ro.solutions.rr.flightplan.utils.NetworkUtils;

public class PilotsManagementViewModel extends AndroidViewModel {

    private String TAG = PilotsManagementViewModel.class.getSimpleName();

    private PilotRepository mPilotRepository;
    private RemoteRepository mNetworkRepository;
    private UserAccountRepository mUserAccountRepository;

    private MutableLiveData<HashMap<Boolean, String>> mNetworkRequestStatusMsg;
    private MutableLiveData<Boolean> mShouldShowLoadingDialog;
    private UserAccount mUserAccount;

    public PilotsManagementViewModel(@NonNull Application application) {
        super(application);
        UserAccountDao mUserAccountDao = ((MainApp) application).getDatabaseManager().getUserDao();
        PilotsDao mPilotsDao = ((MainApp) application).getDatabaseManager().getPilotsDao();
        NetworkServices mNetworkServices = ((MainApp)application).getNetworkServices();

        mUserAccountRepository = new UserAccountRepository(mUserAccountDao);
        mPilotRepository = new PilotRepository(mPilotsDao);
        mNetworkRepository = new RemoteRepository(mNetworkServices);
        mUserAccount = mUserAccountRepository.getLoggedInUser();
    }

    public LiveData<List<Pilot>> getCurrentPilotsInDb() {
        return mPilotRepository.getAllPilots();
    }

    public MutableLiveData<HashMap<Boolean,String>> getRequestStatus() {
        if (mNetworkRequestStatusMsg == null ) {
            mNetworkRequestStatusMsg = new MutableLiveData<>();
        } return mNetworkRequestStatusMsg;
    }

    public MutableLiveData<Boolean> getShouldShowLoadingDialog() {
        if (mShouldShowLoadingDialog == null) {
            mShouldShowLoadingDialog = new MutableLiveData<>();
            mShouldShowLoadingDialog.setValue(false);
        } return mShouldShowLoadingDialog;
    }

    private CreateEditPilotRequest createPilotRequest (@NonNull Pilot pilot) {
        if (mUserAccount != null) {
            CreateEditPilotRequest pilotRequest = new CreateEditPilotRequest();
            String sessionToken = mUserAccount.getmSessionToken();
            pilotRequest.setmSession(sessionToken);
            pilotRequest.setmPilot(pilot);
            return pilotRequest;
        } else {
            return null;
        }
    }

    public void addPilotRemote(@NonNull Pilot pilot) {
        HashMap<Boolean, String> networkRequestMessage = new HashMap<>();
        if (NetworkUtils.isConnected(getApplication())) {
            CreateEditPilotRequest pilotRequest = createPilotRequest(pilot);
            if (pilotRequest != null) {
                submitUploadRequest(pilotRequest, networkRequestMessage);
            }
        } else {
            networkRequestMessage.put(true, "There is no internet connection available");
            mNetworkRequestStatusMsg.setValue(networkRequestMessage);
        }
    }

    public void updateRemotePilot(@NonNull Pilot pilot) {
        CreateEditPilotRequest pilotRequest = createPilotRequest(pilot);
        HashMap<Boolean, String> networkReqMessage = new HashMap<>();
        if (pilotRequest != null) {
            if (NetworkUtils.isConnected(getApplication())) {
                submitUpdatePilotRequest(pilotRequest, pilot, networkReqMessage);
            } else {
                Log.d(TAG, "There is not internet connection available at this time");
            }
        }
    }

    public void deletePilotRemote (@NonNull Pilot pilot) {
        HashMap<Boolean, String> networkReqMessage = new HashMap<>();
        CreateEditPilotRequest pilotRequest = createPilotRequest(pilot);
        if (pilotRequest != null) {
            if (NetworkUtils.isConnected(getApplication())){
                submitDeletePilotRequest(pilotRequest, pilot, networkReqMessage);
            } else {
                networkReqMessage.put(true, "There is no internet connection available");
                mNetworkRequestStatusMsg.setValue(networkReqMessage);
                Log.d(TAG , "There is no internet connection available");
            }
        } else {
            Log.e(TAG , "Pilot request creation returned null");
        }
    }

    private void submitUploadRequest (@NonNull CreateEditPilotRequest pilotRequest, @NonNull HashMap<Boolean, String> networkRequestMessage) {
        mShouldShowLoadingDialog.setValue(true);
        mNetworkRepository.uploadNewPilot(pilotRequest)
                .subscribe(new Observer<Response<CreatePilotResponse>>() {
            @Override
            public void onSubscribe(Disposable d) { }

            @Override
            public void onNext(Response<CreatePilotResponse> pilotResponse) {
                int code = pilotResponse.code();
                Log.d(TAG , "Received backend response with status code == " + code);
                mShouldShowLoadingDialog.setValue(false);
                if (code == 200) {
                    if (Objects.requireNonNull(pilotResponse.body()).getmNewPilotId() != null) {
                        Pilot pilot = pilotRequest.getmPilot();
                        pilot.setId(pilotResponse.body().getmNewPilotId());
                        mPilotRepository.insertSinglePilotSync(pilot);
                        networkRequestMessage.put(true, "Pilot added successfully");
                        mNetworkRequestStatusMsg.setValue(networkRequestMessage);
                    } else {
                        Log.e(TAG , "Remote backend returned a null for the pilot id");
                    }

                } else {
                    networkRequestMessage.put(true, "Error, pilot could not be added");
                    mNetworkRequestStatusMsg.setValue(networkRequestMessage);
                }
            }

            @Override
            public void onError(Throwable e) { e.printStackTrace(); }

            @Override
            public void onComplete() { Log.d(TAG,"Network request done"); }
        });
    }


    private void submitDeletePilotRequest(@NonNull CreateEditPilotRequest pilotRequest, @NonNull Pilot pilot, @NonNull HashMap<Boolean, String> networkReqMessage) {
        mShouldShowLoadingDialog.setValue(true);
        mNetworkRepository.deletePilot(pilotRequest).subscribe(new Observer<Response<ResponseBody>>() {
            @Override
            public void onSubscribe(Disposable d) {  }

            @Override
            public void onNext(Response<ResponseBody> responseBodyResponse) {
                int code = responseBodyResponse.code();
                Log.d(TAG , "Received backend response with status code == " + code);
                mShouldShowLoadingDialog.setValue(false);
                if (code == 200) {
                    networkReqMessage.put(true, "Pilot deleted successfully ");
                    mNetworkRequestStatusMsg.setValue(networkReqMessage);
                    mPilotRepository.deleteSinglePilotSync(pilot);
                } else {
                    networkReqMessage.put(true, "Error, pilot could not be deleted");
                    mNetworkRequestStatusMsg.setValue(networkReqMessage);
                }
            }

            @Override
            public void onError(Throwable e) { e.printStackTrace(); }

            @Override
            public void onComplete() { }
        });
    }

    private void submitUpdatePilotRequest(@NonNull CreateEditPilotRequest pilotRequest, @NonNull Pilot pilot, @NonNull HashMap<Boolean, String> netReqMessage) {
        mShouldShowLoadingDialog.setValue(true);
        mNetworkRepository.updatePilot(pilotRequest).subscribe(new Observer<Response<ResponseBody>>() {
            @Override
            public void onSubscribe(Disposable d) { }

            @Override
            public void onNext(Response<ResponseBody> createPilotResponseResponse) {
                int code = createPilotResponseResponse.code();
                Log.d(TAG , "Received backend response with status code == " + code);
                mShouldShowLoadingDialog.setValue(false);
                if (code == 200) {
                    mPilotRepository.updateSinglePilotSync(pilot);
                    netReqMessage.put(true, "Pilot updated successfully");
                    mNetworkRequestStatusMsg.setValue(netReqMessage);
                } else {
                    netReqMessage.put(true, "Error, pilot could not be added");
                    mNetworkRequestStatusMsg.setValue(netReqMessage);
                }
            }

            @Override
            public void onError(Throwable e) { e.printStackTrace();}

            @Override
            public void onComplete() { }
        });
    }
}
