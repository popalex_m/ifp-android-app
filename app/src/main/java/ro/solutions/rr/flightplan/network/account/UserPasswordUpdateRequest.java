package ro.solutions.rr.flightplan.network.account;

import com.google.gson.annotations.SerializedName;

public class UserPasswordUpdateRequest {

    @SerializedName("session")
    private String session;
    @SerializedName("new_password")
    private String mNewPassword;
    @SerializedName("old_password")
    private String mOldPassword;

    public UserPasswordUpdateRequest(String session, String mNewPassword, String mOldPassword) {
        this.session = session;
        this.mNewPassword = mNewPassword;
        this.mOldPassword = mOldPassword;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getmNewPassword() {
        return mNewPassword;
    }

    public void setmNewPassword(String mNewPassword) {
        this.mNewPassword = mNewPassword;
    }

    public String getmOldPassword() {
        return mOldPassword;
    }

    public void setmOldPassword(String mOldPassword) {
        this.mOldPassword = mOldPassword;
    }
}
