package ro.solutions.rr.flightplan.app;

import android.app.Application;

import com.facebook.stetho.Stetho;

import ro.solutions.rr.flightplan.BuildConfig;
import ro.solutions.rr.flightplan.services.DatabaseManager;
import ro.solutions.rr.flightplan.services.NetworkManager;
import ro.solutions.rr.flightplan.services.NetworkServices;

public class MainApp extends Application {

    private DatabaseManager mDatabaseManager;
    private NetworkServices mNetworkServices;

    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        mDatabaseManager = DatabaseManager.getDatabaseInstance(this);
        mNetworkServices = NetworkManager.getClient(BuildConfig.BACKEND_URL).create(NetworkServices.class);
    }

    public DatabaseManager getDatabaseManager() {
        return mDatabaseManager;
    }

    public NetworkServices getNetworkServices() {
        return mNetworkServices;
    }
}
