package ro.solutions.rr.flightplan.ui.mainNav.main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import ro.solutions.rr.flightplan.R;
import ro.solutions.rr.flightplan.ui.login.LoginActivity;
import ro.solutions.rr.flightplan.ui.mainNav.createplan.CreateFlightPlanFragment;
import ro.solutions.rr.flightplan.ui.mainNav.history.FlightPlanHistoryFragment;
import ro.solutions.rr.flightplan.ui.mainNav.pilots.PilotsManagementFragment;
import ro.solutions.rr.flightplan.ui.mainNav.planes.PlanesManagementFragment;
import ro.solutions.rr.flightplan.ui.mainNav.user.main.UserManagementFragment;

public class MainActivity extends AppCompatActivity
                                         implements NavigationView.OnNavigationItemSelectedListener {

    private String TAG = MainActivity.class.getSimpleName();

    private String PILOTS_FRAGMENT_TAG = PilotsManagementFragment.class.getSimpleName();
    private String PLANES_FRAGMENT_TAG = PlanesManagementFragment.class.getSimpleName();
    private String USER_MANAGEMENT_FRAGMENT_TAG = UserManagementFragment.class.getSimpleName();
    private String CREATE_FLIGHT_PLAN_FRAGMENT_TAG = CreateFlightPlanFragment.class.getSimpleName();
    private String FLIGHT_PLAN_HISTORY_FRAGMENT_TAG = CreateFlightPlanFragment.class.getSimpleName();

    private FragmentManager mFragmentManager;
    private MainActivityViewModel mMainActivityViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        mFragmentManager = getSupportFragmentManager();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mMainActivityViewModel =  ViewModelProviders.of(MainActivity.this).get(MainActivityViewModel.class);
        registerViewModelObservers();

        switchToHistoryPlan();
    }

    private void registerViewModelObservers () {
        mMainActivityViewModel.getGlobalDataResetState().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer userAccountRows) {
                if (userAccountRows != null) {
                    if (userAccountRows == 0) {
                        Log.d(TAG , "Reset is done, switching to login activity");
                        switchToLoginActivity();
                    }
                }
             }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_pilot_management) {
            switchToPilotsManagement();
        } else if (id == R.id.nav_fleet_management) {
            switchToPlanesManagement();
        } else if (id == R.id.nav_flight_history) {
            switchToHistoryPlan();
        } else if (id == R.id.nav_create_new_document) {
            switchToCreateFlightPlan();
        } else if (id == R.id.nav_user_account) {
            switchToUserSettings();
        } else if (id == R.id.nav_log_out) {
              mMainActivityViewModel.deleteAllDataLocally();
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void switchToPilotsManagement() {
        PilotsManagementFragment pilotsManagementFragment = new PilotsManagementFragment();
        setFragment(pilotsManagementFragment, PILOTS_FRAGMENT_TAG);
    }

    private void switchToPlanesManagement() {
        PlanesManagementFragment pilotsManagementFragment = new PlanesManagementFragment();
        setFragment(pilotsManagementFragment, PLANES_FRAGMENT_TAG);
    }

    private void switchToUserSettings(){
        UserManagementFragment userManagementFragment = new UserManagementFragment();
        setFragment(userManagementFragment, USER_MANAGEMENT_FRAGMENT_TAG);
    }

    private void switchToCreateFlightPlan(){
        CreateFlightPlanFragment createFlightPlanFragment = new CreateFlightPlanFragment();
        setFragment(createFlightPlanFragment, CREATE_FLIGHT_PLAN_FRAGMENT_TAG);
    }

    private void switchToHistoryPlan(){
        FlightPlanHistoryFragment createFlightPlanFragment = new FlightPlanHistoryFragment();
        setFragment(createFlightPlanFragment, FLIGHT_PLAN_HISTORY_FRAGMENT_TAG);
    }

    public void setFragment(Fragment fragment, String fragmentTAG) {
       mFragmentManager = getSupportFragmentManager();
       mFragmentManager.beginTransaction()
                .replace(R.id.main_activity_container, fragment , fragmentTAG)
                .commit();
    }

    private void switchToLoginActivity() {
        Intent loginActivity = new Intent(this, LoginActivity.class);
        startActivity(loginActivity);
        finish();
    }
}
